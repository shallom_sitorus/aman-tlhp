<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LhpController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ObrikController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\TemuanController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EditprofilController;
use App\Http\Controllers\KategoritlController;
use App\Http\Controllers\RekomendasiController;
use App\Http\Controllers\TimpemeriksaController;
use App\Http\Controllers\TindaklanjutController;
use App\Http\Controllers\JadwalpengawasanController;
use App\Http\Controllers\JenispemeriksaanController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use App\Http\Controllers\PrintController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth'])->controller(DashboardController::class)->group(function () {
    Route::get('/dashboard', 'index');
});

Route::middleware(['auth'])->controller(LhpController::class)->group(function () {
    Route::get('/lhp', 'index');
    Route::get('/detaillhp/{id}', 'detailLhp');
    Route::get('/lhp/{id}', 'lhp')->name('jdwlhp');
    Route::post('/create-lhp', 'createLHP');
    Route::put('/update-lhp/{id}', 'updateLHP');
    Route::get('/delete-lhp/{id}', 'deleteLHP');
    Route::post('/create-tlhp', 'createTLHP'); 
    Route::put('/update-tlhp/{id}', 'updateTLHP');
    Route::get('/delete-tlhp/{id}', 'deleteTLHP');
    Route::post('/create-trlhp', 'createTRLHP'); 
    Route::put('/update-trlhp/{id}', 'updateTRLHP');
    Route::get('/delete-trlhp/{id}', 'deleteTRLHP');
    Route::get('/lhp-tl/{id}', 'listTL');
    // Route::get('/printlhp', 'printLhp');

});

Route::middleware(['auth'])->controller(JadwalpengawasanController::class)->group(function () {
    Route::get('/JadwalPengawasan', 'index');
    Route::get('/DetailJadwal/{id}', 'Detailjadwal');
    Route::post('/Tambah-jadwal', 'TambahJadwal');
    Route::post('/selectTim2','Timpem2')->name('tim2.select');
    Route::post('/selectTim3','Timpem3')->name('tim3.select');
    Route::post('/updateTim2','gettp2')->name('tim2update');
    Route::post('/updateTim3','gettp3')->name('tim3update');
    Route::post('/selectJenis2','Jenispem2')->name('jenis2.select');
    Route::post('/selectJenis3','Jenispem3')->name('jenis3.select');
    Route::get('/Edit-JadwalLoad', 'EditJadwalLoad')->name('EditJadwalLoad');
    Route::post('/Edit-JadwalLoad', 'UpdateJadwal')->name('UpdateJadwal');
    // Route::post('/Update-jadwal/{id}', 'UpdateJadwal');
    Route::get('/Delete-jadwal/{id}', 'DeleteJadwal');
    Route::post('/create-timaudit', 'createAudit');
    Route::put('/update-timaudit/{id}', 'updateAudit');
    Route::get('/delete-timaudit/{id}', 'deleteAudit');
    Route::post('/update-timpemeriksa/{id}', 'updateTim');
    Route::post('/update-jenispengawasan/{id}', 'updateJenis');
    Route::post('/update-obrik/{id}', 'updateObrik');
    Route::post('/update-jadwal/{id}', 'updateJadwal');
    Route::post('/update-anggaran/{id}', 'updateAnggaran');
    Route::post('/update-area/{id}', 'updateArea');
    Route::post('/update-tujuan/{id}', 'updateTujuan');
    Route::post('/update-ruang/{id}', 'updateRuang');
    Route::post('/update-hari/{id}', 'updateHari');
    Route::post('/update-sarana/{id}', 'updateSarana');
    Route::post('/update-tingkat/{id}', 'updateTingkat');
    Route::post('/update-keterangan/{id}', 'updateKeterangan');
});

Route::middleware(['auth'])->controller(JenisPemeriksaanController::class)->group(function () {
    Route::get('/Jenispemeriksaan1', 'jenispemeriksaan1');
    Route::get('/Jenispemeriksaan1/{id}', 'jenispemeriksaan2');
    Route::get('/Jenispemeriksaan2/{id}', 'jenispemeriksaan3');
    Route::post('/Tambah-jp1', 'TambahJP1');
    Route::post('/Update-jp1/{id}', 'UpdateJP1');
    Route::get('/Delete-jp1/{id}', 'DeleteJP1');
    Route::post('/Tambah-jp2', 'TambahJP2');
    Route::post('/Update-jp2/{id}', 'UpdateJP2');
    Route::get('/Delete-jp2/{id}', 'DeleteJP2');
    Route::post('/Tambah-jp3', 'TambahJP3');
    Route::post('/Update-jp3/{id}', 'UpdateJP3');
    Route::get('/Delete-jp3/{id}', 'DeleteJP3');
});

Route::middleware(['auth'])->controller(RekomendasiController::class)->group(function(){
    Route::get('/rekomendasi', 'index' );
    Route::post('/create-rekomendasi', 'create');
    Route::put('/update-rekomendasi/{id}', 'update');
    Route::get('/delete-rekomendasi/{id}', 'delete');

});

Route::middleware(['auth'])->controller(ObrikController::class)->group(function () {
    Route::get('/mitra-pengawasan', 'index');
    Route::get('/mitra-pengawasan/{id}', 'listuser');
    Route::post('/create-mitra', 'createObrik');
    Route::put('/update-mitra/{id}', 'updateObrik');
    Route::get('/delete-mitra/{id}', 'deleteObrik');
});

Route::middleware(['auth'])->controller(UserController::class)->group(function () {
    Route::post('/create-user', 'create');
    Route::put('/update-user/{id}', 'update');
    Route::get('/delete-user/{id}', 'delete');
    Route::get('/myprofil', 'profil');
    Route::get('/editprofil', 'editprofil');
    Route::put('/update-profil/{id}', 'updateProfil');
});

Route::middleware(['auth'])->controller(TemuanController::class)->group(function(){
    Route::get('/temuan', 'index' );
    Route::post('/create-temuan', 'create');
    Route::put('/update-temuan/{id}', 'update');
    Route::get('/delete-temuan/{id}', 'delete');

});

Route::middleware(['auth'])->controller(KategoritlController::class)->group(function(){
    Route::get('/kategoritl', 'index' );
    Route::post('/create-kategoritl', 'create');
    Route::put('/update-kategoritl/{id}', 'update');
    Route::get('/delete-kategoritl/{id}', 'delete');

});

Route::middleware(['auth'])->controller(TimpemeriksaController::class)->group(function () {
    Route::get('/Timpemeriksa1', 'timpemeriksa1');
    Route::get('/Timpemeriksa1/{id}', 'timpemeriksa2');
    Route::get('/Timpemeriksa2/{id}', 'timpemeriksa3');
    Route::post('/Tambah-tp1', 'TambahTP1');
    Route::post('/Update-tp1/{id}', 'UpdateTP1');
    Route::get('/Delete-tp1/{id}', 'DeleteTP1');
    Route::post('/Tambah-tp2', 'TambahTP2');
    Route::post('/Update-tp2/{id}', 'UpdateTP2');
    Route::get('/Delete-tp2/{id}', 'DeleteTP2');
    Route::post('/Tambah-tp3', 'TambahTP3');
    Route::post('/Update-tp3/{id}', 'UpdateTP3');
    Route::get('/Delete-tp3/{id}', 'DeleteTP3');
});

Route::middleware(['auth'])->controller(LaporanController::class)->group(function () {
    Route::get('/printlhp', 'printLhp');
    Route::get('/printlhp/{id}', 'printLhpDetail');

});

Route::middleware(['auth'])->controller(TindaklanjutController::class)->group(function () {
    Route::post('/create-tindaklanjut', 'createTindakLanjut'); 
    Route::put('/update-tindaklanjut/{id}', 'updateTindakLanjut');
    Route::get('/delete-tindaklanjut/{id}', 'deleteTindakLanjut');
    Route::post('/create-penagihan1', 'createPenagihan1'); 
    Route::post('/create-penagihanSatu/{id}', 'createPenagihanSatu'); 
    Route::post('/create-penagihan2/{id}', 'createPenagihan2'); 
    Route::post('/create-pemberitahuan/{id}', 'createPemberitahuan'); 
    Route::post('/create-tindakantambahan/{id}', 'createTindakanTambahan'); 
});


Route::get('/print', [PrintController::class, 'index']);
