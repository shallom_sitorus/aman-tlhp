<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Concerns\AsPivot;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Tlhp extends Pivot
{
    use HasFactory;

    protected $table = 'tlhp';
    protected $guarded = ['id'];

    public function lhp() : BelongsTo
    {
        return $this->belongsTo(Lhp::class, 'id_lhp');
    }

    public function temuan() : BelongsTo
    {
        return $this->belongsTo(Temuan::class, 'id_temuan');
    }

    public function rekomendasi()
    {
        return $this->belongsToMany(Rekomendasi::class, 'trlhp', 'id_tlhp', 'id_rekomendasi')->using(Trlhp::class)->withPivot(['id', 'uraian_rekomendasi']);
    }
}
