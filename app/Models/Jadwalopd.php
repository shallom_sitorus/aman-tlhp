<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Jadwalopd extends Pivot
{
    use HasFactory;
    protected $table = 'jadwal_opd';
    protected $guarded = ['id'];

    protected $fillable = [
        'id_jadwal',
        'id_opd',
    ];

    public function jadwal() : BelongsTo
    {
        return $this->belongsTo(Jadwalpengawasan::class, 'id_jadwal');
    }

    public function opd() : BelongsTo
    {
        return $this->belongsTo(Opd::class, 'id_opd');
    }

    public function lhp()
    {
        return $this->hasMany(Lhp::class, 'jadwal');
    }
}
