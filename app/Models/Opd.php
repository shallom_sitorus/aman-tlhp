<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opd extends Model
{
    use HasFactory;
    protected $table = 'opd';
    protected $guarded = ['id'];

    protected $fillable = [
        'nama', 'alamat', 'email', 'telp'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
    public function jadwal()
    {
        return $this->belongsToMany(Jadwalpengawasan::class, 'jadwal_opd', 'id_jadwal', 'id_opd')->using(Jadwalopd::class)->withPivot('id');
    }
}
