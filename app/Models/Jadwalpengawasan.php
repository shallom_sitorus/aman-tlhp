<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jadwalpengawasan extends Model
{
    use HasFactory;
    protected $table = 'jadwal_pengawasan';
    protected $guarded = ['id'];

    protected $fillable = [
        'tim_pemeriksa1',
        'tim_pemeriksa2',
        'tim_pemeriksa3',
        'jenis_pemeriksaan1',
        'jenis_pemeriksaan2',
        'jenis_pemeriksaan3',
        'user',
        'rmp',
        'rsp',
        'rpl',
        'hp',
        'pj',
        'wp',
        'kt',
        'pt',
        'at',
        'anggaran',
        'area_pengawasan',
        'tujuan_sasaran',
        'ruang_lingkup',
        'sarana',
        'tingkat_risiko',
        'keterangan',
        'surat_tugas',
    ];

    public function timpemeriksa1(){
        return $this->belongsTo(Timpemeriksa1::class, 'tim_pemeriksa1');
    }

    public function timpemeriksa2(){
        return $this->belongsTo(Timpemeriksa2::class, 'tim_pemeriksa2');
    }
    
    public function timpemeriksa3(){
        return $this->belongsTo(Timpemeriksa3::class, 'tim_pemeriksa3');
    }

    public function jenispemeriksaan1(){
        return $this->belongsTo(Jenispemeriksaan1::class, 'jenis_pemeriksaan1');
    }

    public function jenispemeriksaan2(){
        return $this->belongsTo(Jenispemeriksaan2::class, 'jenis_pemeriksaan2');
    }

    public function jenispemeriksaan3(){
        return $this->belongsTo(Jenispemeriksaan3::class, 'jenis_pemeriksaan3');
    }

    public function opd()
    {
        return $this->belongsToMany(Opd::class, 'jadwal_opd', 'id_jadwal', 'id_opd')->using(Jadwalopd::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user');
    }

    public function lhp()
    {
        return $this->hasOne(Lhp::class, 'jadwal');
    }

    public function audit()
    {
        return $this->hasMany(Timaudit::class, 'jadwal');
    }

}
