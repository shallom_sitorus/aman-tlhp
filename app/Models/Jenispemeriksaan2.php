<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenispemeriksaan2 extends Model
{
    use HasFactory;

    protected $table = 'jenis_pemeriksaan2' ;
    protected $guarded = ['id'];

    public function jenispemeriksaan1()
    {
        return $this->belongsTo(Jenispemeriksaan1::class, 'id_jp1');
    }

    public function jenispemeriksaan3()
    {
        return $this->hasMany(Jenispemeriksaan3::class);
    }
    
    public function jadwalpengawasan()
    {
        return $this->hasMany(Jadwalpengawasan::class);
    }
}
