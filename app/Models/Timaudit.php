<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timaudit extends Model
{
    use HasFactory;
    protected $table = 'tim_audit';
    protected $guarded = ['id'];

    protected $fillable = [
        'nama',
        'jadwal',
        'kedudukan',
    ];

}
