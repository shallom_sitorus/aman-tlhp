<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timpemeriksa1 extends Model
{
    use HasFactory;

    protected $table = 'tim_pemeriksa1' ;
    protected $guarded = ['id'];

    
    public function jadwalpengawasan()
    {
        return $this->hasMany(Jadwalpengawasan::class);
    }

}
