<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategoritl extends Model
{
    use HasFactory;

    protected $table = 'kategori_tl';
    protected $guarded = ['id'];

    public function tindaklanjut()
    {
        return $this->hasMany(Tindaklanjut::class);
    }
}
