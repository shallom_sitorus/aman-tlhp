<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Trlhp extends Pivot
{
    use HasFactory;

    protected $table = 'trlhp';
    protected $guarded = ['id'];

    public function rekomendasi(): BelongsTo
    {
        return $this->belongsTo(Rekomendasi::class, 'id_rekomendasi');
    }

    public function tlhp(): BelongsTo
    {
        return $this->belongsTo(Tlhp::class, 'id_tlhp');
    }

    public function tindaklanjut()
    {
        return $this->hasMany(Tindaklanjut::class, 'id_trlhp');
    }
}
