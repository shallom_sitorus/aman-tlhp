<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Temuan extends Model
{
    use HasFactory;

    protected $table = 'temuan';
    protected $guarded = ['id'];

    public function lhp()
    {
        return $this->belongsToMany(Lhp::class, 'tlhp', 'id_lhp', 'id_temuan')->using(Tlhp::class)->withPivot(['id', 'uraian_temuan']);
    }

    public function tlhp(): HasMany
    {
        return $this->hasMany(Tlhp::class, 'id_temuan');
    }

    // public function rekomendasi()
    // {
    //     return $this->belongsToMany(Rekomendasi::class, 'trlhp', 'id_tlhp', 'id_rekomendasi')->using(Trlhp::class)->withPivot('uraian_rekomendasi');
    // }
}
