<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenispemeriksaan1 extends Model
{
    use HasFactory;
    
    protected $table = 'jenis_pemeriksaan1' ;
    protected $guarded = ['id'];

    public function jenispemeriksaan2()
    {
        return $this->hasMany(Jenispemeriksaan2::class, 'id_jp1');
    }
    
    public function jadwalpengawasan()
    {
        return $this->hasMany(Jadwalpengawasan::class);
    }
}
