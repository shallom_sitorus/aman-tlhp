<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tindaklanjut extends Model
{
    use HasFactory;

    protected $table = 'tindak_lanjut';
    protected $guarded = ['id'];

    public function lhp()
    {
        return $this->belongsTo(Lhp::class, 'id_lhp');
    }
    
    public function trlhp()
    {
        return $this->belongsTo(Trlhp::class, 'id_trlhp');
    }

    public function kategoritl()
    {
        return $this->belongsTo(Kategoritl::class, 'kategori_tl');
    }
}
