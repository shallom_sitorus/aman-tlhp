<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lhp extends Model
{
    use HasFactory;

    protected $table = 'lhp';
    protected $guarded = ['id'];

    public function temuan()
    {
        return $this->belongsToMany(Temuan::class, 'tlhp', 'id_lhp', 'id_temuan')->using(Tlhp::class)->withPivot(['id', 'uraian_temuan']);
    }

    public function trlhp()
    {
        return $this->hasManyThrough(Trlhp::class, Tlhp::class, 'id_lhp', 'id_tlhp');
    }

    public function tindaklanjut()
    {
        return $this->hasOne(Tindaklanjut::class, 'id_lhp');
    }

    public function jadwalpengawasan()
    {
        return $this->belongsTo(Jadwalpengawasan::class, 'jadwal');
    }
    
    public function jadwalopd()
    {
        return $this->belongsTo(Jadwalopd::class, 'jadwal_opd');
    }
    
    // public function jadwalopd()
    // {
    //     return $this->belongsToMany(Opd::class, 'jadwal_opd', 'id_jadwal', 'id_opd')->using(Jadwalopd::class);
    // }

    // public function opd()
    // {
    //     return $this->belongsToMany(Opd::class, 'jadwal_opd', 'id_jadwal', 'id_opd')->using(Jadwalopd::class);
    // }
}
