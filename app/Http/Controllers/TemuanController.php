<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Temuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TemuanController extends Controller
{
    public function index()
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'temuan' => Temuan::all(),
        ];
        return view('Temuan.index', $data);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_temuan' => 'required|unique:temuan',
            'nama_temuan' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        Temuan::create($validated);

        return redirect('/temuan')->with('success', 'Temuan berhasil ditambahkan!');
    }

    public function update(Request $request, $id)
    {
        $temuan = Temuan::find($id);

        $rules = [
            'nama_temuan' => 'required'
        ];

        if($request->kode_temuan != $temuan->kode_temuan) {
            $rules['kode_temuan'] = 'required|unique:temuan';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        $temuan->update($validated);

        return redirect('/temuan')->with('success', 'Temuan berhasil diubah!');
    }

    public function delete($id)
    {
        $temuan = Temuan::find($id);
        $temuan->delete();
        return redirect('/temuan')->with('success', 'Temuan berhasil dihapus!');
    }
}