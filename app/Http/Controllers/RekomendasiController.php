<?php

namespace App\Http\Controllers;

use App\Models\Rekomendasi;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RekomendasiController extends Controller
{
    public function index()
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'rekomendasi' => Rekomendasi::all(),
        ];
        return view('Rekomendasi.index', $data);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_rekomendasi' => 'required|unique:rekomendasi',
            'nama_rekomendasi' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        Rekomendasi::create($validated);

        return redirect('/rekomendasi')->with('success', 'Rekomendasi berhasil ditambahkan!');
    }

    public function update(Request $request, $id)
    {
        $rekomendasi = Rekomendasi::find($id);

        $rules = [
            'nama_rekomendasi' => 'required'
        ];

        if($request->kode_rekomendasi != $rekomendasi->kode_rekomendasi) {
            $rules['kode_rekomendasi'] = 'required|unique:rekomendasi';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        $rekomendasi->update($validated);

        return redirect('/rekomendasi')->with('success', 'Rekomendasi berhasil diubah!');
    }

    public function delete($id)
    {
        $rekomendasi = Rekomendasi::find($id);
        $rekomendasi->delete();
        return redirect('/rekomendasi')->with('success', 'Rekomendasi berhasil dihapus!');
    }
}