<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Kategoritl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class KategoritlController extends Controller
{
    public function index()
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'kategori_tl' => Kategoritl::all()
        ];
        return view('KategoriTL.index', $data);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'required|unique:kategori_tl'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        Kategoritl::create($validated);

        return redirect('/kategoritl')->with('success', 'Kategori Tindak Lanjut berhasil ditambahkan!');
    }

    public function update(Request $request, $id)
    {
        $kategori = Kategoritl::find($id);

        if($request->nama_kategori != $kategori->nama_kategori) {
            $rules['nama_kategori'] = 'required|unique:kategori_tl';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        $kategori->update($validated);

        return redirect('/kategoritl')->with('success', 'Kategori Tindak Lanjut berhasil diubah!');
    }

    public function delete($id)
    {
        $kategori = Kategoritl::find($id);
        $kategori->delete();
        return redirect('/kategoritl')->with('success', 'Kategori Tindak Lanjut berhasil dihapus!');
    }
}
