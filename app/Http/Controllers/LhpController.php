<?php

namespace App\Http\Controllers;

use App\Models\Jadwalopd;
use App\Models\Lhp;
use App\Models\Tlhp;
use App\Models\User;
use App\Models\Trlhp;
use App\Models\Temuan;
use App\Models\Kategoritl;
use App\Models\Rekomendasi;
use App\Models\Tindaklanjut;
use Illuminate\Http\Request;
use App\Models\Jadwalpengawasan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;


class LhpController extends Controller
{
    public function index(Request $request)
    {
        $jadwal = Jadwalopd::where('id_opd', Auth::user()->kode_opd)->get();
        $jd1 = [];
        foreach ($jadwal as $j) {
            $jd1[] = [
                $j->id_jadwal
            ];
        }

        if ($request->tanggal_lhp) {
            
            if (Auth::user()->role_id == 2) {
                $lhp = Lhp::whereIn('jadwal', $jd1)->where(DB::raw('YEAR(tanggal_lhp)'), $request->tanggal_lhp)->get();
            } else {
                $lhp = Lhp::where(DB::raw('YEAR(tanggal_lhp)'), $request->tanggal_lhp)->get();
            }
        } else {
            if (Auth::user()->role_id == 2) {
                $lhp = Lhp::whereIn('jadwal', $jd1)->where(DB::raw('YEAR(tanggal_lhp)'), now())->get();
            } else {
                $lhp = Lhp::where(DB::raw('YEAR(tanggal_lhp)'), now())->get();
            }
        }
        
        $tahun = DB::table('lhp')->select(DB::raw('YEAR(tanggal_lhp) as tahun'))->orderBy(DB::raw('YEAR(tanggal_lhp)'), 'desc')->groupBy(DB::raw('YEAR(tanggal_lhp)'))->pluck('tahun');

        $data = [
            'users' => User::find(Auth::user()->id),
            'listlhp' => $lhp,
            'temuans' => Temuan::all(),
            'tahun' => $tahun,
            'rekomendasis' => Rekomendasi::all()
        ];

        return view('LHP.listlhp', $data);
    }

    public function detailLhp($id)
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'lhp' => Lhp::find($id),
            'list' => Lhp::where('id', $id)->get(),
            'temuans' => Temuan::all(),
            'rekomendasis' => Rekomendasi::all(),
            'kategoritl' => Kategoritl::all()
        ];
        // dd($data['tlhp']);
        return view('LHP.detaillhp', $data);
    }

    public function lhp($id)
    {
        $jadwal = Jadwalopd::where('id_jadwal', $id)->get();
        

        $data = [
            'users' => User::find(Auth::user()->id),
            'listlhp' => Lhp::where('jadwal', $jadwal[0]['id_jadwal'])->get(),
            // 'listlhp' => Jadwalopd::where('id_jadwal', $id)->get(),
            'temuans' => Temuan::all(),
            'rekomendasis' => Rekomendasi::all(),
            'jadwal' => Jadwalpengawasan::find($id),
            'entitas' => $jadwal
        ];

        return view('LHP.tambahlhp', $data);
    }

    public function createLHP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'no_lhp' => 'required|max:255',
            'tanggal_lhp' => 'required|max:255',
            'tanggal_kirim' => 'required|max:255',
            'jadwal' => 'required',
            'tanggapan' => 'required',
            'batas_waktu' => 'required|max:255',
            'jenis_tl' => 'required',
            'jadwal_opd' => 'required',
            // 'id_temuan' => 'required',
            // 'uraian_temuan' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        // Create tabel LHP
        Lhp::create([
            'no_lhp' => $request->no_lhp,
            'tanggal_lhp' => $request->tanggal_lhp,
            'tanggal_kirim' => $request->tanggal_kirim,
            'jadwal' => $request->jadwal,
            'tanggapan' => $request->tanggapan,
            'batas_waktu' => $request->batas_waktu,
            'jenis_tl' => $request->jenis_tl,
            'jadwal_opd' => $request->jadwal_opd
        ]);

        //Create tabel relasi TLHP
        // if (count($request->id_temuan)>0) {
        //     foreach ($request->id_temuan as $item => $value) {
        //         $data2 = array(
        //             'id_lhp' => $lhp->id,
        //             'id_temuan' => $request['id_temuan'][$item]
        //         );
        //         Tlhp::create($data2);
        //     }

        //     $tlhp_id = Tlhp::latest()->first()->id;
        //     Tlhp::where('id', $tlhp_id)->update([
        //         'uraian_temuan' => $request->uraian_temuan
        //     ]);
        // }

        return redirect('/lhp/'. $request->jadwal)->with('success', 'LHP berhasil ditambahkan!');
    }

    public function updateLHP(Request $request, $id)
    {
        $lhp = Lhp::find($id);

        $rules = [
            'no_lhp' => 'required|max:255',
            'tanggal_lhp' => 'required|max:255',
            'tanggal_kirim' => 'required|max:255',
            'jadwal' => 'required',
            'tanggapan' => 'required',
            'batas_waktu' => 'required|max:255',
            'jenis_tl' => 'required',
            // 'id_temuan' => 'required',
            // 'uraian_temuan' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        $lhp->update([
            'no_lhp' => $request->no_lhp,
            'tanggal_lhp' => $request->tanggal_lhp,
            'tanggal_kirim' => $request->tanggal_kirim,
            'jadwal' => $request->jadwal,
            'tanggapan' => $request->tanggapan,
            'batas_waktu' => $request->batas_waktu,
            'jenis_tl' => $request->jenis_tl,
            'jadwal_opd' => $request->jadwal_opd,
        ]);

        return back()->with('success', 'LHP berhasil diubah!');
    }

    public function deleteLHP($id)
    {
        $lhp = Lhp::find($id);
        if ($lhp->tindaklanjut) {
            $tindaklanjut = Tindaklanjut::find($id);
            if ($tindaklanjut->file_surat_tl) {
                unlink('surat_tl/'. $tindaklanjut->file_surat_tl);
            }
            $tindaklanjut->delete();
        }
        
        $tlhp = Tlhp::where('id_lhp', $lhp->id)->first();
        if ($tlhp != null) {
            Trlhp::where('id_lhp', $tlhp->id)->delete();
            Tlhp::where('id_lhp', $lhp->id)->delete();
        }
        $lhp->delete();

        return back()->with('success', 'LHP berhasil dihapus!');
    }

    public function createTLHP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_temuan' => 'required',
            'uraian_temuan' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        // Create tabel TLHP
        Tlhp::create([
            'id_temuan' => $request->id_temuan,
            'uraian_temuan' => $request->uraian_temuan,
            'id_lhp' => $request->id_lhp,
        ]);

        return redirect('/detaillhp/'. $request->id_lhp)->with('success', 'Temuan LHP berhasil ditambahkan!');
    }

    public function updateTLHP(Request $request, $id)
    {
        $tlhp = Tlhp::find($id);

        $rules = [
            'id_temuan' => 'required',
            'uraian_temuan' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        $tlhp->update([
            'id_temuan' => $request->id_temuan,
            'uraian_temuan' => $request->uraian_temuan,
            'id_lhp' => $request->id_lhp,
        ]);

        return redirect('/detaillhp/'. $request->id_lhp)->with('success', 'Temuan LHP berhasil diubah!');
    }

    public function deleteTLHP($id)
    {
        $tlhp = Tlhp::find($id);
        Trlhp::where('id_tlhp', $tlhp->id)->delete();
        $tlhp->delete();

        return back()->with('success', 'Temuan LHP berhasil dihapus!');
    }

    public function createTRLHP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_rekomendasi' => 'required',
            'uraian_rekomendasi' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        // dd($request);

        // Create tabel TRLHP
        Trlhp::create([
            'id_rekomendasi' => $request->id_rekomendasi,
            'uraian_rekomendasi' => $request->uraian_rekomendasi,
            'id_tlhp' => $request->id_tlhp,
        ]);

        return back()->with('success', 'Rekomendasi LHP berhasil ditambahkan!');
    }

    public function updateTRLHP(Request $request, $id)
    {
        $trlhp = Trlhp::find($id);

        $rules = [
            'id_rekomendasi' => 'required',
            'uraian_rekomendasi' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        $trlhp->update([
            'id_rekomendasi' => $request->id_rekomendasi,
            'uraian_rekomendasi' => $request->uraian_rekomendasi,
            'id_tlhp' => $request->id_tlhp,
        ]);

        return back()->with('success', 'Rekomendasi LHP berhasil diubah!');
    }

    public function deleteTRLHP($id)
    {
        $trlhp = Trlhp::find($id);
        $trlhp->delete();

        return back()->with('success', 'Rekomendasi LHP berhasil dihapus!');
    }

    public function listTL($id)
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'reko' => Trlhp::where('id', $id)->get(),
            'temuans' => Temuan::all(),
            'rekomendasis' => Rekomendasi::all(),
            'kategoritl' => Kategoritl::all()
        ];

        // dd($data['temuan']);
        return view('LHP.tindaklanjut', $data);
    }
}
