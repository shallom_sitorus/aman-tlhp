<?php

namespace App\Http\Controllers;

use App\Models\Jadwalopd;
use App\Models\Lhp;
use App\Models\User;
use App\Models\Tindaklanjut;
use Illuminate\Http\Request;
use App\Models\Jadwalpengawasan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $jadwal = Jadwalopd::where('id_opd', Auth::user()->kode_opd)->get();
        $id_jadwal = [];
        foreach ($jadwal as $j) {
            $id_jadwal[] = [
                $j->id_jadwal
            ];
        }
        $lhpmitra = Lhp::whereIn('jadwal', $id_jadwal)->get();
        $id_lhp = [];
        foreach ($lhpmitra as $lhp) {
            $id_lhp[] = [
                $lhp->id
            ];
        }

        if (Auth::user()->role_id == 2) {
            $jmllhp = $lhpmitra->count();
            $jmljadwal = Jadwalpengawasan::whereIn('id', $id_jadwal)->count();
            $jmltl = Tindaklanjut::whereIn('id_lhp', $id_jadwal)->count();
            $lhpperbulan = DB::table('lhp')->select(DB::raw('count(id) as lhpperbulan'))->where('jadwal', $id_jadwal)->groupBy(DB::raw('MONTH(tanggal_lhp)'))->orderBy(DB::raw('MONTH(tanggal_lhp)'), 'ASC')->pluck('lhpperbulan');
            $bulan = DB::table('lhp')->select(DB::raw('MONTHNAME(tanggal_lhp) as bulan'))->where('jadwal', $id_jadwal)->groupBy(DB::raw('MONTHNAME(tanggal_lhp)'))->orderBy(DB::raw('MONTH(tanggal_lhp)'), 'ASC')->pluck('bulan');
        } else {
            $jmllhp = Lhp::all()->count();
            $jmljadwal = Jadwalpengawasan::all()->count();
            $jmltl = Tindaklanjut::all()->count();
            $lhpperbulan = DB::table('lhp')->select(DB::raw('count(id) as lhpperbulan'))->groupBy(DB::raw('MONTH(tanggal_lhp)'))->orderBy(DB::raw('MONTH(tanggal_lhp)'), 'ASC')->pluck('lhpperbulan');
            $bulan = DB::table('lhp')->select(DB::raw('MONTHNAME(tanggal_lhp) as bulan'))->groupBy(DB::raw('MONTHNAME(tanggal_lhp)'))->orderBy(DB::raw('MONTH(tanggal_lhp)'), 'ASC')->pluck('bulan');
        }
        
        $data = [
            'users' => User::find(Auth::user()->id),
            'jumlahlhp' => $jmllhp,
            'jumlahjdwl' => $jmljadwal,
            'jumlahtl' => $jmltl,
            'lhpperbulan' => $lhpperbulan,
            'bulan' => $bulan
        ];
        
        return view('dashboard', $data);
    }
}
