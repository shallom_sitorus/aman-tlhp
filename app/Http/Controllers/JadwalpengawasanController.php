<?php

namespace App\Http\Controllers;

use App\Models\Opd;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Timpemeriksa1;
use App\Models\Jadwalpengawasan;
use App\Models\Jadwalopd;
use App\Models\Jenispemeriksaan1;
use App\Models\Jenispemeriksaan2;
use App\Models\Lhp;
use App\Models\Timaudit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class JadwalpengawasanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->rmp) {
                $jadwal = Jadwalpengawasan::with('audit','timpemeriksa1', 'jenispemeriksaan1', 'jenispemeriksaan2', 'opd', 'user', 'lhp')
                ->where(DB::raw('YEAR(rmp)'), $request->rmp)->get();
        } else {
                $jadwal = Jadwalpengawasan::with('audit','timpemeriksa1', 'jenispemeriksaan1', 'jenispemeriksaan2', 'opd', 'user', 'lhp')
                ->where(DB::raw('YEAR(rmp)'), now())->get();
        }
        
        $tahun = DB::table('jadwal_pengawasan')->select(DB::raw('YEAR(rmp) as tahun'))->orderBy(DB::raw('YEAR(rmp)'), 'desc')->groupBy(DB::raw('YEAR(rmp)'))->pluck('tahun');

        $data = [
        'users' => User::find(Auth::user()->id),
        'jadwal' => $jadwal,
        't_p1' => Timpemeriksa1::pluck('jenis_tp1', 'id'),
        'j_p1' => Jenispemeriksaan1::pluck('jenis_jp1', 'id'),
        'jp2' => Jenispemeriksaan2::pluck('jenis_jp2', 'id'),
        'obrik' => Opd::pluck('nama', 'id'),
        'tahun' => $tahun
        ];

        return view('JadwalPengawasan.jdwl', $data);
    }

    public function Detailjadwal($id)
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'jadwal' => Jadwalpengawasan::with('audit','timpemeriksa1', 'jenispemeriksaan1', 'jenispemeriksaan2', 'opd', 'user', 'lhp')->where('id', $id)->get(),
            't_p1' => Timpemeriksa1::pluck('jenis_tp1', 'id'),
            'j_p1' => Jenispemeriksaan1::pluck('jenis_jp1', 'id'),
            'jp2' => Jenispemeriksaan2::pluck('jenis_jp2', 'id'),
            'obrik' => Opd::pluck('nama', 'id'),
        ];

        return view('JadwalPengawasan.detailjdwl', $data);
    }

    public function TambahJadwal(Request $request)
    {
        // dd($request);
        try{
        DB::beginTransaction();
        $validator = Validator::make($request->all(),[
            'tim_pemeriksa1' => 'required',
            'jenis_pemeriksaan1' => 'required',
            'jenis_pemeriksaan2' => 'required',
            'obrik' => 'required',
            'rmp' => 'required',
            'rsp' => 'required',
            'rpl' => 'required',
            'hp' => 'required|numeric',
            'anggaran' => 'required|integer',
            'timaudit' => 'required',
            'kedudukan' => 'required',
            'area_pengawasan' => 'required',
            'tujuan_sasaran' => 'required',
            'ruang_lingkup' =>'required',
            'sarana' => 'required',
            'tingkat_risiko' => 'required',
            'pj' => 'required',
            'wp' => 'required',
            'pt' => 'required',
            'kt' => 'required',
            'at' => 'required'
        ]);

        if($validator->fails()){
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }

        $jadwal = Jadwalpengawasan::create([
            'tim_pemeriksa1' => request()->tim_pemeriksa1,
            'jenis_pemeriksaan1' => request()->jenis_pemeriksaan1,
            'jenis_pemeriksaan2' => request()->jenis_pemeriksaan2,
            'obrik' => request()->obrik,
            'rmp' => request()->rmp,
            'rsp' => request()->rsp,
            'rpl' => request()->rpl,
            'hp' => request()->hp,
            'pj' => request()->pj,
            'wp' => request()->wp,
            'pt' => request()->pt,
            'kt' => request()->kt,
            'at' => request()->at,
            'anggaran' => request()->anggaran,
            'area_pengawasan' => request()->area_pengawasan,
            'tujuan_sasaran' => request()->tujuan_sasaran,
            'ruang_lingkup' => request()->ruang_lingkup,
            'sarana' => request()->sarana,
            'tingkat_risiko' => request()->tingkat_risiko,
            'keterangan' => request()->keterangan
        ]);


        if (count($request->timaudit)>0) {
            foreach ($request->timaudit as $item => $value) {
                $data2 = array(
                    'jadwal' => $jadwal->id,
                    'nama' => $request['timaudit'][$item],
                    'kedudukan' => $request['kedudukan'][$item],
                );
                Timaudit::create($data2);
            }
        }

        if (count($request->obrik)>0) {
            foreach ($request->obrik as $item => $value) {
                $obrik = array(
                    'id_jadwal' => $jadwal->id,
                    'id_opd' => $request['obrik'][$item]
                );
                Jadwalopd::create($obrik);
            }
        }
        DB::commit();
            return redirect('JadwalPengawasan')->with('success', 'Jadwal Pengawasan berhasil ditambahkan!');
        }catch(\Exception $e){
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function Jenispem2(Request $request){
        $jenis2 = Jenispemeriksaan2::where('id_jp1', $request->get('id'))
        ->pluck('jenis_jp2', 'id');

        return response()->json($jenis2);
    }
    
    public function EditJadwalLoad(Request $request)
    {
        $jadwal = Jadwalpengawasan::where('id',$request->id)->get();

        $j1 = Jenispemeriksaan1::all();
        $j2 = Jenispemeriksaan2::where('id_jp1', $jadwal[0]['jenis_pemeriksaan1'])->get();
        
        $t1 = Timpemeriksa1::all();
        $opd = Opd::all();
        $obrik = Jadwalopd::where('id_jadwal', $jadwal[0]['id'])->get();
        $audit = Timaudit::where('jadwal', $jadwal[0]['id'])->get();

        return response()->json(['audit'=>$audit,'jadwal'=>$jadwal,'tp1'=>$t1, 'jp1'=>$j1, 'jp2'=>$j2, 'obrik'=>$obrik, 'opd'=>$opd]);
    }

    public function DeleteJadwal($id)
    {
        $jadwal = Jadwalpengawasan::find($id);
        Timaudit::where('jadwal', $jadwal->id)->delete();
        Lhp::where('jadwal', $jadwal->id)->delete();
        Jadwalopd::where('id_jadwal', $jadwal->id)->delete();
        $jadwal->delete();

        return back()->with('success', 'Jadwal Pengawasan berhasil dihapus!');
    }

    public function createAudit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'kedudukan' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        Timaudit::create([
            'nama' => $request->nama,
            'kedudukan' => $request->kedudukan,
            'jadwal' => $request->jadwal
        ]);

        return redirect('/DetailJadwal/'. $request->jadwal)->with('success', 'Tim Audit berhasil ditambahkan!');
    }

    public function updateAudit(Request $request, $id)
    {
        $audit = Timaudit::find($id);
        
        $rules = [
            'nama' => 'required|max:255',
            'kedudukan' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        $audit->update([
            'nama' => $request->nama,
            'kedudukan' => $request->kedudukan,
            'jadwal' => $request->jadwal
        ]);

        return redirect('/DetailJadwal/'. $request->jadwal)->with('success', 'Tim Audit berhasil diubah!');
    }

    public function deleteAudit($id)
    {
        $timaudit = Timaudit::find($id);
        $timaudit->delete();
        return redirect('/DetailJadwal/'. $timaudit->jadwal)->with('success', 'Tim Audit berhasil dihapus!');
    }

    public function updateTim(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'tim_pemeriksa1' => 'required',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'tim_pemeriksa1' => request()->tim_pemeriksa1,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Tim Pemeriksa berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function updateJenis(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'jenis_pemeriksaan1' => 'required',
                'jenis_pemeriksaan2' => 'required',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'jenis_pemeriksaan1' => request()->jenis_pemeriksaan1,
                'jenis_pemeriksaan2' => request()->jenis_pemeriksaan2,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Jenis Pengawasan berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function updateObrik(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'obrik' => 'required',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }

            Jadwalopd::where('id_jadwal', $request->id)->delete();

            if (count($request->obrik)>0) {
                foreach ($request->obrik as $item => $value) {
                    $obrik = array(
                        'id_jadwal' => $jadwal->id,
                        'id_opd' => $request['obrik'][$item]
                    );
                    Jadwalopd::create($obrik);
                }
            }
            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Entitas berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
        
    }

    public function updateJadwal(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'rmp' => 'required',
                'rsp' => 'required',
                'rpl' => 'required',
                'hp' => 'required|numeric',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'rmp' => request()->rmp,
                'rsp' => request()->rsp,
                'rpl' => request()->rpl,
                'hp' => request()->hp,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Jadwal berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function updateAnggaran(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'anggaran' => 'required|integer',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'anggaran' => request()->anggaran,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Anggaran berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function updateArea(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'area_pengawasan' => 'required',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'area_pengawasan' => request()->area_pengawasan,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Area Pengawasan berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function updateTujuan(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'tujuan_sasaran' => 'required',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'tujuan_sasaran' => request()->tujuan_sasaran,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Tujuan Sasaran berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function updateRuang(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'ruang_lingkup' => 'required',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'ruang_lingkup' => request()->ruang_lingkup,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Ruang Lingkup berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function updateHari(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'pj' => 'required',
                'wp' => 'required',
                'pt' => 'required',
                'kt' => 'required',
                'at' => 'required'
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'pj' => request()->pj,
                'wp' => request()->wp,
                'pt' => request()->pt,
                'kt' => request()->kt,
                'at' => request()->at,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Hari Pemeriksaan berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }
    
    public function updateSarana(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'sarana' => 'required',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'sarana' => request()->sarana,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Sarana dan Prasarana berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function updateTingkat(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
            $validator = Validator::make($request->all(),[
                'tingkat_risiko' => 'required',
            ]);

            if($validator->fails()){
                return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
            }
            
            $jadwal->update([
                'tingkat_risiko' => request()->tingkat_risiko,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Tingkat Risiko berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }

    public function updateKeterangan(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $jadwal = Jadwalpengawasan::find($id);
           
            $jadwal->update([
                'keterangan' => request()->keterangan,
            ]);

            DB::commit();
            return redirect('/DetailJadwal/'. $id)->with('success', 'Keterangan berhasil diubah!');
        } catch(\Exception $e) {
            DB::rollback(); 
            return redirect()->back()->with('warning','Something Went Wrong!');
        }
    }
}
