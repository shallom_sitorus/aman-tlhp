<?php

namespace App\Http\Controllers;

use App\Models\Timpemeriksa1;
use App\Models\Timpemeriksa2;
use App\Models\Timpemeriksa3;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TimpemeriksaController extends Controller
{
    public function timpemeriksa1()
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'tp1' => Timpemeriksa1::all()
        ];

        return view('TimPemeriksa.tim1', $data);
    }

    public function TambahTP1(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'jenis_tp1' => 'required'
        ]);

        if($validator->fails()){
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }

        $validated = $validator->validate();

        Timpemeriksa1::create($validated);

        return redirect('Timpemeriksa1')->with('success', 'Tim Pemeriksa berhasil ditambahkan!');
    }

    public function UpdateTP1(Request $request)
    {
        $this->validate($request,[
            'jenis_tp1' => 'required'
        ]);

        $TP1 = Timpemeriksa1::find($request->id);

        $TP1->update([
            'jenis_tp1' => $request->jenis_tp1
        ]);

        // dd($JP1);
        return redirect('Timpemeriksa1')->with('success', 'Tim Pemeriksa berhasil diubah!');
    }

    public function DeleteTP1($id)
    {
        $TP1 = Timpemeriksa1::find($id);
        $TP1->delete();
        return redirect('Timpemeriksa1')->with('success', 'Tim Pemeriksa berhasil dihapus!');
    }
    
}