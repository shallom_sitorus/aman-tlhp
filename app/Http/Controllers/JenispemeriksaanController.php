<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Jenispemeriksaan1;
use App\Models\Jenispemeriksaan2;
use App\Models\Jenispemeriksaan3;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class JenispemeriksaanController extends Controller
{
    public function jenispemeriksaan1()
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'jp1' => Jenispemeriksaan1::all()
        ];

        return view('JenisPemeriksaan.jenis_pemeriksaan1', $data);
    }

    public function jenispemeriksaan2($id)
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'jp1' => Jenispemeriksaan1::find($id),
            'j_p2' => Jenispemeriksaan2::where('id_jp1', $id)->get()
        ];

        return view('JenisPemeriksaan.jenis_pemeriksaan2', $data);
    }

    public function jenispemeriksaan3($id)
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'jp2' => Jenispemeriksaan2::find($id),
            'j_p3' => Jenispemeriksaan3::where('id_jp2', $id)->get()
        ];

        return view('JenisPemeriksaan.jenis_pemeriksaan3', $data);
    }

    public function TambahJP1(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'jenis_jp1' => 'required'
        ]);

        if($validator->fails()){
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }

        $validated = $validator->validate();

        Jenispemeriksaan1::create($validated);

        return redirect('Jenispemeriksaan1')->with('success', 'Jenis Pemeriksaan berhasil ditambahkan!');
    }

    public function UpdateJP1(Request $request)
    {
        $this->validate($request,[
            'jenis_jp1' => 'required'
        ]);

        $JP1 = Jenispemeriksaan1::find($request->id);

        $JP1->update([
            'jenis_jp1' => $request->jenis_jp1
        ]);

        // dd($JP1);
        return redirect('Jenispemeriksaan1')->with('success', 'Jenis Pemeriksaan berhasil diubah!');
    }

    public function DeleteJP1($id)
    {
        $JP1 = Jenispemeriksaan1::find($id);
        $JP2 = Jenispemeriksaan2::where('id_jp1', $JP1->id)->first();
        if ($JP2 != null) {
            Jenispemeriksaan3::where('id_jp2', $JP2->id)->delete();
            Jenispemeriksaan2::where('id_jp1', $JP1->id)->delete();
        }
        $JP1->delete();
        return redirect('Jenispemeriksaan1')->with('success', 'Jenis Pemeriksaan berhasil dihapus!');
    }
    
    public function TambahJP2(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'jenis_jp2' => 'required'
        ]);
        
        if($validator->fails()){
            return back()->with('toast_error', $validator->massege()->all()[0])->withInput();
        }

        Jenispemeriksaan2::create([
            'jenis_jp2' => request()->jenis_jp2,
            'id_jp1'    => request()->id_jp1
        ]);
        
        return redirect('Jenispemeriksaan1/'.request()->id_jp1)->with('success', 'Rincian Jenis Pemeriksaan berhasil ditambahkan!');

    }
    
    public function UpdateJP2(Request $request)
    {
        $this->validate($request,[
            'jenis_jp2' => 'required'
        ]);
        
        $JP2 = Jenispemeriksaan2::find($request->id);
        
        $JP2->update([
            'jenis_jp2' => $request->jenis_jp2
        ]);
        
        return redirect('Jenispemeriksaan1/'.$JP2->jenispemeriksaan1->id)->with('success', 'Rincian Jenis Pemeriksaan berhasil diubah!');
    }
    
    public function DeleteJP2($id)
    {
        $JP2 = Jenispemeriksaan2::find($id);
        Jenispemeriksaan3::where('id_jp2', $JP2->id)->delete();
        $JP2->delete();
        return redirect('Jenispemeriksaan1/'.$JP2->jenispemeriksaan1->id)->with('success', 'Rincian Jenis Pemeriksaan berhasil dihapus!');
    }
    
    public function TambahJP3(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'jenis_jp3' => 'required'
        ]);
        
        if($validator->fails()){
            return back()->with('toast_error', $validator->messages()->all()[0])->withInput();
        }
        
        Jenispemeriksaan3::create([
            'jenis_jp3' => request()->jenis_jp3,
            'id_jp2'    => request()->id_jp2
        ]);
        
        return redirect('Jenispemeriksaan2/'.request()->id_jp2)->with('success', 'New Jenis Pemeriksaan III has been added!');
    }
    
    public function UpdateJP3(Request $request)
    {
        $this->validate($request,[
            'jenis_jp3' => 'required'
        ]);
        
        $JP3 = Jenispemeriksaan3::find($request->id);
        
        $JP3->update([
            'jenis_jp3' => $request->jenis_jp3
        ]);
        
        return redirect('Jenispemeriksaan2/'.$JP3->jenispemeriksaan2->id)->with('success', 'Jenis Pemeriksaan III has been updated!');
    }

    public function DeleteJP3($id)
    {
        $JP3 = Jenispemeriksaan3::find($id);
        $JP3->delete();
        return redirect('Jenispemeriksaan2/'.$JP3->jenispemeriksaan2->id)->with('success', 'Jenis Pemeriksaan III has been deleted!');
    }
}
