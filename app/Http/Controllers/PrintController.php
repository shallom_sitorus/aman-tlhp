<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\Lhp;
use App\Models\User;
use App\Models\Temuan;
use App\Models\Rekomendasi;
use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Models\Jadwalpengawasan;
use App\Models\Tindaklanjut;
use App\Models\Trlhp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PrintController extends Controller
{
    public function index()
    {
        $filename = "lhpterbaru.pdf";

        $data = [
            'title' => 'DAFTAR TEMUAN DAN MONITORING TINDAK LANJUT HASIL PENGAWASAN INSPEKTORAT DAERAH KABUPATEN SUKOHARJO',
            // 'users' => User::find(Auth::user()->id),
            // 'listlhp' => Lhp::with('tindaklanjut', 'tindaklanjut.kategoritl')->where('jadwal', $id)->get(),
        ];

        $html = view('print', $data);

        $pdf = new TCPDF;

        $pdf::SetTitle('Laporan Hasil Pengawasan');
        $pdf::AddPage('H', 'F4');
        $pdf::writeHTML($html);
        $pdf :: Output($filename, 'I');

    }
}
