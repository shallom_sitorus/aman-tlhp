<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function create(Request $request)
    {        
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'username' => 'required|min:3|max:255|unique:users',
            'password' => 'required|min:8|max:255',
            'foto' => 'image|file|max:1024'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->email){
            $this->validate($request,[
                'email' => 'email:dns|unique:users'
            ]);
        }

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'role_id' => $request->role_id,
            'kode_opd' => $request->kode_opd,
            'foto' => 'defaultFoto.png'
        ]);

        // dd($request);
        return redirect('/mitra-pengawasan/'. $request->kode_opd)->with('success', 'User berhasil ditambahkan!');
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $rules = [
            'name' => 'required|max:255',
            'password' => 'required|min:8|max:255'
        ];

        if($request->email != $user->email) {
            $rules['email'] = 'email:dns|unique:users';
        }
        if($request->username != $user->username) {
            $rules['username'] = 'required|min:3|max:255|unique:users';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        //Pengkondisian password
        if($request->password != $request->password_lama) {
            $passwordHash = Hash::make($request->password);
        } else {
            $passwordHash = $request->password_lama;
        }
        
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => $passwordHash,
            'role_id' => $request->role_id,
            'kode_opd' => $request->kode_opd
        ]);

        return redirect('/mitra-pengawasan/'. $request->kode_opd)->with('success', 'User berhasil diubah!');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/mitra-pengawasan/'. $user->opd->id)->with('success', 'User berhasil dihapus!');
    }

    public function profil()
    {
        $data = [
            'users' => User::find(Auth::user()->id)
        ];

        return view('Profil.myprofil', $data);
    }

    public function editprofil()
    {
        $data = [
            'users' => User::find(Auth::user()->id)
        ];
        
        return view('Profil.editprofil', $data);
    }

    public function updateProfil(Request $request, $id)
    {
        $user = User::find(Auth::user()->id);

        $rules = [
            'name' => 'required|max:255',
            'password' => 'required|min:8|max:255',
            'foto' => 'image|file|max:1024'
        ];

        if($request->email != $user->email) {
            $rules['email'] = 'email:dns|unique:users';
        }
        if($request->username != $user->username) {
            $rules['username'] = 'required|min:3|max:255|unique:users';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        //Pengkondisian password
        if($request->password != $request->password_lama) {
            $passwordHash = Hash::make($request->password);
        } else {
            $passwordHash = $request->password_lama;
        }

        // Pengkondisian upload gambar
        if ($request->file('foto')) {
            $nama_foto = date('Ymdhis') . '_' . $request->file('foto')->getClientOriginalName();
            $request->file('foto')->move('images/user/', $nama_foto);
            if ($request->foto_lama != 'defaultFoto.png') {
                unlink('images/user/'. $request->foto_lama);
            }
        } else {
            $nama_foto = $request->foto_lama;
        }
        
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => $passwordHash,
            'role_id' => $request->role_id,
            'kode_opd' => $request->kode_opd,
            'foto' => $nama_foto
        ]);

        return redirect('/myprofil')->with('success', 'Profile berhasil diubah!');
    }
}
