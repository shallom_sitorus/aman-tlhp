<?php

namespace App\Http\Controllers;
use App\Models\Opd;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ObrikController extends Controller
{
    public function index()
    {
        $data = [
            'users' => User::find(Auth::user()->id),
            'obrik' => Opd::all()
        ];
        return view('Obrik.obrik', $data);
    }

    public function listuser($id){
        $data = [
            'users' => User::find(Auth::user()->id),
            'opd' => Opd::find($id),
            'list' => User::where('kode_opd', $id)->get(),
            'roles' => Role::all()
        ];
        // dd($data['list']);
        return view('Obrik.detailobrik', $data);
    }

    public function createObrik(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'alamat' => 'required',
            'email' => 'required|email:dns',
            'telp' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        Opd::create($validated);

        return redirect('/mitra-pengawasan')->with('success', 'Entitas Pengawasan berhasil ditambahkan!');
    }

    public function updateObrik(Request $request, $id)
    {
        $obrik = Opd::find($id);

        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'alamat' => 'required',
            'email' => 'required|email:dns',
            'telp' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $validated = $validator->validate();

        $obrik->update($validated);

        return redirect('/mitra-pengawasan')->with('success', 'Entitas Pengawasan berhasil diubah!');
    }

    public function deleteObrik($id)
    {
        $obrik = Opd::find($id);
        $obrik->delete();
        return redirect('/mitra-pengawasan')->with('success', 'Entitas Pengawasan berhasil dihapus!');
    }
}