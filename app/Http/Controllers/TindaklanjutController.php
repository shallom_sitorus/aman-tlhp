<?php

namespace App\Http\Controllers;

use App\Models\Tindaklanjut;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TindaklanjutController extends Controller
{
    public function createTindakLanjut(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'no_tl' => 'required|max:255',
            'tgl_surat' => 'required',
            'file_surat_tl' => 'file|max:10240|mimes:pdf'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('file_surat_tl')) {
            $file_surat_tl = $request->file('file_surat_tl');
            $nama_file = date('Ymdhis') . '_' . $request->file('file_surat_tl')->getClientOriginalName();
            $file_surat_tl->move('surat_tl/', $nama_file);

            Tindaklanjut::create([
                'no_tl' => $request->no_tl,
                'tgl_surat' => $request->tgl_surat,
                'file_surat_tl' => $nama_file,
                'kategori_tl' => $request->kategori_tl,
                'id_trlhp' => $request->id_trlhp,
                'id_lhp' => $request->id_lhp
            ]);
        } else {
            Tindaklanjut::create([
                'no_tl' => $request->no_tl,
                'tgl_surat' => $request->tgl_surat,
                'kategori_tl' => $request->kategori_tl,
                'id_trlhp' => $request->id_trlhp,
                'id_lhp' => $request->id_lhp
            ]);
        }

        return back()->with('success', 'Tindak Lanjut berhasil ditambahkan!');
    }

    public function updateTindakLanjut(Request $request, $id)
    {
        $tindaklanjut = Tindaklanjut::find($id);

        if (Auth::user()->role_id == 2) {
            $rules = [
                'no_tl' => 'required|max:255',
                'tgl_surat' => 'required',
                'file_surat_tl' => 'file|max:10240|mimes:pdf',
            ];
        } else {
            $rules = [
                'kategori_tl' => 'required',
                'keterangan' => 'required',
            ];
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        //Pengkondisian upload file baru
        if ($request->file('file_surat_tl')) {
            $nama_fileSuratTL = date('Ymdhis') . '_' . $request->file('file_surat_tl')->getClientOriginalName();
            $request->file('file_surat_tl')->move('surat_tl/', $nama_fileSuratTL);
            if ($request->oldFileSuratTL) {
                unlink('surat_tl/'. $request->oldFileSuratTL);
            }
        } else {
            $nama_fileSuratTL = $request->oldFileSuratTL;
        }

        if ($request->file('file_penagihan1')) {
            $nama_filePenagihan1 = date('Ymdhis') . '_' . $request->file('file_penagihan1')->getClientOriginalName();
            $request->file('file_penagihan1')->move('surat_penagihan1/', $nama_filePenagihan1);
            if ($request->oldFilePenagihan1) {
                unlink('surat_penagihan1/'. $request->oldFilePenagihan1);
            }
        } else {
            $nama_filePenagihan1 = $request->oldFilePenagihan1;
        }

        if ($request->file('file_penagihan2')) {
            $nama_filePenagihan2 = date('Ymdhis') . '_' . $request->file('file_penagihan2')->getClientOriginalName();
            $request->file('file_penagihan2')->move('surat_penagihan2/', $nama_filePenagihan2);
            if ($request->oldFilePenagihan2) {
                unlink('surat_penagihan2/'. $request->oldFilePenagihan2);
            }
        } else {
            $nama_filePenagihan2 = $request->oldFilePenagihan2;
        }

        if ($request->file('file_pemberitahuan')) {
            $nama_filePemberitahuan = date('Ymdhis') . '_' . $request->file('file_pemberitahuan')->getClientOriginalName();
            $request->file('file_pemberitahuan')->move('surat_pemberitahuan/', $nama_filePemberitahuan);
            if ($request->oldFilePemberitahuan) {
                unlink('surat_pemberitahuan/'. $request->oldFilePemberitahuan);
            }
        } else {
            $nama_filePemberitahuan = $request->oldFilePemberitahuan;
        }

        if ($request->file('file_tindakan_tambahan')) {
            $nama_fileTambahan = date('Ymdhis') . '_' . $request->file('file_tindakan_tambahan')->getClientOriginalName();
            $request->file('file_tindakan_tambahan')->move('surat_tambahan/', $nama_fileTambahan);
            if ($request->oldFileTambahan) {
                unlink('surat_tambahan/'. $request->oldFileTambahan);
            }
        } else {
            $nama_fileTambahan = $request->oldFileTambahan;
        }

        //Pengkondisian update data tindak lanjut
        if (Auth::user()->role_id == 2) {
            $tindaklanjut->update([
                'no_tl' => $request->no_tl,
                'tgl_surat' => $request->tgl_surat,
                'file_surat_tl' => $nama_fileSuratTL,
            ]);   
        } else {
            $tindaklanjut->update([
                'kategori_tl' => $request->kategori_tl,
                'keterangan' => $request->keterangan,
                'tgl_penagihan1' => $request->tgl_penagihan1,
                'file_penagihan1' => $nama_filePenagihan1,
                'tgl_penagihan2' => $request->tgl_penagihan2,
                'file_penagihan2' => $nama_filePenagihan2,
                'tgl_terbit_pemberitahuan' => $request->tgl_terbit_pemberitahuan,
                'file_pemberitahuan' => $nama_filePemberitahuan,
                'no_tindakan_tambahan' => $request->no_tindakan_tambahan,
                'tgl_tindakan_tambahan' => $request->tgl_tindakan_tambahan,
                'file_tindakan_tambahan' => $nama_fileTambahan,
                'id_trlhp' => $request->id_trlhp,
            ]);  
        }

        return back()->with('success', 'Tindak Lanjut berhasil diubah!');
    }

    public function deleteTindakLanjut($id)
    {
        $tindaklanjut = Tindaklanjut::find($id);
        if ($tindaklanjut->file_surat_tl) {
            unlink('surat_tl/'. $tindaklanjut->file_surat_tl);
        }
        if ($tindaklanjut->file_penagihan1) {
            unlink('surat_penagihan1/'. $tindaklanjut->file_penagihan1);
        }
        if ($tindaklanjut->file_penagihan2) {
            unlink('surat_penagihan2/'. $tindaklanjut->file_penagihan2);
        }
        if ($tindaklanjut->file_pemberitahuan) {
            unlink('surat_pemberitahuan/'. $tindaklanjut->file_pemberitahuan);
        }
        if ($tindaklanjut->file_tindakan_tambahan) {
            unlink('surat_tambahan/'. $tindaklanjut->file_tindakan_tambahan);
        }
        $tindaklanjut->delete();
        return back()->with('success', 'Tindak Lanjut berhasil dihapus!');
    }

    public function createPenagihan1(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tgl_penagihan1' => 'required',
            'file_penagihan1' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('file_penagihan1')) {
            $file_penagihan1 = $request->file('file_penagihan1');
            $nama_file = date('Ymdhis') . '_' . $request->file('file_penagihan1')->getClientOriginalName();
            $file_penagihan1->move('surat_penagihan1/', $nama_file);
        }

        Tindaklanjut::create([
            'tgl_penagihan1' => $request->tgl_penagihan1,
            'file_penagihan1' => $nama_file,
            'id_trlhp' => $request->id_trlhp
        ]);

        return back()->with('success', 'Surat Penagihan 1 berhasil ditambahkan!');
    }

    public function createPenagihanSatu(Request $request, $id)
    {
        $tl = Tindaklanjut::find($id);
        $validator = Validator::make($request->all(), [
            'tgl_penagihan1' => 'required',
            'file_penagihan1' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('file_penagihan1')) {
            $file_penagihan1 = $request->file('file_penagihan1');
            $nama_file = date('Ymdhis') . '_' . $request->file('file_penagihan1')->getClientOriginalName();
            $file_penagihan1->move('surat_penagihan1/', $nama_file);
        }

        $tl->update([
            'tgl_penagihan1' => $request->tgl_penagihan1,
            'file_penagihan1' => $nama_file,
            'id_trlhp' => $request->id_trlhp
        ]);

        return back()->with('success', 'Surat Penagihan 1 berhasil ditambahkan!');
    }

    public function createPenagihan2(Request $request, $id)
    {
        $tl = Tindaklanjut::find($id);
        $validator = Validator::make($request->all(), [
            'tgl_penagihan2' => 'required',
            'file_penagihan2' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('file_penagihan2')) {
            $file_penagihan2 = $request->file('file_penagihan2');
            $nama_file = date('Ymdhis') . '_' . $request->file('file_penagihan2')->getClientOriginalName();
            $file_penagihan2->move('surat_penagihan2/', $nama_file);
        }

        $tl->update([
            'tgl_penagihan2' => $request->tgl_penagihan2,
            'file_penagihan2' => $nama_file,
            'id_trlhp' => $request->id_trlhp
        ]);

        return back()->with('success', 'Surat Penagihan 2 berhasil ditambahkan!');
    }

    public function createPemberitahuan(Request $request, $id)
    {
        $tl = Tindaklanjut::find($id);
        $validator = Validator::make($request->all(), [
            'tgl_terbit_pemberitahuan' => 'required',
            'file_pemberitahuan' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('file_pemberitahuan')) {
            $file_pemberitahuan = $request->file('file_pemberitahuan');
            $nama_file = date('Ymdhis') . '_' . $request->file('file_pemberitahuan')->getClientOriginalName();
            $file_pemberitahuan->move('surat_pemberitahuan/', $nama_file);
        }

        $tl->update([
            'tgl_terbit_pemberitahuan' => $request->tgl_terbit_pemberitahuan,
            'file_pemberitahuan' => $nama_file,
            'id_trlhp' => $request->id_trlhp
        ]);

        return back()->with('success', 'Surat Pemberitahuan berhasil ditambahkan!');
    }

    public function createTindakanTambahan(Request $request, $id)
    {
        $tl = Tindaklanjut::find($id);
        $validator = Validator::make($request->all(), [
            'no_tindakan_tambahan' => 'required',
            'tgl_tindakan_tambahan' => 'required',
            'file_tindakan_tambahan' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('file_tindakan_tambahan')) {
            $file_tindakan_tambahan = $request->file('file_tindakan_tambahan');
            $nama_file = date('Ymdhis') . '_' . $request->file('file_tindakan_tambahan')->getClientOriginalName();
            $file_tindakan_tambahan->move('surat_tambahan/', $nama_file);
        }

        $tl->update([
            'no_tindakan_tambahan' => $request->no_tindakan_tambahan,
            'tgl_tindakan_tambahan' => $request->tgl_tindakan_tambahan,
            'file_tindakan_tambahan' => $nama_file,
            'id_trlhp' => $request->id_trlhp
        ]);

        return back()->with('success', 'Surat Tindakan Tambahan berhasil ditambahkan!');
    }
}
