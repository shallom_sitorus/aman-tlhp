<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\Lhp;
use App\Models\User;
use App\Models\Temuan;
use App\Models\Rekomendasi;
use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Models\Jadwalpengawasan;
use App\Models\Tindaklanjut;
use App\Models\Trlhp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LaporanController extends Controller
{

    public function printLhp(Request $request)
    {   
        // dd($request);
        $jadwal = Jadwalpengawasan::where('obrik', Auth::user()->kode_opd)->get();
        $id_jadwal = [];
        foreach ($jadwal as $j) {
            $id_jadwal[] = [
                $j->id
            ];
        }

        if ($request->tanggal_lhp) {
            
            if (Auth::user()->role_id == 2) {
                $lhp = Lhp::where('jadwal', $id_jadwal)->where(DB::raw('YEAR(tanggal_lhp)'), $request->tanggal_lhp)->with('tindaklanjut', 'tindaklanjut.kategoritl')->get();
            } else {
                $lhp = Lhp::where(DB::raw('YEAR(tanggal_lhp)'), $request->tanggal_lhp)->with('tindaklanjut', 'tindaklanjut.kategoritl')->get();
            }
        } 
        
        $filename = "lhp.pdf";

        $data = [
            'title' => 'DAFTAR TEMUAN DAN MONITORING TINDAK LANJUT HASIL PENGAWASAN INSPEKTORAT DAERAH KABUPATEN SUKOHARJO',
            'users' => User::find(Auth::user()->id),
            'listlhp' => $lhp,
        ];
        $html = view('LHP.printlhp', $data);

        $pdf = new TCPDF;

        $pdf::SetTitle('Laporan Hasil Pengawasan');
        $pdf::SetMargins(4, 3, PDF_MARGIN_RIGHT);
        $pdf::AddPage('L', 'F4');
        $pdf::writeHTML($html);
        $pdf :: Output($filename, 'I');


    }

    public function printLhpDetail($id)
    {   
        
        $filename = "lhp.pdf";

        $data = [
            'title' => 'DAFTAR TEMUAN DAN MONITORING TINDAK LANJUT HASIL PENGAWASAN INSPEKTORAT DAERAH KABUPATEN SUKOHARJO',
            'users' => User::find(Auth::user()->id),
            'listlhp' => Lhp::with('tindaklanjut', 'tindaklanjut.kategoritl')->where('jadwal', $id)->get(),
        ];

        $html = view('LHP.printlhp', $data);

        $pdf = new TCPDF;

        $pdf::SetTitle('Laporan Hasil Pengawasan');
        $pdf::AddPage('L', 'F4');
        $pdf::writeHTML($html);
        $pdf :: Output($filename, 'I');


    }

    
}
