<?php

namespace App\Http\Controllers;

use App\Models\Jadwalpengawasan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfilController extends Controller
{
    public function index()
    {
        $data = [
            'users' => User::find(Auth::user()->id)
        ];
        return view('/Profil/myprofil', $data);
    }
}