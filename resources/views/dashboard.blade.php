@extends('layouts.home')

@section('content')
<div class="card-body">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
</div>
<div id="content-page" class="content-page" style="padding: 1rem 0;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-lg-3">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height overflow-hidden">
                    <div class="iq-card-body pb-0">
                        <div class="rounded-circle iq-card-icon iq-bg-primary"><i class="fa fa-book"></i>
                        </div>
                        <span class="float-right line-height-6">Laporan Hasil Pengawasan</span>
                        <div class="clearfix"></div>
                        <div class="text-center">
                            <h2 class="mb-0"><span class="counter">{{ $jumlahlhp }}</span></h2>
                        </div>
                    </div>
                    {{-- <div id="chart-1"></div> --}}
                    <div>
                        <svg id="SvgjsSvg1092" width="843" height="80" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1094" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs1093"><clipPath id="gridRectMask7nakm269l"><rect id="SvgjsRect1106" width="846" height="83" x="-1.5" y="-1.5" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><clipPath id="gridRectMarkerMask7nakm269l"><rect id="SvgjsRect1107" width="845" height="82" x="-1" y="-1" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><linearGradient id="SvgjsLinearGradient1113" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1114" stop-opacity="0.5" stop-color="rgba(0,132,255,0.5)" offset="0"></stop><stop id="SvgjsStop1115" stop-opacity="0" stop-color="rgba(255,255,255,0)" offset="1"></stop><stop id="SvgjsStop1116" stop-opacity="0" stop-color="rgba(255,255,255,0)" offset="1"></stop></linearGradient></defs><line id="SvgjsLine1097" x1="421.5035842293907" y1="0" x2="421.5035842293907" y2="80" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="421.5035842293907" y="0" width="1" height="80" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG1119" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG1120" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG1123" class="apexcharts-grid"><line id="SvgjsLine1125" x1="0" y1="80" x2="843" y2="80" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine1124" x1="0" y1="1" x2="0" y2="80" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1109" class="apexcharts-area-series apexcharts-plot-series"><g id="SvgjsG1110" class="apexcharts-series" seriesName="series1" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1117" d="M 0 80L 0 20C 75.08440860215053 20 139.44247311827957 65 214.5268817204301 65C 287.14372759856633 65 349.3867383512545 30 422.0035842293907 30C 496.8362007168459 30 560.9784434203789 50 635.8110599078341 50C 708.3271889400921 50 770.483870967742 10 843 10C 843 10 843 10 843 80M 843 10z" fill="url(#SvgjsLinearGradient1113)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMask7nakm269l)" pathTo="M 0 80L 0 20C 75.08440860215053 20 139.44247311827957 65 214.5268817204301 65C 287.14372759856633 65 349.3867383512545 30 422.0035842293907 30C 496.8362007168459 30 560.9784434203789 50 635.8110599078341 50C 708.3271889400921 50 770.483870967742 10 843 10C 843 10 843 10 843 80M 843 10z" pathFrom="M -1 80L -1 80L 214.5268817204301 80L 422.0035842293907 80L 635.8110599078341 80L 843 80"></path><path id="SvgjsPath1118" d="M 0 20C 75.08440860215053 20 139.44247311827957 65 214.5268817204301 65C 287.14372759856633 65 349.3867383512545 30 422.0035842293907 30C 496.8362007168459 30 560.9784434203789 50 635.8110599078341 50C 708.3271889400921 50 770.483870967742 10 843 10" fill="none" fill-opacity="1" stroke="#0084ff" stroke-opacity="1" stroke-linecap="butt" stroke-width="3" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMask7nakm269l)" pathTo="M 0 20C 75.08440860215053 20 139.44247311827957 65 214.5268817204301 65C 287.14372759856633 65 349.3867383512545 30 422.0035842293907 30C 496.8362007168459 30 560.9784434203789 50 635.8110599078341 50C 708.3271889400921 50 770.483870967742 10 843 10" pathFrom="M -1 80L -1 80L 214.5268817204301 80L 422.0035842293907 80L 635.8110599078341 80L 843 80"></path><g id="SvgjsG1111" class="apexcharts-series-markers-wrap"><g class="apexcharts-series-markers"><circle id="SvgjsCircle1131" r="0" cx="422.0035842293907" cy="30" class="apexcharts-marker wer8y14pr no-pointer-events" stroke="#ffffff" fill="#0084ff" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g><g id="SvgjsG1112" class="apexcharts-datalabels"></g></g></g><line id="SvgjsLine1126" x1="0" y1="0" x2="843" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine1127" x1="0" y1="0" x2="843" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG1128" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG1129" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG1130" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect1096" width="0" height="0" x="0" y="0" rx="0" ry="0" fill="#fefefe" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect><g id="SvgjsG1121" class="apexcharts-yaxis" rel="0" transform="translate(-21, 0)"><g id="SvgjsG1122" class="apexcharts-yaxis-texts-g"></g></g></svg>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-lg-3">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height overflow-hidden">
                    <div class="iq-card-body pb-0">
                        <div class="rounded-circle iq-card-icon iq-bg-danger"><i class="ri-calendar-fill"></i></i>
                        </div>
                        <span class="float-right line-height-6">Jadwal Pengawasan</span>
                        <div class="clearfix"></div>
                        <div class="text-center">
                            <h2 class="mb-0"><span class="counter">{{ $jumlahjdwl }}</span></h2>
                        </div>
                    </div>
                    <div>
                        <svg id="SvgjsSvg1178" width="843" height="80" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1180" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs1179"><clipPath id="gridRectMask5mzf0yus"><rect id="SvgjsRect1192" width="846" height="83" x="-1.5" y="-1.5" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><clipPath id="gridRectMarkerMask5mzf0yus"><rect id="SvgjsRect1193" width="845" height="82" x="-1" y="-1" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><linearGradient id="SvgjsLinearGradient1199" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1200" stop-opacity="0.5" stop-color="rgba(230,65,65,0.5)" offset="0"></stop><stop id="SvgjsStop1201" stop-opacity="0" stop-color="rgba(255,255,255,0)" offset="1"></stop><stop id="SvgjsStop1202" stop-opacity="0" stop-color="rgba(255,255,255,0)" offset="1"></stop></linearGradient></defs><line id="SvgjsLine1183" x1="421.5035842293907" y1="0" x2="421.5035842293907" y2="80" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="421.5035842293907" y="0" width="1" height="80" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG1205" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG1206" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG1209" class="apexcharts-grid"><line id="SvgjsLine1211" x1="0" y1="80" x2="843" y2="80" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine1210" x1="0" y1="1" x2="0" y2="80" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1195" class="apexcharts-area-series apexcharts-plot-series"><g id="SvgjsG1196" class="apexcharts-series" seriesName="series1" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1203" d="M 0 80L 0 6.666666666666671C 75.08440860215053 6.666666666666671 139.44247311827957 66.66666666666667 214.5268817204301 66.66666666666667C 287.14372759856633 66.66666666666667 349.3867383512545 26.66666666666667 422.0035842293907 26.66666666666667C 496.8362007168459 26.66666666666667 560.9784434203789 60.00000000000001 635.8110599078341 60.00000000000001C 708.3271889400921 60.00000000000001 770.483870967742 26.66666666666667 843 26.66666666666667C 843 26.66666666666667 843 26.66666666666667 843 80M 843 26.66666666666667z" fill="url(#SvgjsLinearGradient1199)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMask5mzf0yus)" pathTo="M 0 80L 0 6.666666666666671C 75.08440860215053 6.666666666666671 139.44247311827957 66.66666666666667 214.5268817204301 66.66666666666667C 287.14372759856633 66.66666666666667 349.3867383512545 26.66666666666667 422.0035842293907 26.66666666666667C 496.8362007168459 26.66666666666667 560.9784434203789 60.00000000000001 635.8110599078341 60.00000000000001C 708.3271889400921 60.00000000000001 770.483870967742 26.66666666666667 843 26.66666666666667C 843 26.66666666666667 843 26.66666666666667 843 80M 843 26.66666666666667z" pathFrom="M -1 106.66666666666667L -1 106.66666666666667L 214.5268817204301 106.66666666666667L 422.0035842293907 106.66666666666667L 635.8110599078341 106.66666666666667L 843 106.66666666666667"></path><path id="SvgjsPath1204" d="M 0 6.666666666666671C 75.08440860215053 6.666666666666671 139.44247311827957 66.66666666666667 214.5268817204301 66.66666666666667C 287.14372759856633 66.66666666666667 349.3867383512545 26.66666666666667 422.0035842293907 26.66666666666667C 496.8362007168459 26.66666666666667 560.9784434203789 60.00000000000001 635.8110599078341 60.00000000000001C 708.3271889400921 60.00000000000001 770.483870967742 26.66666666666667 843 26.66666666666667" fill="none" fill-opacity="1" stroke="#e64141" stroke-opacity="1" stroke-linecap="butt" stroke-width="3" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMask5mzf0yus)" pathTo="M 0 6.666666666666671C 75.08440860215053 6.666666666666671 139.44247311827957 66.66666666666667 214.5268817204301 66.66666666666667C 287.14372759856633 66.66666666666667 349.3867383512545 26.66666666666667 422.0035842293907 26.66666666666667C 496.8362007168459 26.66666666666667 560.9784434203789 60.00000000000001 635.8110599078341 60.00000000000001C 708.3271889400921 60.00000000000001 770.483870967742 26.66666666666667 843 26.66666666666667" pathFrom="M -1 106.66666666666667L -1 106.66666666666667L 214.5268817204301 106.66666666666667L 422.0035842293907 106.66666666666667L 635.8110599078341 106.66666666666667L 843 106.66666666666667"></path><g id="SvgjsG1197" class="apexcharts-series-markers-wrap"><g class="apexcharts-series-markers"><circle id="SvgjsCircle1217" r="0" cx="422.0035842293907" cy="26.66666666666667" class="apexcharts-marker wmmvsmdbv no-pointer-events" stroke="#ffffff" fill="#e64141" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g><g id="SvgjsG1198" class="apexcharts-datalabels"></g></g></g><line id="SvgjsLine1212" x1="0" y1="0" x2="843" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine1213" x1="0" y1="0" x2="843" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG1214" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG1215" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG1216" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect1182" width="0" height="0" x="0" y="0" rx="0" ry="0" fill="#fefefe" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect><g id="SvgjsG1207" class="apexcharts-yaxis" rel="0" transform="translate(-21, 0)"><g id="SvgjsG1208" class="apexcharts-yaxis-texts-g"></g></g></svg>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-lg-3">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height overflow-hidden">
                    <div class="iq-card-body pb-0">
                        <div class="rounded-circle iq-card-icon iq-bg-success"><i class="ri-check-fill"></i></div>
                        <span class="float-right line-height-6">Sudah Ditindaklanjuti</span>
                        <div class="clearfix"></div>
                        <div class="text-center">
                            <h2 class="mb-0"><span class="counter">{{ $jumlahtl }}</span></h2>
                        </div>
                    </div>
                    <div>
                        <svg id="SvgjsSvg1049" width="843" height="80" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1051" class="apexcharts-inner apexcharts-graphical" transform="translate(0, 0)"><defs id="SvgjsDefs1050"><clipPath id="gridRectMaskhtvrvi7b"><rect id="SvgjsRect1063" width="846" height="83" x="-1.5" y="-1.5" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><clipPath id="gridRectMarkerMaskhtvrvi7b"><rect id="SvgjsRect1064" width="845" height="82" x="-1" y="-1" rx="0" ry="0" fill="#ffffff" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect></clipPath><linearGradient id="SvgjsLinearGradient1070" x1="0" y1="0" x2="0" y2="1"><stop id="SvgjsStop1071" stop-opacity="0.5" stop-color="rgba(0,202,0,0.5)" offset="0"></stop><stop id="SvgjsStop1072" stop-opacity="0" stop-color="rgba(255,255,255,0)" offset="1"></stop><stop id="SvgjsStop1073" stop-opacity="0" stop-color="rgba(255,255,255,0)" offset="1"></stop></linearGradient></defs><line id="SvgjsLine1054" x1="214.0268817204301" y1="0" x2="214.0268817204301" y2="80" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="214.0268817204301" y="0" width="1" height="80" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG1076" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG1077" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG1080" class="apexcharts-grid"><line id="SvgjsLine1082" x1="0" y1="80" x2="843" y2="80" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine1081" x1="0" y1="1" x2="0" y2="80" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1066" class="apexcharts-area-series apexcharts-plot-series"><g id="SvgjsG1067" class="apexcharts-series" seriesName="series1" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1074" d="M 0 80L 0 24C 75.08440860215053 24 139.44247311827957 64 214.5268817204301 64C 287.14372759856633 64 349.3867383512545 24 422.0035842293907 24C 496.8362007168459 24 560.9784434203789 64 635.8110599078341 64C 708.3271889400921 64 770.483870967742 4 843 4C 843 4 843 4 843 80M 843 4z" fill="url(#SvgjsLinearGradient1070)" fill-opacity="1" stroke-opacity="1" stroke-linecap="butt" stroke-width="0" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskhtvrvi7b)" pathTo="M 0 80L 0 24C 75.08440860215053 24 139.44247311827957 64 214.5268817204301 64C 287.14372759856633 64 349.3867383512545 24 422.0035842293907 24C 496.8362007168459 24 560.9784434203789 64 635.8110599078341 64C 708.3271889400921 64 770.483870967742 4 843 4C 843 4 843 4 843 80M 843 4z" pathFrom="M -1 144L -1 144L 214.5268817204301 144L 422.0035842293907 144L 635.8110599078341 144L 843 144"></path><path id="SvgjsPath1075" d="M 0 24C 75.08440860215053 24 139.44247311827957 64 214.5268817204301 64C 287.14372759856633 64 349.3867383512545 24 422.0035842293907 24C 496.8362007168459 24 560.9784434203789 64 635.8110599078341 64C 708.3271889400921 64 770.483870967742 4 843 4" fill="none" fill-opacity="1" stroke="#00ca00" stroke-opacity="1" stroke-linecap="butt" stroke-width="3" stroke-dasharray="0" class="apexcharts-area" index="0" clip-path="url(#gridRectMaskhtvrvi7b)" pathTo="M 0 24C 75.08440860215053 24 139.44247311827957 64 214.5268817204301 64C 287.14372759856633 64 349.3867383512545 24 422.0035842293907 24C 496.8362007168459 24 560.9784434203789 64 635.8110599078341 64C 708.3271889400921 64 770.483870967742 4 843 4" pathFrom="M -1 144L -1 144L 214.5268817204301 144L 422.0035842293907 144L 635.8110599078341 144L 843 144"></path><g id="SvgjsG1068" class="apexcharts-series-markers-wrap"><g class="apexcharts-series-markers"><circle id="SvgjsCircle1088" r="0" cx="214.5268817204301" cy="64" class="apexcharts-marker wvl9j9x9li no-pointer-events" stroke="#ffffff" fill="#00ca00" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g><g id="SvgjsG1069" class="apexcharts-datalabels"></g></g></g><line id="SvgjsLine1083" x1="0" y1="0" x2="843" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine1084" x1="0" y1="0" x2="843" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG1085" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG1086" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG1087" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect1053" width="0" height="0" x="0" y="0" rx="0" ry="0" fill="#fefefe" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0"></rect><g id="SvgjsG1078" class="apexcharts-yaxis" rel="0" transform="translate(-21, 0)"><g id="SvgjsG1079" class="apexcharts-yaxis-texts-g"></g></g></svg>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height overflow-hidden">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Grafik Laporan Hasil Pengawasan </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        {{-- <div class="d-flex justify-content-around">
                            <div class="price-week-box mr-5">
                                <span>Minggu ini</span>
                                <h2><span class="counter">180</span> <i
                                        class="ri-funds-line text-success font-size-18"></i></h2>
                            </div>
                            <div class="price-week-box">
                                <span>Minggu sebelumnya</span>
                                <h2><span class="counter">52.5</span><i
                                        class="ri-funds-line text-danger font-size-18"></i></h2>
                            </div>
                        </div> --}}
                        <canvas id="myChart" style="width:100%;height:335px;"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height animation-card">
                    <div class="iq-card-body p-0">
                        <div class="an-text">
                            <span>Aplikasi </span>
                            <h4 class="display-5 font-weight-bold">AMAN TLHP</h4>
                        </div>
                        <!-- <a href="#"><img src="{!! asset('images/coba3.gif') !!}" class="img-fluid" alt=""></a> -->
                        <div class="an-img">
                            <div class="bodymovin" data-bm-path="images/small/data.json" data-bm-renderer="svg"
                                data-bm-loop="true"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1600 1200"
                                    width="1600" height="1200" preserveAspectRatio="xMidYMid meet"
                                    style="width: 100%; height: 100%; transform: translate3d(0px, 0px, 0px);">
                                    <defs>
                                        <clipPath id="__lottie_element_2">
                                            <rect width="1600" height="1200" x="0" y="0"></rect>
                                        </clipPath>
                                        <clipPath id="__lottie_element_4">
                                            <path d="M0,0 L1600,0 L1600,1200 L0,1200z"></path>
                                        </clipPath>
                                        <mask id="__lottie_element_253" mask-type="alpha">
                                            <g transform="matrix(1.0000077486038208,-0.0006201472133398056,0.0006201472133398056,1.0000077486038208,800.1674194335938,599.9237060546875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(165,91,58)" fill-opacity="1"
                                                        d=" M-138,-455.5 C-138,-455.5 -138,-411.5 -138,-411.5 C-138,-411.5 -136.5,-393 -117.5,-393 C-98.5,-393 -98.5,-409.5 -98.5,-409.5 C-98.5,-409.5 -90.5,-437 -90.5,-437 C-90.5,-437 -138,-455.5 -138,-455.5z">
                                                    </path>
                                                    <path stroke-linecap="butt" stroke-linejoin="miter" fill-opacity="0"
                                                        stroke-miterlimit="4" stroke="rgb(255,255,255)"
                                                        stroke-opacity="1" stroke-width="167" d="M0 0"></path>
                                                </g>
                                            </g>
                                        </mask>
                                    </defs>
                                    <g clip-path="url(#__lottie_element_2)">
                                        <g transform="matrix(1,0,0,1,0,0)" opacity="1" style="display: block;">
                                            <rect width="1600" height="1200" fill="transparent"></rect>
                                        </g>
                                        <g clip-path="url(#__lottie_element_4)"
                                            transform="matrix(0.8700000047683716,0,0,0.8700000047683716,104,78)"
                                            opacity="1" style="display: block;">
                                            <g transform="matrix(1.75,0,0,1.75,766.5,601.5)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-126.77100372314453,301.9339904785156 C-126.77100372314453,301.9339904785156 -127.76899719238281,301.8590087890625 -127.76899719238281,301.8590087890625 C-127.76899719238281,301.8590087890625 -110.9800033569336,75.20899963378906 -110.9800033569336,75.20899963378906 C-110.9800033569336,75.20899963378906 -109.98200225830078,75.28299713134766 -109.98200225830078,75.28299713134766 C-109.98200225830078,75.28299713134766 -126.77100372314453,301.9339904785156 -126.77100372314453,301.9339904785156z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.75,0,0,1.75,766.5,601.5)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-46.88800048828125,301.9339904785156 C-46.88800048828125,301.9339904785156 -47.88600158691406,301.8590087890625 -47.88600158691406,301.8590087890625 C-47.88600158691406,301.8590087890625 -31.097999572753906,75.20899963378906 -31.097999572753906,75.20899963378906 C-31.097999572753906,75.20899963378906 -30.100000381469727,75.28299713134766 -30.100000381469727,75.28299713134766 C-30.100000381469727,75.28299713134766 -46.88800048828125,301.9339904785156 -46.88800048828125,301.9339904785156z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.75,0,0,1.75,766.5,601.5)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-43.65800094604492,252.05999755859375 C-43.65800094604492,252.05999755859375 -131.24400329589844,252.05999755859375 -131.24400329589844,252.05999755859375 C-131.24400329589844,252.05999755859375 -131.24400329589844,251.05999755859375 -131.24400329589844,251.05999755859375 C-131.24400329589844,251.05999755859375 -43.65800094604492,251.05999755859375 -43.65800094604492,251.05999755859375 C-43.65800094604492,251.05999755859375 -43.65800094604492,252.05999755859375 -43.65800094604492,252.05999755859375z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.75,0,0,1.75,766.5,601.5)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-38.99300003051758,189.0709991455078 C-38.99300003051758,189.0709991455078 -126.5790023803711,189.0709991455078 -126.5790023803711,189.0709991455078 C-126.5790023803711,189.0709991455078 -126.5790023803711,188.0709991455078 -126.5790023803711,188.0709991455078 C-126.5790023803711,188.0709991455078 -38.99300003051758,188.0709991455078 -38.99300003051758,188.0709991455078 C-38.99300003051758,188.0709991455078 -38.99300003051758,189.0709991455078 -38.99300003051758,189.0709991455078z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.75,0,0,1.75,766.5,601.5)" opacity="1"
                                                style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-34.77799987792969,131.0970001220703 C-34.77799987792969,131.0970001220703 -122.36399841308594,131.0970001220703 -122.36399841308594,131.0970001220703 C-122.36399841308594,131.0970001220703 -122.36399841308594,130.0970001220703 -122.36399841308594,130.0970001220703 C-122.36399841308594,130.0970001220703 -34.77799987792969,130.0970001220703 -34.77799987792969,130.0970001220703 C-34.77799987792969,130.0970001220703 -34.77799987792969,131.0970001220703 -34.77799987792969,131.0970001220703z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.799729824066162,0.031183531507849693,-0.031183531507849693,1.799729824066162,798.655517578125,587.084716796875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(0,201,156)" fill-opacity="1"
                                                        d=" M111.2490005493164,187.7100067138672 C125.84700012207031,203.052001953125 150.1179962158203,203.656005859375 165.46099853515625,189.0570068359375 C180.80299377441406,174.45899963378906 181.40699768066406,150.18800354003906 166.80799865722656,134.84500122070312 C152.2100067138672,119.50299835205078 127.93900299072266,118.89900207519531 112.59600067138672,133.4980010986328 C97.25399780273438,148.0959930419922 96.6500015258789,172.36700439453125 111.2490005493164,187.7100067138672z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.799729824066162,0.031183531507849693,-0.031183531507849693,1.799729824066162,798.655517578125,587.084716796875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(8,191,139)" fill-opacity="1"
                                                        d=" M111.2490005493164,187.7100067138672 C125.84700012207031,203.052001953125 150.1179962158203,203.656005859375 165.46099853515625,189.0570068359375 C180.80299377441406,174.45899963378906 181.40699768066406,150.18800354003906 166.80799865722656,134.84500122070312 C152.2100067138672,119.50299835205078 127.93900299072266,118.89900207519531 112.59600067138672,133.4980010986328 C97.25399780273438,148.0959930419922 96.6500015258789,172.36700439453125 111.2490005493164,187.7100067138672z M111.21800231933594,187.70899963378906 C125.81600189208984,203.052001953125 150.08700561523438,203.656005859375 165.42999267578125,189.0570068359375 C176.03399658203125,178.9669952392578 179.5919952392578,164.2550048828125 175.96099853515625,151.0659942626953 C175.96099853515625,151.0659942626953 100.71900177001953,163.38600158691406 100.71900177001953,163.38600158691406 C101.197998046875,172.17100524902344 104.6780014038086,180.83599853515625 111.21800231933594,187.70899963378906z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.799729824066162,0.031183531507849693,-0.031183531507849693,1.799729824066162,798.655517578125,587.084716796875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M137.2310028076172,169.12100219726562 C137.2310028076172,169.12100219726562 136.56700134277344,168.3730010986328 136.56700134277344,168.3730010986328 C136.56700134277344,168.3730010986328 159.343994140625,148.14100646972656 159.343994140625,148.14100646972656 C159.343994140625,148.14100646972656 160.00799560546875,148.88900756835938 160.00799560546875,148.88900756835938 C160.00799560546875,148.88900756835938 137.2310028076172,169.12100219726562 137.2310028076172,169.12100219726562z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.799729824066162,0.031183531507849693,-0.031183531507849693,1.799729824066162,798.655517578125,587.084716796875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M136.3070068359375,156.8300018310547 C136.3070068359375,156.8300018310547 123.91799926757812,151.4499969482422 123.91799926757812,151.4499969482422 C123.91799926757812,151.4499969482422 124.31600189208984,150.53199768066406 124.31600189208984,150.53199768066406 C124.31600189208984,150.53199768066406 136.7050018310547,155.91200256347656 136.7050018310547,155.91200256347656 C136.7050018310547,155.91200256347656 136.3070068359375,156.8300018310547 136.3070068359375,156.8300018310547z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.799729824066162,0.031183531507849693,-0.031183531507849693,1.799729824066162,798.655517578125,587.084716796875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M140.6280059814453,301.9119873046875 C140.6280059814453,301.9119873046875 135.50100708007812,140.50100708007812 135.50100708007812,140.50100708007812 C135.50100708007812,140.50100708007812 136.50100708007812,140.47000122070312 136.50100708007812,140.47000122070312 C136.50100708007812,140.47000122070312 141.6280059814453,301.8810119628906 141.6280059814453,301.8810119628906 C141.6280059814453,301.8810119628906 140.6280059814453,301.9119873046875 140.6280059814453,301.9119873046875z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.748942255973816,-0.06083739921450615,0.06083739921450615,1.748942255973816,774.9908447265625,613.1087036132812)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(0,185,143)" fill-opacity="1"
                                                        d=" M121.63600158691406,223.177001953125 C121.63600158691406,253.96099853515625 146.59100341796875,278.9150085449219 177.37399291992188,278.9150085449219 C208.15699768066406,278.9150085449219 233.11199951171875,253.96099853515625 233.11199951171875,223.177001953125 C233.11199951171875,192.3939971923828 208.15699768066406,167.43899536132812 177.37399291992188,167.43899536132812 C146.59100341796875,167.43899536132812 121.63600158691406,192.3939971923828 121.63600158691406,223.177001953125z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.748942255973816,-0.06083739921450615,0.06083739921450615,1.748942255973816,774.9908447265625,613.1087036132812)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M177.58599853515625,238.5030059814453 C177.58599853515625,238.5030059814453 177.16200256347656,237.5970001220703 177.16200256347656,237.5970001220703 C177.16200256347656,237.5970001220703 217.95899963378906,218.552001953125 217.95899963378906,218.552001953125 C217.95899963378906,218.552001953125 218.38299560546875,219.45799255371094 218.38299560546875,219.45799255371094 C218.38299560546875,219.45799255371094 177.58599853515625,238.5030059814453 177.58599853515625,238.5030059814453z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.748942255973816,-0.06083739921450615,0.06083739921450615,1.748942255973816,774.9908447265625,613.1087036132812)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M179.5070037841797,223.57899475097656 C179.5070037841797,223.57899475097656 146.53199768066406,199.2169952392578 146.53199768066406,199.2169952392578 C146.53199768066406,199.2169952392578 147.12600708007812,198.41200256347656 147.12600708007812,198.41200256347656 C147.12600708007812,198.41200256347656 180.1009979248047,222.7740020751953 180.1009979248047,222.7740020751953 C180.1009979248047,222.7740020751953 179.5070037841797,223.57899475097656 179.5070037841797,223.57899475097656z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.748942255973816,-0.06083739921450615,0.06083739921450615,1.748942255973816,774.9908447265625,613.1087036132812)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M167.43800354003906,301.9779968261719 C167.43800354003906,301.9779968261719 166.4510040283203,301.81500244140625 166.4510040283203,301.81500244140625 C166.4510040283203,301.81500244140625 183.62600708007812,196.6790008544922 183.62600708007812,196.6790008544922 C183.62600708007812,196.6790008544922 184.61199951171875,196.84100341796875 184.61199951171875,196.84100341796875 C184.61199951171875,196.84100341796875 167.43800354003906,301.9779968261719 167.43800354003906,301.9779968261719z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.0000077486038208,-0.0006201472133398056,0.0006201472133398056,1.0000077486038208,800.1674194335938,599.9237060546875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(165,91,58)" fill-opacity="1"
                                                        d=" M-138,-455.5 C-138,-455.5 -138,-411.5 -138,-411.5 C-138,-411.5 -136.5,-393 -117.5,-393 C-98.5,-393 -98.5,-409.5 -98.5,-409.5 C-98.5,-409.5 -90.5,-437 -90.5,-437 C-90.5,-437 -138,-455.5 -138,-455.5z">
                                                    </path>
                                                    <path stroke-linecap="butt" stroke-linejoin="miter" fill-opacity="0"
                                                        stroke-miterlimit="4" stroke="rgb(255,255,255)"
                                                        stroke-opacity="1" stroke-width="167" d="M0 0"></path>
                                                </g>
                                            </g>
                                            <g mask="url(#__lottie_element_253)" style="display: block;">
                                                <g transform="matrix(1.0000077486038208,-0.0006201472133398056,0.0006201472133398056,1.0000077486038208,800.1674194335938,599.9237060546875)"
                                                    opacity="1">
                                                    <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                        <path fill="rgb(142,74,47)" fill-opacity="1"
                                                            d=" M-136.75,-451.5 C-136.75,-451.5 -128.5,-415.75 -98.25,-412.25 C-68,-408.75 -83,-424.25 -83,-424.25 C-83,-424.25 -136.75,-451.5 -136.75,-451.5z">
                                                        </path>
                                                        <path stroke-linecap="butt" stroke-linejoin="miter"
                                                            fill-opacity="0" stroke-miterlimit="4"
                                                            stroke="rgb(255,255,255)" stroke-opacity="1"
                                                            stroke-width="167" d="M0 0"></path>
                                                    </g>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,857.9176025390625,352.1927795410156)">
                                                    <g opacity="1"
                                                        transform="matrix(0.019690189510583878,-0.9998061060905457,0.9998061060905457,0.019690189510583878,2.4573898315429688,-124.77855682373047)">
                                                    </g>
                                                    <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                        <g class="04" opacity="1" transform="matrix(1,0,0,1,0,0)"></g>
                                                    </g>
                                                    <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                        <g opacity="1" transform="matrix(1,0,0,1,-200,-200)"></g>
                                                    </g>
                                                    <g opacity="1" transform="matrix(1,0,0,1,-135,0)">
                                                        <g opacity="1" transform="matrix(1,0,0,1,0,0)"></g>
                                                    </g>
                                                </g>
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <g opacity="1"
                                                        transform="matrix(1,0,0,1,857.9176025390625,352.1927795410156)">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                            fill-opacity="0" stroke="rgb(248,153,0)" stroke-opacity="1"
                                                            stroke-width="34.2"
                                                            d=" M0,0 C0,0 -105.69,-20.81 -105.69,-20.81 C-105.69,-20.81 -121.79,-123.84 -122.31,-127.15">
                                                        </path>
                                                        <g opacity="1"
                                                            transform="matrix(0.019690189510583878,-0.9998061060905457,0.9998061060905457,0.019690189510583878,2.4573898315429688,-124.77855682373047)">
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,594)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(34,39,71)" fill-opacity="1"
                                                        d=" M-110.97599792480469,112.23300170898438 C-110.97599792480469,112.23300170898438 -114.58200073242188,130.5970001220703 -114.58200073242188,130.5970001220703 C-114.58200073242188,130.5970001220703 -81.95999908447266,130.5970001220703 -81.95999908447266,130.5970001220703 C-81.95999908447266,127.13899993896484 -84.19000244140625,124.07599639892578 -87.48100280761719,123.01300048828125 C-87.48100280761719,123.01300048828125 -99.85299682617188,119.0199966430664 -99.85299682617188,119.0199966430664 C-99.85299682617188,119.0199966430664 -98.27100372314453,108.46499633789062 -98.27100372314453,108.46499633789062 C-98.27100372314453,108.46499633789062 -110.97599792480469,112.23300170898438 -110.97599792480469,112.23300170898438z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,594)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M-102.40799713134766,-86.52400207519531 C-102.40799713134766,-86.52400207519531 -104.89399719238281,-68.6729965209961 -106.16400146484375,-51.49800109863281 C-107.43299865722656,-34.321998596191406 -98.5,16.20400047302246 -98.5,16.20400047302246 C-98.5,16.20400047302246 -113.41100311279297,114.79900360107422 -113.41100311279297,114.79900360107422 C-113.41100311279297,114.79900360107422 -104.9209976196289,118.65299987792969 -101.01300048828125,117.53700256347656 C-97.1050033569336,116.41999816894531 -97.10399627685547,116.41999816894531 -97.10399627685547,116.41999816894531 C-97.10399627685547,116.41999816894531 -72.53900146484375,15.607000350952148 -72.53900146484375,15.607000350952148 C-72.53900146484375,15.607000350952148 -56.347999572753906,-75.60700225830078 -56.347999572753906,-75.60700225830078 C-56.347999572753906,-75.60700225830078 -102.40799713134766,-86.52400207519531 -102.40799713134766,-86.52400207519531z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,594)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(39,52,94)" fill-opacity="1"
                                                        d=" M-102.40799713134766,-86.52400207519531 C-102.40799713134766,-86.52400207519531 -104.89399719238281,-68.6729965209961 -106.16400146484375,-51.49800109863281 C-107.43299865722656,-34.321998596191406 -98.5,16.20400047302246 -98.5,16.20400047302246 C-98.5,16.20400047302246 -113.41100311279297,114.79900360107422 -113.41100311279297,114.79900360107422 C-113.41100311279297,114.79900360107422 -104.9209976196289,118.65299987792969 -101.01300048828125,117.53700256347656 C-97.1050033569336,116.41999816894531 -97.10399627685547,116.41999816894531 -97.10399627685547,116.41999816894531 C-97.10399627685547,116.41999816894531 -72.53900146484375,15.607000350952148 -72.53900146484375,15.607000350952148 C-72.53900146484375,15.607000350952148 -56.347999572753906,-75.60700225830078 -56.347999572753906,-75.60700225830078 C-56.347999572753906,-75.60700225830078 -102.40799713134766,-86.52400207519531 -102.40799713134766,-86.52400207519531z M-56.347999572753906,-75.60700225830078 C-56.347999572753906,-75.60700225830078 -101.66999816894531,-86.3499984741211 -101.66999816894531,-86.3499984741211 C-101.66999816894531,-86.3499984741211 -102.46299743652344,-86.1259994506836 -102.46299743652344,-86.1259994506836 C-102.63200378417969,-84.88899993896484 -103.17900085449219,-80.80899810791016 -103.83499908447266,-75.23600006103516 C-103.83499908447266,-75.23600006103516 -73.52799987792969,-62.6150016784668 -73.52799987792969,-62.6150016784668 C-73.52799987792969,-62.6150016784668 -87.45099639892578,-5.5 -87.45099639892578,-5.5 C-87.45099639892578,-5.5 -81.41300201416016,52.025001525878906 -81.41300201416016,52.025001525878906 C-81.41300201416016,52.025001525878906 -72.53900146484375,15.607000350952148 -72.53900146484375,15.607000350952148 C-72.53900146484375,15.607000350952148 -56.347999572753906,-75.60700225830078 -56.347999572753906,-75.60700225830078z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999998331069946,-0.0005581280565820634,0.0005581280565820634,1.7999998331069946,787.0414428710938,593.9624633789062)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(249,194,0)" fill-opacity="1"
                                                        d=" M-67.5770034790039,-223.35499572753906 C-67.5770034790039,-223.35499572753906 -96.16200256347656,-214.39500427246094 -96.16200256347656,-214.39500427246094 C-98.4540023803711,-213.67599487304688 -102.31999969482422,-210.66299438476562 -102.4000015258789,-207.99000549316406 C-102.4000015258789,-207.99000549316406 -106.16400146484375,-82.4520034790039 -106.16400146484375,-82.4520034790039 C-97.26899719238281,-78.88300323486328 -83.99099731445312,-74.7979965209961 -67.51399993896484,-74.10199737548828 C-42.720001220703125,-73.05400085449219 -23.437000274658203,-80.21900177001953 -13.277000427246094,-84.88800048828125 C-15.690999984741211,-127.09400177001953 -18.104999542236328,-169.30099487304688 -20.51799964904785,-211.5070037841797 C-20.51799964904785,-211.5070037841797 -46.970001220703125,-223.03700256347656 -46.970001220703125,-223.03700256347656 C-46.970001220703125,-223.03700256347656 -67.5770034790039,-223.35499572753906 -67.5770034790039,-223.35499572753906z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999998331069946,-0.0005581280565820634,0.0005581280565820634,1.7999998331069946,787.0414428710938,593.9624633789062)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(237,182,7)" fill-opacity="1"
                                                        d=" M-13.277000427246094,-84.88700103759766 C-13.994000434875488,-97.42400360107422 -14.711000442504883,-109.96099853515625 -15.428000450134277,-122.49700164794922 C-15.428000450134277,-122.49700164794922 -51.89099884033203,-117.66600036621094 -51.89099884033203,-117.66600036621094 C-51.89099884033203,-117.66600036621094 -60.53099822998047,-86.52400207519531 -60.53099822998047,-86.52400207519531 C-60.53099822998047,-86.52400207519531 -77.33300018310547,-88.49500274658203 -77.33300018310547,-88.49500274658203 C-77.33300018310547,-88.49500274658203 -69.10399627685547,-142.2209930419922 -69.10399627685547,-142.2209930419922 C-69.10399627685547,-142.2209930419922 -104.03900146484375,-153.32400512695312 -104.03900146484375,-153.32400512695312 C-104.03900146484375,-153.32400512695312 -106.16400146484375,-82.4520034790039 -106.16400146484375,-82.4520034790039 C-97.26899719238281,-78.88300323486328 -83.99099731445312,-74.7979965209961 -67.51399993896484,-74.10199737548828 C-42.720001220703125,-73.05500030517578 -23.437000274658203,-80.21900177001953 -13.277000427246094,-84.88700103759766z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,677.8963012695312,164.9952392578125)"
                                                opacity="1" style="display: block;"></g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-28.038999557495117,-288.8039855957031 C-28.038999557495117,-288.8039855957031 -24.104999542236328,-287.3340148925781 -24.378000259399414,-277.9429931640625 C-24.507999420166016,-273.4880065917969 -26.173999786376953,-265.05999755859375 -26.173999786376953,-265.05999755859375 C-26.173999786376953,-265.05999755859375 -36.430999755859375,-291.2510070800781 -36.430999755859375,-291.2510070800781 C-36.430999755859375,-291.2510070800781 -28.038999557495117,-288.8039855957031 -28.038999557495117,-288.8039855957031z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(165,91,58)" fill-opacity="1"
                                                        d=" M-27.55900001525879,-239.6580047607422 C-35.66999816894531,-222.59300231933594 -55.801998138427734,-232.67300415039062 -64.42400360107422,-241.27699279785156 C-73.04499816894531,-249.88099670410156 -71.85399627685547,-282.7659912109375 -69.46700286865234,-288.9930114746094 C-67.08000183105469,-295.22100830078125 -47.154998779296875,-300.2760009765625 -36.645999908447266,-296.2560119628906 C-26.13599967956543,-292.2359924316406 -19.44700050354004,-256.7229919433594 -27.55900001525879,-239.6580047607422z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-58.15800094604492,-268.1679992675781 C-58.15800094604492,-268.1679992675781 -55.91899871826172,-249.27999877929688 -50.74800109863281,-247.76800537109375 C-46.32699966430664,-246.47500610351562 -40.18199920654297,-252.7899932861328 -32.2140007019043,-253.12899780273438 C-27.7810001373291,-253.31700134277344 -26.18199920654297,-252.89700317382812 -24.378000259399414,-251.67599487304688 C-24.905000686645508,-247.1999969482422 -25.933000564575195,-243.06100463867188 -27.551000595092773,-239.6580047607422 C-35.66299819946289,-222.59300231933594 -55.79499816894531,-232.67300415039062 -64.41600036621094,-241.27699279785156 C-68.58899688720703,-245.44200134277344 -70.46299743652344,-255.29400634765625 -71.01499938964844,-265.05999755859375 C-71.01499938964844,-265.05999755859375 -58.15800094604492,-268.1679992675781 -58.15800094604492,-268.1679992675781z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-61.54199981689453,-261.39599609375 C-61.54199981689453,-261.39599609375 -59.055999755859375,-261.1499938964844 -58.03799819946289,-262.6300048828125 C-57.020999908447266,-264.1090087890625 -59.9379997253418,-272.4590148925781 -56.99300003051758,-281.3160095214844 C-54.88999938964844,-287.6369934082031 -45.3380012512207,-291.29998779296875 -37.52199935913086,-289.7640075683594 C-29.704999923706055,-288.22900390625 -27.736000061035156,-287.5400085449219 -26.937999725341797,-289.8110046386719 C-26.139999389648438,-292.0820007324219 -29.274999618530273,-297.4209899902344 -31.988000869750977,-299.5400085449219 C-34.70000076293945,-301.65899658203125 -47.415000915527344,-305.38800048828125 -58.0099983215332,-301.06500244140625 C-68.60600280761719,-296.7430114746094 -70.7249984741211,-293.8609924316406 -73.0979995727539,-286.9949951171875 C-75.47200012207031,-280.1289978027344 -73.94599914550781,-262.9219970703125 -73.0979995727539,-261.82000732421875 C-72.25,-260.7179870605469 -61.54199981689453,-261.39599609375 -61.54199981689453,-261.39599609375z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(165,91,58)" fill-opacity="1"
                                                        d=" M-73.52799987792969,-262.79901123046875 C-73.52799987792969,-258.39898681640625 -67.68099975585938,-254.49200439453125 -64.09200286865234,-254.49200439453125 C-60.50299835205078,-254.49200439453125 -60.53099822998047,-258.39898681640625 -60.53099822998047,-262.79901123046875 C-60.53099822998047,-267.1990051269531 -63.441001892089844,-270.7669982910156 -67.02999877929688,-270.7669982910156 C-70.61900329589844,-270.7669982910156 -73.52799987792969,-267.1990051269531 -73.52799987792969,-262.79901123046875z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-24.378000259399414,-251.67599487304688 C-24.378000259399414,-251.67599487304688 -24.402999877929688,-230.9949951171875 -26.81100082397461,-227.26600646972656 C-29.2189998626709,-223.53700256347656 -37.24300003051758,-222.7259979248047 -40.85900115966797,-224.28199768066406 C-44.47600173950195,-225.83799743652344 -50.35300064086914,-231.44700622558594 -51.821998596191406,-236.1929931640625 C-53.29199981689453,-240.94000244140625 -30.39699935913086,-247.718994140625 -30.39699935913086,-247.718994140625 C-30.39699935913086,-247.718994140625 -24.378000259399414,-251.67599487304688 -24.378000259399414,-251.67599487304688z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(142,74,47)" fill-opacity="1"
                                                        d=" M-71.15499877929688,-264.85400390625 C-71.5719985961914,-262.8840026855469 -69.32499694824219,-260.58099365234375 -67.71900177001953,-260.2409973144531 C-66.11299896240234,-259.8999938964844 -69.56400299072266,-262.59100341796875 -69.14700317382812,-264.5610046386719 C-68.72899627685547,-266.531005859375 -65.88400268554688,-267.4649963378906 -67.48999786376953,-267.80499267578125 C-69.09600067138672,-268.14599609375 -70.73699951171875,-266.82501220703125 -71.15499877929688,-264.85400390625z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-18.656999588012695,-263.8349914550781 C-18.656999588012695,-263.8349914550781 -23.95400047302246,-263.8349914550781 -23.95400047302246,-263.8349914550781 C-23.95400047302246,-263.8349914550781 -23.95400047302246,-264.8349914550781 -23.95400047302246,-264.8349914550781 C-23.95400047302246,-264.8349914550781 -18.656999588012695,-264.8349914550781 -18.656999588012695,-264.8349914550781 C-18.656999588012695,-264.8349914550781 -18.656999588012695,-263.8349914550781 -18.656999588012695,-263.8349914550781z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-24.909000396728516,-253.843994140625 C-28.632999420166016,-253.843994140625 -31.66200065612793,-257.7510070800781 -31.66200065612793,-262.5539855957031 C-31.66200065612793,-267.3580017089844 -28.632999420166016,-271.2650146484375 -24.909000396728516,-271.2650146484375 C-21.18600082397461,-271.2650146484375 -18.156999588012695,-267.3580017089844 -18.156999588012695,-262.5539855957031 C-18.156999588012695,-257.7510070800781 -21.18600082397461,-253.843994140625 -24.909000396728516,-253.843994140625z M-24.909000396728516,-270.2650146484375 C-28.08099937438965,-270.2650146484375 -30.66200065612793,-266.8059997558594 -30.66200065612793,-262.5539855957031 C-30.66200065612793,-258.3030090332031 -28.08099937438965,-254.843994140625 -24.909000396728516,-254.843994140625 C-21.73699951171875,-254.843994140625 -19.156999588012695,-258.3030090332031 -19.156999588012695,-262.5539855957031 C-19.156999588012695,-266.8059997558594 -21.73699951171875,-270.2650146484375 -24.909000396728516,-270.2650146484375z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-40.54100036621094,-253.843994140625 C-44.26499938964844,-253.843994140625 -47.29399871826172,-257.7510070800781 -47.29399871826172,-262.5539855957031 C-47.29399871826172,-267.3580017089844 -44.26499938964844,-271.2650146484375 -40.54100036621094,-271.2650146484375 C-36.81700134277344,-271.2650146484375 -33.78799819946289,-267.3580017089844 -33.78799819946289,-262.5539855957031 C-33.78799819946289,-257.7510070800781 -36.81700134277344,-253.843994140625 -40.54100036621094,-253.843994140625z M-40.54100036621094,-270.2650146484375 C-43.7130012512207,-270.2650146484375 -46.29399871826172,-266.8059997558594 -46.29399871826172,-262.5539855957031 C-46.29399871826172,-258.3030090332031 -43.7130012512207,-254.843994140625 -40.54100036621094,-254.843994140625 C-37.36899948120117,-254.843994140625 -34.78799819946289,-258.3030090332031 -34.78799819946289,-262.5539855957031 C-34.78799819946289,-266.8059997558594 -37.36899948120117,-270.2650146484375 -40.54100036621094,-270.2650146484375z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-46.643001556396484,-263.8349914550781 C-46.643001556396484,-263.8349914550781 -60.53099822998047,-263.8349914550781 -60.53099822998047,-263.8349914550781 C-60.53099822998047,-263.8349914550781 -60.53099822998047,-264.8349914550781 -60.53099822998047,-264.8349914550781 C-60.53099822998047,-264.8349914550781 -46.643001556396484,-264.8349914550781 -46.643001556396484,-264.8349914550781 C-46.643001556396484,-264.8349914550781 -46.643001556396484,-263.8349914550781 -46.643001556396484,-263.8349914550781z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.8000136613845825,-0.0014511443441733718,0.0014511443441733718,1.8000136613845825,782.6424560546875,593.3143920898438)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M-31.16200065612793,-263.8349914550781 C-31.16200065612793,-263.8349914550781 -34.28799819946289,-263.8349914550781 -34.28799819946289,-263.8349914550781 C-34.28799819946289,-263.8349914550781 -34.28799819946289,-264.8349914550781 -34.28799819946289,-264.8349914550781 C-34.28799819946289,-264.8349914550781 -31.16200065612793,-264.8349914550781 -31.16200065612793,-264.8349914550781 C-31.16200065612793,-264.8349914550781 -31.16200065612793,-263.8349914550781 -31.16200065612793,-263.8349914550781z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.0000157356262207,-0.0008061978151090443,0.0008061978151090443,1.0000157356262207,798.2642822265625,600.1495971679688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(255,255,255)" fill-opacity="1"
                                                        d=" M-87.125,-449.125 C-87.125,-449.125 -86,-437.875 -76,-437.875 C-67.25,-437.875 -66.125,-447.75 -66.125,-447.75 C-66.125,-447.75 -87.125,-449.125 -87.125,-449.125z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.0000157356262207,-0.0008061978151090443,0.0008061978151090443,1.0000157356262207,800.2483520507812,599.9008178710938)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(165,91,58)" fill-opacity="1"
                                                        d=" M-99,-492.875 C-99,-492.875 -93.25,-498.625 -81.25,-496.375">
                                                    </path>
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        fill-opacity="0" stroke="rgb(26,34,73)" stroke-opacity="1"
                                                        stroke-width="4"
                                                        d=" M-99,-492.875 C-99,-492.875 -93.25,-498.625 -81.25,-496.375">
                                                    </path>
                                                </g>
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(165,91,58)" fill-opacity="1"
                                                        d=" M-73.375,-496.375 C-73.375,-496.375 -67.5,-499.625 -61.125,-497.75">
                                                    </path>
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        fill-opacity="0" stroke="rgb(26,34,73)" stroke-opacity="1"
                                                        stroke-width="4"
                                                        d=" M-73.375,-496.375 C-73.375,-496.375 -67.5,-499.625 -61.125,-497.75">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.0000078678131104,-0.0003100736066699028,0.0003100736066699028,1.0000078678131104,800.0429077148438,599.9578247070312)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(249,194,0)" fill-opacity="1"
                                                        d=" M-43,-229 C-43,-229 -106,-222 -106,-222 C-106,-222 -128.5,-163.5 -128.5,-163.5 C-128.5,-163.5 -89,-161 -48.5,-171.5 C-39,-173 -39.5,-173.5 -39.5,-173.5 C-39.5,-173.5 -43,-229 -43,-229z">
                                                    </path>
                                                    <path stroke-linecap="butt" stroke-linejoin="miter" fill-opacity="0"
                                                        stroke-miterlimit="4" stroke="rgb(255,255,255)"
                                                        stroke-opacity="1" stroke-width="6" d="M0 0"></path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(255,147,152)" fill-opacity="1"
                                                        d=" M49.229000091552734,150.38099670410156 C49.229000091552734,150.38099670410156 107.84500122070312,203.8249969482422 107.84500122070312,203.8249969482422 C107.84500122070312,203.8249969482422 107.84500122070312,301.89599609375 107.84500122070312,301.89599609375 C107.84500122070312,301.89599609375 34.819000244140625,271.06201171875 34.819000244140625,271.06201171875 C34.819000244140625,271.06201171875 49.229000091552734,150.38099670410156 49.229000091552734,150.38099670410156z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(244,130,139)" fill-opacity="1"
                                                        d=" M49.229000091552734,150.38099670410156 C49.229000091552734,150.38099670410156 107.84500122070312,203.8249969482422 107.84500122070312,203.8249969482422 C107.84500122070312,203.8249969482422 107.84500122070312,301.89599609375 107.84500122070312,301.89599609375 C107.84500122070312,301.89599609375 34.819000244140625,271.06201171875 34.819000244140625,271.06201171875 C34.819000244140625,271.06201171875 49.229000091552734,150.38099670410156 49.229000091552734,150.38099670410156z M80.01499938964844,178.4510040283203 C80.01499938964844,178.4510040283203 61.25,282.28399658203125 61.25,282.28399658203125 C61.25,282.28399658203125 34.819000244140625,271.06201171875 34.819000244140625,271.06201171875 C34.819000244140625,271.06201171875 49.229000091552734,150.38099670410156 49.229000091552734,150.38099670410156 C49.229000091552734,150.38099670410156 80.01499938964844,178.4510040283203 80.01499938964844,178.4510040283203z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(255,147,152)" fill-opacity="1"
                                                        d=" M-52.35900115966797,150.38099670410156 C-52.35900115966797,150.38099670410156 -110.97599792480469,203.8249969482422 -110.97599792480469,203.8249969482422 C-110.97599792480469,203.8249969482422 -110.97599792480469,301.89599609375 -110.97599792480469,301.89599609375 C-110.97599792480469,301.89599609375 -37.95000076293945,271.06201171875 -37.95000076293945,271.06201171875 C-37.95000076293945,271.06201171875 -52.35900115966797,150.38099670410156 -52.35900115966797,150.38099670410156z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(244,130,139)" fill-opacity="1"
                                                        d=" M-52.35900115966797,150.38099670410156 C-52.35900115966797,150.38099670410156 -110.97599792480469,203.8249969482422 -110.97599792480469,203.8249969482422 C-110.97599792480469,203.8249969482422 -110.97599792480469,301.89599609375 -110.97599792480469,301.89599609375 C-110.97599792480469,301.89599609375 -37.95000076293945,271.06201171875 -37.95000076293945,271.06201171875 C-37.95000076293945,271.06201171875 -52.35900115966797,150.38099670410156 -52.35900115966797,150.38099670410156z M-52.35900115966797,150.38099670410156 C-52.35900115966797,150.38099670410156 -87.45099639892578,182.5 -87.45099639892578,182.5 C-87.45099639892578,182.5 -75.75,287 -75.75,287 C-75.75,287 -37.95000076293945,271.06201171875 -37.95000076293945,271.06201171875 C-37.95000076293945,271.06201171875 -52.35900115966797,150.38099670410156 -52.35900115966797,150.38099670410156z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(226,226,226)" fill-opacity="1"
                                                        d=" M-48.4640007019043,-106.24500274658203 C-65.625,-63.4630012512207 -80.9219970703125,-8.524999618530273 -80.9219970703125,51.39500045776367 C-80.9219970703125,147.03599548339844 -63.472999572753906,229.98300170898438 -37.95000076293945,271.06201171875 C-37.95000076293945,271.06201171875 34.819000244140625,271.06201171875 34.819000244140625,271.06201171875 C60.34199905395508,229.98300170898438 77.79100036621094,147.03599548339844 77.79100036621094,51.39500045776367 C77.79100036621094,-8.524999618530273 62.49399948120117,-63.4630012512207 45.33300018310547,-106.24500274658203 C30.33300018310547,-103.28800201416016 14.623000144958496,-101.71299743652344 -1.565000057220459,-101.71299743652344 C-17.753000259399414,-101.71299743652344 -33.4640007019043,-103.28800201416016 -48.4640007019043,-106.24500274658203z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(248,248,248)" fill-opacity="1"
                                                        d=" M78.32099914550781,51.39500045776367 C78.32099914550781,-8.524999618530273 63.02399826049805,-63.4630012512207 45.862998962402344,-106.24400329589844 C30.863000869750977,-103.28800201416016 15.152000427246094,-101.71299743652344 -1.034999966621399,-101.71299743652344 C-4.618000030517578,-101.71299743652344 -8.173999786376953,-101.8030014038086 -11.706999778747559,-101.95500183105469 C-19.732999801635742,-66.8280029296875 -24.909000396728516,-28.791000366210938 -24.909000396728516,10.939000129699707 C-24.909000396728516,117.87999725341797 -8.140000343322754,212.62399291992188 17.656999588012695,271.06201171875 C17.656999588012695,271.06201171875 35.3489990234375,271.06201171875 35.3489990234375,271.06201171875 C60.87300109863281,229.98300170898438 78.32099914550781,147.03599548339844 78.32099914550781,51.39500045776367z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(255,147,152)" fill-opacity="1"
                                                        d=" M-9.270999908447266,301.89599609375 C-9.270999908447266,301.89599609375 6.140999794006348,301.89599609375 6.140999794006348,301.89599609375 C6.140999794006348,301.89599609375 6.140999794006348,167.89300537109375 6.140999794006348,167.89300537109375 C6.140999794006348,167.89300537109375 -9.270999908447266,167.89300537109375 -9.270999908447266,167.89300537109375 C-9.270999908447266,167.89300537109375 -9.270999908447266,301.89599609375 -9.270999908447266,301.89599609375z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(244,130,139)" fill-opacity="1"
                                                        d=" M-9.270999908447266,301.89599609375 C-9.270999908447266,301.89599609375 6.140999794006348,301.89599609375 6.140999794006348,301.89599609375 C6.140999794006348,301.89599609375 6.140999794006348,239.6219940185547 6.140999794006348,239.6219940185547 C6.140999794006348,239.6219940185547 -9.270999908447266,239.6219940185547 -9.270999908447266,239.6219940185547 C-9.270999908447266,239.6219940185547 -9.270999908447266,301.89599609375 -9.270999908447266,301.89599609375z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(169,194,220)" fill-opacity="1"
                                                        d=" M32.20899963378906,-43.19300079345703 C32.20899963378906,-24.540000915527344 17.08799934387207,-9.418999671936035 -1.565000057220459,-9.418999671936035 C-20.218000411987305,-9.418999671936035 -35.34000015258789,-24.540000915527344 -35.34000015258789,-43.19300079345703 C-35.34000015258789,-61.84600067138672 -20.218000411987305,-76.96900177001953 -1.565000057220459,-76.96900177001953 C17.08799934387207,-76.96900177001953 32.20899963378906,-61.84600067138672 32.20899963378906,-43.19300079345703z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M6.216000080108643,27.94099998474121 C6.216000080108643,32.237998962402344 2.7330000400543213,35.722999572753906 -1.565000057220459,35.722999572753906 C-5.86299991607666,35.722999572753906 -9.347000122070312,32.237998962402344 -9.347000122070312,27.94099998474121 C-9.347000122070312,23.64299964904785 -5.86299991607666,20.15999984741211 -1.565000057220459,20.15999984741211 C2.7330000400543213,20.15999984741211 6.216000080108643,23.64299964904785 6.216000080108643,27.94099998474121z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.5999755859375,590)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M6.216000080108643,59.064998626708984 C6.216000080108643,63.362998962402344 2.7330000400543213,66.84700012207031 -1.565000057220459,66.84700012207031 C-5.86299991607666,66.84700012207031 -9.347000122070312,63.362998962402344 -9.347000122070312,59.064998626708984 C-9.347000122070312,54.768001556396484 -5.86299991607666,51.284000396728516 -1.565000057220459,51.284000396728516 C2.7330000400543213,51.284000396728516 6.216000080108643,54.768001556396484 6.216000080108643,59.064998626708984z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7995394468307495,0.04071156680583954,-0.04071156680583954,1.7995394468307495,796.4573974609375,599.3685302734375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M-45.441001892089844,-129.43899536132812 C-31.63800048828125,-122.86499786376953 -16.80500030517578,-117.4540023803711 -1.121000051498413,-113.45099639892578 C14.564000129699707,-109.447998046875 30.176000595092773,-107.08899688720703 45.441001892089844,-106.24500274658203 C37.71200180053711,-164.6320037841797 22.12700080871582,-204.54299926757812 22.12700080871582,-204.54299926757812 C22.12700080871582,-204.54299926757812 -10.678000450134277,-176.98199462890625 -45.441001892089844,-129.43899536132812z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7995394468307495,0.04071156680583954,-0.04071156680583954,1.7995394468307495,796.4573974609375,599.3685302734375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(39,52,94)" fill-opacity="1"
                                                        d=" M-45.441001892089844,-129.43899536132812 C-31.63800048828125,-122.86499786376953 -16.80500030517578,-117.4540023803711 -1.121000051498413,-113.45099639892578 C14.564000129699707,-109.447998046875 30.176000595092773,-107.08899688720703 45.441001892089844,-106.24500274658203 C37.71200180053711,-164.6320037841797 22.12700080871582,-204.54299926757812 22.12700080871582,-204.54299926757812 C22.12700080871582,-204.54299926757812 -10.678000450134277,-176.98199462890625 -45.441001892089844,-129.43899536132812z M22.398000717163086,-204.54299926757812 C22.398000717163086,-204.54299926757812 22.30299949645996,-204.46099853515625 22.30299949645996,-204.46099853515625 C20.339000701904297,-202.79400634765625 -11.496999740600586,-175.49000549316406 -45.17100143432617,-129.43899536132812 C-34.28499984741211,-124.25399780273438 -22.749000549316406,-119.80599975585938 -10.668999671936035,-116.18900299072266 C-10.668999671936035,-116.18900299072266 22.398000717163086,-204.54299926757812 22.398000717163086,-204.54299926757812z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,800,600)" opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M188.5,305 C188.5,305 218.36900329589844,496.99700927734375 218.36900329589844,496.99700927734375 C218.36900329589844,496.99700927734375 251.36900329589844,495.99700927734375 251.36900329589844,495.99700927734375 C251.36900329589844,495.99700927734375 230.5,311 230.5,311 C230.5,311 188.5,305 188.5,305z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,800,600)" opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M248,146.5 C248,146.5 206.5,140 188,172.5 C189,231.5 189.18699645996094,312.25201416015625 189.18699645996094,312.25201416015625 C189.18699645996094,312.25201416015625 206.5,334.5 231,313.5 C236,269.5 248,146.5 248,146.5z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,784,596.7999877929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M99.16000366210938,275.16900634765625 C99.16000366210938,275.16900634765625 100.69200134277344,303.06298828125 100.69200134277344,303.06298828125 C100.69200134277344,303.06298828125 59.3380012512207,303.06298828125 59.3380012512207,303.06298828125 C59.3380012512207,298.7619934082031 62.112998962402344,294.9519958496094 66.20700073242188,293.63299560546875 C66.20700073242188,293.63299560546875 83.46099853515625,288.0719909667969 83.46099853515625,288.0719909667969 C83.46099853515625,288.0719909667969 82.88700103759766,275.16900634765625 82.88700103759766,275.16900634765625 C82.88700103759766,275.16900634765625 99.16000366210938,275.16900634765625 99.16000366210938,275.16900634765625z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,784,596.7999877929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M147.00100708007812,70.2490005493164 C147.00100708007812,70.2490005493164 150.6840057373047,79.9729995727539 146.25799560546875,94.24500274658203 C143.09100341796875,104.45999908447266 125.96399688720703,105.48100280761719 125.96399688720703,105.48100280761719 C125.96399688720703,105.48100280761719 98.16600036621094,183.07899475097656 98.16600036621094,183.07899475097656 C98.16600036621094,183.07899475097656 101.7699966430664,278.5400085449219 101.7699966430664,278.5400085449219 C101.7699966430664,278.5400085449219 92.85700225830078,284.1860046386719 86.68000030517578,282.135009765625 C80.50199890136719,280.0830078125 80.50199890136719,280.0830078125 80.50199890136719,280.0830078125 C80.50199890136719,280.0830078125 73.8499984741211,187.11099243164062 74.32499694824219,176.41600036621094 C74.8010025024414,165.7209930419922 86.68000030517578,70.2490005493164 86.68000030517578,70.2490005493164 C86.68000030517578,70.2490005493164 147.00100708007812,70.2490005493164 147.00100708007812,70.2490005493164z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,784,596.7999877929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(39,52,94)" fill-opacity="1"
                                                        d=" M75.66500091552734,162.4340057373047 C75.66500091552734,162.4340057373047 97.49299621582031,92 97.49299621582031,92 C97.49299621582031,92 130.31199645996094,79.09200286865234 130.31199645996094,79.09200286865234 C130.31199645996094,79.09200286865234 124.51499938964844,70.2490005493164 124.51499938964844,70.2490005493164 C124.51499938964844,70.2490005493164 86.67900085449219,70.2490005493164 86.67900085449219,70.2490005493164 C86.67900085449219,70.2490005493164 78.875,132.9739990234375 75.66500091552734,162.4340057373047z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.700006127357483,0.0005312300054356456,-0.0005312300054356456,1.700006127357483,797.1919555664062,590.8353881835938)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(251,173,131)" fill-opacity="1"
                                                        d=" M112.26499938964844,-50.9010009765625 C112.26499938964844,-50.9010009765625 112.60299682617188,-38.31800079345703 112.60299682617188,-38.31800079345703 C112.60299682617188,-38.31800079345703 114.0479965209961,-35.284000396728516 123.0719985961914,-34.47700119018555 C127.66300201416016,-34.06800079345703 130.93099975585938,-37.297000885009766 130.93099975585938,-37.297000885009766 C130.93099975585938,-37.297000885009766 132.71400451660156,-57.41999816894531 132.71400451660156,-57.41999816894531 C132.71400451660156,-57.41999816894531 112.26499938964844,-50.9010009765625 112.26499938964844,-50.9010009765625z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7000057697296143,0.0005312298308126628,-0.0005312298308126628,1.7000057697296143,797.1923828125,590.8353271484375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(230,141,92)" fill-opacity="1"
                                                        d=" M112.05999755859375,-48.612998962402344 C112.05999755859375,-48.612998962402344 112.56099700927734,-39.85599899291992 112.56099700927734,-39.85599899291992 C113.35700225830078,-39.66699981689453 114.24600219726562,-39.76499938964844 115.06600189208984,-40.077999114990234 C119.37699890136719,-41.724998474121094 118.85099792480469,-46.55799865722656 121.53900146484375,-55.37099838256836 C121.53900146484375,-55.37099838256836 112.05999755859375,-48.612998962402344 112.05999755859375,-48.612998962402344z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,800,600)" opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(251,173,130)" fill-opacity="1"
                                                        d=" M88.75,-138 C88.63099670410156,-137.64199829101562 91.73200225830078,-130.8280029296875 92.11599731445312,-129.94500732421875 C92.5,-129.06199645996094 93.3334732055664,-122.73603820800781 93.3334732055664,-122.73603820800781 C93.42537689208984,-122.19184112548828 93.2565689086914,-121.37378692626953 92.95674896240234,-120.91043090820312 C92.95674896240234,-120.91043090820312 91.29325103759766,-118.33956909179688 91.29325103759766,-118.33956909179688 C90.9934310913086,-117.87621307373047 90.44232177734375,-117.82577514648438 90.0633773803711,-118.22701263427734 C90.0633773803711,-118.22701263427734 82.9366226196289,-125.77298736572266 82.9366226196289,-125.77298736572266 C82.55767822265625,-126.17422485351562 81.8140869140625,-126.60379028320312 81.27719116210938,-126.73162078857422 C81.27719116210938,-126.73162078857422 77.97280883789062,-127.51837921142578 77.97280883789062,-127.51837921142578 C77.4359130859375,-127.64620971679688 77.18199157714844,-127.34052276611328 77.40613555908203,-126.83618927001953 C77.40613555908203,-126.83618927001953 82.59386444091797,-115.16381072998047 82.59386444091797,-115.16381072998047 C82.81800842285156,-114.65947723388672 83.3049087524414,-113.92163848876953 83.68045043945312,-113.51720428466797 C83.68045043945312,-113.51720428466797 89,-105.5 92.75,-103.75 C103,-100 104.36278533935547,-109.78865051269531 104.36278533935547,-109.78865051269531 C104.8524398803711,-110.04326629638672 105.18278503417969,-110.69303131103516 105.09999084472656,-111.23868560791016 C105.09999084472656,-111.23868560791016 107.5260009765625,-116.69300079345703 103.05000305175781,-124.75 C100.6259994506836,-129.11300659179688 94.98500061035156,-135.7169952392578 88.75,-138z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9960821270942688,-0.08843322843313217,0.08843322843313217,0.9960821270942688,809.9204711914062,608.421142578125)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(12,191,209)" fill-opacity="1"
                                                        d=" M108.25,-111.25 C108.25,-111.25 92.5,-104 92.5,-104 C92.5,-104 125.5,-11.75 125.5,-11.75 C125.5,-11.75 136,3 148,-3.75 C160,-10.5 157.5,-23.75 157.5,-23.75 C157.5,-23.75 108.25,-111.25 108.25,-111.25z">
                                                    </path>
                                                    <path stroke-linecap="butt" stroke-linejoin="miter" fill-opacity="0"
                                                        stroke-miterlimit="4" stroke="rgb(255,255,255)"
                                                        stroke-opacity="1" stroke-width="4" d="M0 0"></path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.809999942779541,0.0001059094283846207,-0.0001059094283846207,1.809999942779541,786.0482788085938,592.8077392578125)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(96,214,230)" fill-opacity="1"
                                                        d=" M112.60299682617188,-38.31800079345703 C112.60299682617188,-38.31800079345703 98.38800048828125,-37.762001037597656 91.85199737548828,-29.222999572753906 C87.0510025024414,2.6449999809265137 79.77400207519531,72.31600189208984 79.77400207519531,72.31600189208984 C79.77400207519531,72.31600189208984 132.73699951171875,92.8280029296875 152.29800415039062,72.31600189208984 C152.29800415039062,72.31600189208984 146.7830047607422,-23.386999130249023 146.7830047607422,-23.386999130249023 C146.5030059814453,-28.2450008392334 143.40699768066406,-32.48899841308594 138.86599731445312,-34.23899841308594 C138.86599731445312,-34.23899841308594 130.93099975585938,-37.297000885009766 130.93099975585938,-37.297000885009766 C130.93099975585938,-37.297000885009766 112.60299682617188,-38.31800079345703 112.60299682617188,-38.31800079345703z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.809999942779541,0.0001059094283846207,-0.0001059094283846207,1.809999942779541,786.0482788085938,592.8077392578125)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(80,202,211)" fill-opacity="1"
                                                        d=" M79.89800262451172,72.31600189208984 C79.89800262451172,72.31600189208984 90.42500305175781,76.39099884033203 103.822998046875,79.09200286865234 C103.822998046875,79.09200286865234 101.2030029296875,-16.645000457763672 101.2030029296875,-16.645000457763672 C101.2030029296875,-16.645000457763672 88.89800262451172,-13.237000465393066 88.89800262451172,-13.237000465393066 C88.89800262451172,-13.237000465393066 79.89800262451172,72.31600189208984 79.89800262451172,72.31600189208984z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0.00011578138219192624,-0.00011578138219192624,1,994,725)"
                                                opacity="1" style="display: block;"></g>
                                            <g transform="matrix(1.7500033378601074,0.0005468534654937685,-0.0005468534654937685,1.7500033378601074,790.742431640625,588.9328002929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M97.49299621582031,-57.915000915527344 C94.11199951171875,-57.915000915527344 91.36100006103516,-61.457000732421875 91.36100006103516,-65.81099700927734 C91.36100006103516,-70.16500091552734 94.11199951171875,-73.70500183105469 97.49299621582031,-73.70500183105469 C100.8740005493164,-73.70500183105469 103.625,-70.16500091552734 103.625,-65.81099700927734 C103.625,-61.457000732421875 100.8740005493164,-57.915000915527344 97.49299621582031,-57.915000915527344z M97.49299621582031,-72.70500183105469 C94.66300201416016,-72.70500183105469 92.36100006103516,-69.61299896240234 92.36100006103516,-65.81099700927734 C92.36100006103516,-62.00899887084961 94.66300201416016,-58.915000915527344 97.49299621582031,-58.915000915527344 C100.322998046875,-58.915000915527344 102.625,-62.00899887084961 102.625,-65.81099700927734 C102.625,-69.61299896240234 100.322998046875,-72.70500183105469 97.49299621582031,-72.70500183105469z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7500033378601074,0.0005468534654937685,-0.0005468534654937685,1.7500033378601074,790.742431640625,588.9328002929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(251,173,131)" fill-opacity="1"
                                                        d=" M97.93900299072266,-86.7300033569336 C97.93900299072266,-86.7300033569336 95.8290023803711,-65.77400207519531 98.7760009765625,-55.625 C101.72200012207031,-45.474998474121094 110.91500091552734,-41.183998107910156 119.0199966430664,-43.047000885009766 C122.03099822998047,-43.73899841308594 132.9320068359375,-55.99300003051758 134.30799865722656,-71.13500213623047 C135.68299865722656,-86.27799987792969 134.3719940185547,-90.84100341796875 131.90899658203125,-94.04100036621094 C129.4459991455078,-97.24099731445312 107.06600189208984,-101.23100280761719 103.0009994506836,-97.72799682617188 C98.93699645996094,-94.2239990234375 97.93900299072266,-86.7300033569336 97.93900299072266,-86.7300033569336z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7500033378601074,0.0005468534654937685,-0.0005468534654937685,1.7500033378601074,790.742431640625,588.9328002929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(96,214,230)" fill-opacity="1"
                                                        d=" M129.4250030517578,-70.7239990234375 C129.4250030517578,-68.69999694824219 131.0679931640625,-67.05699920654297 133.0919952392578,-67.05699920654297 C135.11700439453125,-67.05699920654297 136.75900268554688,-68.69999694824219 136.75900268554688,-70.7239990234375 C136.75900268554688,-72.7490005493164 135.11700439453125,-74.39099884033203 133.0919952392578,-74.39099884033203 C131.0679931640625,-74.39099884033203 129.4250030517578,-72.7490005493164 129.4250030517578,-70.7239990234375z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7500033378601074,0.0005468534654937685,-0.0005468534654937685,1.7500033378601074,790.742431640625,588.9328002929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M113.2699966430664,-59.347999572753906 C109.5459976196289,-59.347999572753906 106.51699829101562,-63.255001068115234 106.51699829101562,-68.05899810791016 C106.51699829101562,-72.86199951171875 109.5459976196289,-76.76899719238281 113.2699966430664,-76.76899719238281 C116.99400329589844,-76.76899719238281 120.02200317382812,-72.86199951171875 120.02200317382812,-68.05899810791016 C120.02200317382812,-63.255001068115234 116.99400329589844,-59.347999572753906 113.2699966430664,-59.347999572753906z M113.2699966430664,-75.76899719238281 C110.0979995727539,-75.76899719238281 107.51699829101562,-72.30999755859375 107.51699829101562,-68.05899810791016 C107.51699829101562,-63.80699920654297 110.0979995727539,-60.347999572753906 113.2699966430664,-60.347999572753906 C116.44200134277344,-60.347999572753906 119.02200317382812,-63.80699920654297 119.02200317382812,-68.05899810791016 C119.02200317382812,-72.30999755859375 116.44200134277344,-75.76899719238281 113.2699966430664,-75.76899719238281z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7500033378601074,0.0005468534654937685,-0.0005468534654937685,1.7500033378601074,790.742431640625,588.9328002929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M134.86500549316406,-97.97599792480469 C134.86500549316406,-97.97599792480469 139.2310028076172,-92.90899658203125 138.84800720214844,-80.31700134277344 C138.46400451660156,-67.7249984741211 135.59300231933594,-58.404998779296875 131.9550018310547,-54.54600143432617 C128.31700134277344,-50.6870002746582 115.74600219726562,-50.87799835205078 111.88500213623047,-52.983001708984375 C108.02400207519531,-55.0890007019043 100.93900299072266,-63.22200012207031 101.3219985961914,-68.05899810791016 C101.48600006103516,-70.13500213623047 101.68599700927734,-72.83599853515625 101.86000061035156,-75.24400329589844 C102.10600280761719,-78.63600158691406 99.8550033569336,-81.7020034790039 96.54499816894531,-82.48500061035156 C96.54499816894531,-82.48500061035156 95.00399780273438,-82.85099792480469 95.00399780273438,-82.85099792480469 C95.00399780273438,-82.85099792480469 94.0469970703125,-95.29499816894531 100.74800109863281,-100.46399688720703 C107.4489974975586,-105.63400268554688 115.48999786376953,-106.01599884033203 123.53099822998047,-103.91000366210938 C131.57200622558594,-101.80500030517578 134.86500549316406,-97.97599792480469 134.86500549316406,-97.97599792480469z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7500033378601074,0.0005468534654937685,-0.0005468534654937685,1.7500033378601074,790.742431640625,588.9328002929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(251,173,131)" fill-opacity="1"
                                                        d=" M111.3030014038086,-62.729000091552734 C111.3030014038086,-59.75 107.34400177001953,-57.10499954223633 104.91300201416016,-57.10499954223633 C102.48300170898438,-57.10499954223633 102.50299835205078,-59.75 102.50299835205078,-62.729000091552734 C102.50299835205078,-65.70800018310547 104.47200012207031,-68.125 106.90299987792969,-68.125 C109.33300018310547,-68.125 111.3030014038086,-65.70800018310547 111.3030014038086,-62.729000091552734z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7500033378601074,0.0005468534654937685,-0.0005468534654937685,1.7500033378601074,790.742431640625,588.9328002929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(230,141,92)" fill-opacity="1"
                                                        d=" M109.38999938964844,-64.0459976196289 C109.10700225830078,-65.50700378417969 106.91400146484375,-66.4260025024414 105.7239990234375,-66.19499969482422 C104.53299713134766,-65.96399688720703 107.65599822998047,-65.12899780273438 107.93900299072266,-63.667999267578125 C108.22200012207031,-62.20800018310547 106.55599975585938,-60.75299835205078 107.74600219726562,-60.983001708984375 C108.93699645996094,-61.2140007019043 109.67400360107422,-62.58599853515625 109.38999938964844,-64.0459976196289z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7500033378601074,0.0005468534654937685,-0.0005468534654937685,1.7500033378601074,790.742431640625,588.9328002929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M118.45099639892578,-88.49500274658203 C118.45099639892578,-78.66999816894531 126.41600036621094,-70.70600128173828 136.24099731445312,-70.70600128173828 C146.06500244140625,-70.70600128173828 154.02999877929688,-78.66999816894531 154.02999877929688,-88.49500274658203 C154.02999877929688,-98.31900024414062 146.06500244140625,-106.28399658203125 136.24099731445312,-106.28399658203125 C126.41600036621094,-106.28399658203125 118.45099639892578,-98.31900024414062 118.45099639892578,-88.49500274658203z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7500033378601074,0.0005468534654937685,-0.0005468534654937685,1.7500033378601074,790.742431640625,588.9328002929688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M104.25800323486328,-67.03399658203125 C104.25800323486328,-67.03399658203125 91.86100006103516,-67.03399658203125 91.86100006103516,-67.03399658203125 C91.86100006103516,-67.03399658203125 91.86100006103516,-68.03399658203125 91.86100006103516,-68.03399658203125 C91.86100006103516,-68.03399658203125 104.25800323486328,-68.03399658203125 104.25800323486328,-68.03399658203125 C104.25800323486328,-68.03399658203125 104.25800323486328,-67.03399658203125 104.25800323486328,-67.03399658203125z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.0000077486038208,-0.0006201472133398056,0.0006201472133398056,1.0000077486038208,800.1674194335938,599.9237060546875)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(165,91,58)" fill-opacity="1"
                                                        d=" M-137.75,-410.75 C-133.25,-405 -122.25,-400.5 -116,-400.5 C-104,-400.5 -98.05999755859375,-409.37200927734375 -98.05999755859375,-409.37200927734375 C-98.05999755859375,-409.37200927734375 -114.63200378417969,-422.75299072265625 -137.75,-410.75z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.6998882293701172,-0.019496982917189598,0.019496982917189598,1.6998882293701172,782.0978393554688,591.09521484375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M20.28499984741211,-43.19300079345703 C20.28499984741211,-24.540000915527344 5.163000106811523,-9.418999671936035 -13.489999771118164,-9.418999671936035 C-32.143001556396484,-9.418999671936035 -47.26499938964844,-24.540000915527344 -47.26499938964844,-43.19300079345703 C-47.26499938964844,-61.84600067138672 -32.143001556396484,-76.96900177001953 -13.489999771118164,-76.96900177001953 C5.163000106811523,-76.96900177001953 20.28499984741211,-61.84600067138672 20.28499984741211,-43.19300079345703z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.6998882293701172,-0.019496982917189598,0.019496982917189598,1.6998882293701172,782.0978393554688,591.09521484375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(39,52,94)" fill-opacity="1"
                                                        d=" M20.28499984741211,-43.19300079345703 C20.28499984741211,-24.540000915527344 5.163000106811523,-9.418999671936035 -13.489999771118164,-9.418999671936035 C-32.143001556396484,-9.418999671936035 -47.26499938964844,-24.540000915527344 -47.26499938964844,-43.19300079345703 C-47.26499938964844,-61.84600067138672 -32.143001556396484,-76.96900177001953 -13.489999771118164,-76.96900177001953 C5.163000106811523,-76.96900177001953 20.28499984741211,-61.84600067138672 20.28499984741211,-43.19300079345703z M-47.22999954223633,-44.55500030517578 C-47.24800109863281,-44.10300064086914 -47.26499938964844,-43.650001525878906 -47.26499938964844,-43.19300079345703 C-47.26499938964844,-24.540000915527344 -32.143001556396484,-9.418999671936035 -13.489999771118164,-9.418999671936035 C-3.5429999828338623,-9.418999671936035 5.396999835968018,-13.720000267028809 11.57800006866455,-20.562999725341797 C11.57800006866455,-20.562999725341797 -47.22999954223633,-44.55500030517578 -47.22999954223633,-44.55500030517578z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.6998882293701172,-0.019496982917189598,0.019496982917189598,1.6998882293701172,782.0978393554688,591.09521484375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(251,173,131)" fill-opacity="1"
                                                        d=" M7.263000011444092,-7.609000205993652 C7.263000011444092,-7.609000205993652 1.3919999599456787,-12.906000137329102 1.3919999599456787,-12.906000137329102 C-0.32100000977516174,-12.032999992370605 -2.634000062942505,-11.0600004196167 -5.479000091552734,-10.406999588012695 C-7.940999984741211,-9.842000007629395 -10.133000373840332,-9.666999816894531 -11.883000373840332,-9.651000022888184 C-12.286999702453613,-13.911999702453613 -13.668000221252441,-15.23799991607666 -14.722999572753906,-15.968999862670898 C-16.285999298095703,-17.054000854492188 -19.316999435424805,-16.506999969482422 -19.316999435424805,-16.506999969482422 C-19.316999435424805,-16.506999969482422 -17.274999618530273,-7.544000148773193 -15.902999877929688,-4.354000091552734 C-14.531000137329102,-1.1629999876022339 -2.7890000343322754,-0.20600000023841858 -0.8740000128746033,-0.460999995470047 C1.0399999618530273,-0.7170000076293945 1.934000015258789,-0.014999999664723873 1.934000015258789,-0.014999999664723873 C1.934000015258789,-0.014999999664723873 7.263000011444092,-7.609000205993652 7.263000011444092,-7.609000205993652z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,800,600)" opacity="1" style="display: block;">
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,789.5230712890625,584.5092163085938)">
                                                    <g opacity="1"
                                                        transform="matrix(0.5680856108665466,-0.8229694366455078,-0.8229694366455078,-0.5680856108665466,70.16372680664062,-101.64419555664062)">
                                                    </g>
                                                    <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                        <g class="04" opacity="1" transform="matrix(1,0,0,1,0,0)"></g>
                                                    </g>
                                                    <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                        <g opacity="1" transform="matrix(1,0,0,1,-200,-200)"></g>
                                                    </g>
                                                    <g opacity="1" transform="matrix(1,0,0,1,-135,0)">
                                                        <g opacity="1" transform="matrix(1,0,0,1,0,0)"></g>
                                                    </g>
                                                </g>
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <g opacity="1"
                                                        transform="matrix(1,0,0,1,789.5230712890625,584.5092163085938)">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                            fill-opacity="0" stroke="rgb(96,125,139)" stroke-opacity="1"
                                                            stroke-width="6"
                                                            d=" M0,0 C0,0 97.2,45.9 97.2,45.9 C97.2,45.9 169.43,-29.01 171.75,-31.42">
                                                        </path>
                                                        <g opacity="1"
                                                            transform="matrix(0.5680856108665466,-0.8229694366455078,-0.8229694366455078,-0.5680856108665466,70.16372680664062,-101.64419555664062)">
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.0315287113189697,-0.01880135014653206,0.01880135014653206,1.0315287113189697,962.5873413085938,551.7257690429688)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(96,214,230)" fill-opacity="1"
                                                        d=" M-5.105999946594238,-15.770999908447266 C-5.105999946594238,-15.770999908447266 -65.73899841308594,30.395000457763672 -84.23899841308594,56.64500045776367 C-105.23600006103516,86.43800354003906 -71.23899841308594,83.1449966430664 -71.23899841308594,83.1449966430664 C-71.23899841308594,83.1449966430664 -4.007999897003174,51.540000915527344 14.25,15.25 C34.5,-25 -5.105999946594238,-15.770999908447266 -5.105999946594238,-15.770999908447266z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9998959898948669,-0.014418205246329308,0.014418205246329308,0.9998959898948669,881.140869140625,626.6929931640625)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(96,214,230)" fill-opacity="1"
                                                        d=" M-84.25,-52.25 C-84.25,-52.25 -95.25,-33.75 -95.25,-33.75 C-95.25,-33.75 -12.5,17.5 -3.5,17.25 C20.75200080871582,16.576000213623047 13.25,-3.5 13.25,-3.5 C13.25,-3.5 -84.25,-52.25 -84.25,-52.25z">
                                                    </path>
                                                    <path stroke-linecap="butt" stroke-linejoin="miter" fill-opacity="0"
                                                        stroke-miterlimit="4" stroke="rgb(255,255,255)"
                                                        stroke-opacity="1" stroke-width="6" d="M0 0"></path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.6989719867706299,-0.05911385640501976,0.05911385640501976,1.6989719867706299,744.36669921875,605.1090087890625)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(0,201,156)" fill-opacity="1"
                                                        d=" M-160.00399780273438,228.8820037841797 C-190.48699951171875,259.364990234375 -239.91000366210938,259.364990234375 -270.39300537109375,228.8820037841797 C-300.875,198.39999389648438 -300.875,148.9759979248047 -270.39300537109375,118.49400329589844 C-239.91000366210938,88.01100158691406 -190.48699951171875,88.01100158691406 -160.00399780273438,118.49400329589844 C-129.52200317382812,148.9759979248047 -129.52200317382812,198.39999389648438 -160.00399780273438,228.8820037841797z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.6989719867706299,-0.05911385640501976,0.05911385640501976,1.6989719867706299,744.36669921875,605.1090087890625)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M-212.8350067138672,193.8979949951172 C-212.8350067138672,193.8979949951172 -213.60800170898438,193.26600646972656 -213.60800170898438,193.26600646972656 C-213.60800170898438,193.26600646972656 -180.24000549316406,152.4239959716797 -180.24000549316406,152.4239959716797 C-180.24000549316406,152.4239959716797 -179.4669952392578,153.0570068359375 -179.4669952392578,153.0570068359375 C-179.4669952392578,153.0570068359375 -212.8350067138672,193.8979949951172 -212.8350067138672,193.8979949951172z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.6989719867706299,-0.05911385640501976,0.05911385640501976,1.6989719867706299,744.36669921875,605.1090087890625)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M-217.36599731445312,167.7010040283203 C-217.36599731445312,167.7010040283203 -275.0740051269531,150.8509979248047 -275.0740051269531,150.8509979248047 C-275.0740051269531,150.8509979248047 -274.7929992675781,149.89199829101562 -274.7929992675781,149.89199829101562 C-274.7929992675781,149.89199829101562 -217.0850067138672,166.74200439453125 -217.0850067138672,166.74200439453125 C-217.0850067138672,166.74200439453125 -217.36599731445312,167.7010040283203 -217.36599731445312,167.7010040283203z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.6989719867706299,-0.05911385640501976,0.05911385640501976,1.6989719867706299,744.36669921875,605.1090087890625)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(48,61,114)" fill-opacity="1"
                                                        d=" M-197.92300415039062,302.07000732421875 C-197.92300415039062,302.07000732421875 -224.61700439453125,121.302001953125 -224.61700439453125,121.302001953125 C-224.61700439453125,121.302001953125 -223.6269989013672,121.15499877929688 -223.6269989013672,121.15499877929688 C-223.6269989013672,121.15499877929688 -196.93299865722656,301.92401123046875 -196.93299865722656,301.92401123046875 C-196.93299865722656,301.92401123046875 -197.92300415039062,302.07000732421875 -197.92300415039062,302.07000732421875z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,0,0)" opacity="1" style="display: block;">
                                                <g opacity="1"
                                                    transform="matrix(1,0,0,1,706.9075927734375,338.4644775390625)">
                                                    <g opacity="1"
                                                        transform="matrix(0.1135779544711113,-0.9935290813446045,0.9935290813446045,0.1135779544711113,11.484954833984375,-100.46524047851562)">
                                                    </g>
                                                    <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                        <g class="04" opacity="1" transform="matrix(1,0,0,1,0,0)"></g>
                                                    </g>
                                                    <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                        <g opacity="1" transform="matrix(1,0,0,1,-200,-200)"></g>
                                                    </g>
                                                    <g opacity="1" transform="matrix(1,0,0,1,-135,0)">
                                                        <g opacity="1" transform="matrix(1,0,0,1,0,0)"></g>
                                                    </g>
                                                </g>
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <g opacity="1"
                                                        transform="matrix(1,0,0,1,706.9075927734375,338.4644775390625)">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                            fill-opacity="0" stroke="rgb(249,194,0)" stroke-opacity="1"
                                                            stroke-width="25"
                                                            d=" M0,0 C0,0 -102.03,-10.24 -102.03,-10.24 C-102.03,-10.24 -89.4,-108.7 -88.99,-111.87">
                                                        </path>
                                                        <g opacity="1"
                                                            transform="matrix(0.1135779544711113,-0.9935290813446045,0.9935290813446045,0.1135779544711113,11.484954833984375,-100.46524047851562)">
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                            <g transform="matrix(1,0,0,1,800,600)" opacity="1" style="display: block;">
                                            </g>
                                            <g transform="matrix(0.999634861946106,0.02731122262775898,-0.02731122262775898,0.999634861946106,719.5753173828125,343.771484375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(165,91,58)" fill-opacity="1"
                                                        d=" M25.682004928588867,-17.637554168701172 C25.167631149291992,-17.837587356567383 24.339599609375,-17.820098876953125 23.834131240844727,-17.598522186279297 C23.834131240844727,-17.598522186279297 15.5,-15 6.5,-10 C-2.5,-5 -0.9243330955505371,11.554515838623047 -0.9243330955505371,11.554515838623047 C-1.104068636894226,12.07632827758789 -0.8476105332374573,12.697171211242676 -0.35200968384742737,12.940014839172363 C-0.35200968384742737,12.940014839172363 22.852008819580078,24.30998420715332 22.852008819580078,24.30998420715332 C23.347610473632812,24.55282974243164 24.176342010498047,24.8879337310791 24.701444625854492,25.057819366455078 C24.701444625854492,25.057819366455078 31.298555374145508,27.192180633544922 31.298555374145508,27.192180633544922 C31.823657989501953,27.3620662689209 32.65353775024414,27.30518913269043 33.150550842285156,27.065250396728516 C33.150550842285156,27.065250396728516 44,23.5 46.75,20.5 C49.5,17.5 46.5333137512207,13.999444961547852 46.5333137512207,13.999444961547852 C46.51492691040039,13.447851181030273 46.15009307861328,12.720073699951172 45.71913146972656,12.37530517578125 C45.71913146972656,12.37530517578125 22.280868530273438,-6.37530517578125 22.280868530273438,-6.37530517578125 C21.84990692138672,-6.72007417678833 21.787778854370117,-7.343477725982666 22.14221954345703,-7.7665205001831055 C22.14221954345703,-7.7665205001831055 28.60778045654297,-15.483479499816895 28.60778045654297,-15.483479499816895 C28.962221145629883,-15.906521797180176 28.832368850708008,-16.412412643432617 28.317995071411133,-16.612445831298828 C28.317995071411133,-16.612445831298828 25.682004928588867,-17.637554168701172 25.682004928588867,-17.637554168701172z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(0.9810176491737366,-0.13305726647377014,0.13305726647377014,0.9810176491737366,617.8058471679688,225.62217712402344)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(249,194,0)" fill-opacity="1"
                                                        d=" M6.25,-15.5 C-19.45599937438965,-21.566999435424805 -22.099000930786133,-7.453999996185303 -36.5,35.75 C-52.75,89.5 -48.75,104.5 -43,107.75 C-28,114.25 5,54.5 5,54.5 C5,54.5 46.5,-6 6.25,-15.5z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(-0.2665954828262329,-0.9638084769248962,0.9638084769248962,-0.2665954828262329,603.66650390625,327.43988037109375)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(249,194,0)" fill-opacity="1"
                                                        d=" M11.75100040435791,-17.059999465942383 C-1.6019999980926514,-22.68600082397461 -15.777999877929688,-14.234999656677246 -24.746999740600586,6.551000118255615 C-38.560001373291016,42.32600021362305 -56.30799865722656,97.81099700927734 -56.30799865722656,97.81099700927734 C-56.30799865722656,97.81099700927734 -52.145999908447266,112.41600036621094 -34.805999755859375,116.41000366210938 C-32.1150016784668,117.02999877929688 13.696999549865723,22.749000549316406 13.696999549865723,22.749000549316406 C13.696999549865723,22.749000549316406 30.93600082397461,-8.842000007629395 11.75100040435791,-17.059999465942383z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,-0.00021603124332614243,0.00021603124332614243,1.7999999523162842,801.724853515625,600.76025390625)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(165,91,58)" fill-opacity="1"
                                                        d=" M44.44900131225586,-146.36700439453125 C44.45000076293945,-148.17799377441406 43.36800003051758,-149.8090057373047 41.696998596191406,-150.5030059814453 C39.22999954223633,-151.5290069580078 35.595001220703125,-152.54200744628906 32.98099899291992,-151.10800170898438 C28.65399932861328,-148.73500061035156 31.16699981689453,-136.87100219726562 34.516998291015625,-134.22000122070312 C37.867000579833984,-131.5679931640625 41.91400146484375,-132.68499755859375 43.310001373291016,-134.22000122070312 C44.29899978637695,-135.30799865722656 44.446998596191406,-142.35400390625 44.44900131225586,-146.36700439453125z">
                                                    </path>
                                                </g>
                                            </g>
                                            <g transform="matrix(1.7999999523162842,0,0,1.7999999523162842,785.800048828125,592)"
                                                opacity="1" style="display: block;">
                                                <g opacity="1" transform="matrix(1,0,0,1,0,0)">
                                                    <path fill="rgb(26,34,73)" fill-opacity="1"
                                                        d=" M146.13099670410156,276.47198486328125 C146.13099670410156,276.47198486328125 150.47000122070312,295.65399169921875 150.47000122070312,295.65399169921875 C150.47000122070312,295.65399169921875 133.6219940185547,303.06298828125 133.6219940185547,303.06298828125 C133.6219940185547,303.06298828125 114.22200012207031,303.06298828125 114.22200012207031,303.06298828125 C114.22200012207031,299.87200927734375 116.2969970703125,297.04998779296875 119.34500122070312,296.10198974609375 C119.34500122070312,296.10198974609375 128.97500610351562,293.10198974609375 128.97500610351562,293.10198974609375 C128.97500610351562,293.10198974609375 132.3459930419922,277.5299987792969 132.3459930419922,277.5299987792969 C132.3459930419922,277.5299987792969 146.13099670410156,276.47198486328125 146.13099670410156,276.47198486328125z">
                                                    </path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- begin::ChartJS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.0.1/chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
<script>
    var ctx = document.getElementById('myChart');
    var dataX = <?= json_encode($bulan) ?>;
    var dataY = <?= json_encode($lhpperbulan)?>;
    
    // console.log($dataX);
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dataX,
            datasets: [{
                label: 'LHP',
                data: dataY,
                backgroundColor: [
                    // 'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [
                    // 'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                ],
                borderWidth: 2,
                pointStyle: 'circle',
                pointRadius: 10,
                pointHoverRadius: 15
            }]
        },
        options: {
            responsive: true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1
                    }
                }]
            },
            animations: {
                radius: {
                    duration: 400,
                    easing: 'linear',
                    loop: (context) => context.active
                }
            },
        }
    });
</script>
<!-- end::ChartJS -->

<!-- <div class="col-lg-4">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height animation-card">
                    <div class="iq-card-body p-0">

                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <img src="images/nyoba.gif" class="img-fluid mb-4" alt="logo">

                            </div>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <small class="text-muted mt-3 d-inline-block w-100">Aplikasi yang dikembangkan oleh Inspektorat
                            untuk mempermudah dalam mengelola data-data hasil pengawasan secara sistematis.</small>
                    </div>
                </div>
            </div> -->
@endsection