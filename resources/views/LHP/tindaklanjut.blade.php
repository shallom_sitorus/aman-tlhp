@extends('layouts.home')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @foreach ($reko as $trlhp)
                        <div class="iq-card">
                            <div class="iq-header-title">
                                <div style="padding-top: 15px; padding-left: 15px">
                                    <a href="{{ url('/detaillhp/' . $trlhp->tlhp->lhp->id) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#0084ff"
                                            class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                                        </svg>
                                    </a>
                                </div>
                                <h5 align="center" class="card-title-lhp">
                                    {{ $trlhp->tlhp->lhp->no_lhp }}<br>{{ date('d F Y', strtotime($trlhp->tlhp->lhp->tanggal_lhp)) }}
                                </h5>
                            </div>
                            <div class="detail">
                                <table width="100%">
                                    <tr>
                                        <th Width="23%">Entitas Pengawasan</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">
                                            <table>
                                                @foreach ($trlhp->tlhp->lhp->jadwalpengawasan->opd as $opd)
                                                    <tr>
                                                        <td>{{ $opd->nama }}</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Rekomendasi</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <b>{{ $trlhp->rekomendasi->nama_rekomendasi }}
                                                            ({{ $trlhp->rekomendasi->kode_rekomendasi }})
                                                        </b><br>
                                                        {{ $trlhp->uraian_rekomendasi }}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="iq-card-header d-flex justify-content-between">
                                <div class="iq-header-title">
                                    <h4 class="card-title">Tindak Lanjut</h4>
                                </div>
                                <div class="card-toolbarn">
                                    @if (Auth::user()->role_id == 2)
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#tambahTL" title="Tambah Tindak Lanjut">
                                            + Tambah
                                        </button>
                                    @endif
                                    @if (Auth::user()->role_id != 2)
                                        <button type="button" class="btn text-light" style="background: teal"
                                            data-toggle="modal" data-target="#tambahPenagihan1"
                                            title="Tambah Surat Penagihan1">
                                            + Surat Penagihan1
                                        </button>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-items-center mt-3">
                                <div class="col-sm-12 col-md-6">
                                    <div id="user_list_datatable_info" class="dataTables_filter">
                                        <input class="form-control me-2" type="search" placeholder="Cari.." name="search"
                                            aria-label="Search" id="searchInput">
                                    </div>
                                </div>
                            </div>
                            <div class="iq-card-body">
                                <div class="table-responsive">
                                    <table id="data-table" class="table table-striped table-bordered mt-1 middle"
                                        role="grid" aria-describedby="user-list-page-info">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>No&nbsp;dan&nbsp;Tanggal Surat&nbsp;Tindak&nbsp;Lanjut</th>
                                                <th>File Surat&nbsp;Tindak&nbsp;Lanjut</th>
                                                <th>Kategori&nbsp;atau&nbsp;Status Tindak&nbsp;Lanjut</th>
                                                <th class="px-5">Keterangan</th>
                                                <th>Surat Penagihan&nbsp;1</th>
                                                <th>Surat Penagihan&nbsp;2</th>
                                                <th>Surat Pemberitahuan</th>
                                                <th>Surat&nbsp;Permintaan Tindakan&nbsp;Tambahan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($trlhp->tindaklanjut as $tl)
                                                <tr>
                                                    <td>
                                                        <div class="d-flex justify-items-center ">
                                                            <a class="btn btn-light btn-sm mx-1" data-toggle="modal"
                                                                data-placement="top" title="Edit"
                                                                data-original-title="Edit"
                                                                data-target="#editTL{{ $tl->id }}"><span
                                                                    class="svg-icon svg-icon-3">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                                        height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                                                        <path opacity="0.3"
                                                                            d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                                                        <path
                                                                            d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                                                    </svg>
                                                                </span>
                                                            </a>
                                                            @if (Auth::user()->role_id != 2)
                                                                <form class="btn-delete"
                                                                    action="{{ url('/delete-tindaklanjut/' . $tl->id) }}"
                                                                    method="post">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <button type="submit" class="btn btn-light btn-sm mx-1"
                                                                        data-placement="top" title="Delete"
                                                                        data-original-title="Delete">
                                                                        <span class="svg-icon svg-icon-3"><svg
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                width="16" height="16"
                                                                                viewBox="0 0 24 24" fill="#f1416c">
                                                                                <path
                                                                                    d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" />
                                                                                <path opacity="0.5"
                                                                                    d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" />
                                                                                <path opacity="0.5"
                                                                                    d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" />
                                                                            </svg></span>
                                                                    </button>
                                                                </form>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td>{{ $tl->no_tl }} <br>
                                                        {{ $tl->tgl_surat }}
                                                    </td>
                                                    <td>
                                                        @if ($tl->file_surat_tl)
                                                            <a href="{{ asset('surat_tl/' . $tl->file_surat_tl) }}"
                                                                target="_blank"><svg xmlns="http://www.w3.org/2000/svg"
                                                                    width="24" fill="red" viewBox="0 0 384 512">
                                                                    <path
                                                                        d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM64 224H88c30.9 0 56 25.1 56 56s-25.1 56-56 56H80v32c0 8.8-7.2 16-16 16s-16-7.2-16-16V320 240c0-8.8 7.2-16 16-16zm24 80c13.3 0 24-10.7 24-24s-10.7-24-24-24H80v48h8zm72-64c0-8.8 7.2-16 16-16h24c26.5 0 48 21.5 48 48v64c0 26.5-21.5 48-48 48H176c-8.8 0-16-7.2-16-16V240zm32 112h8c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16h-8v96zm96-128h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v32h32c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V304 240c0-8.8 7.2-16 16-16z" />
                                                                </svg></a>
                                                        @else
                                                            Tidak Terlampir
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($tl->kategoritl)
                                                            <span
                                                                class="badge badge-pill badge-secondary">{{ $tl->kategoritl->nama_kategori }}</span>
                                                        @endif
                                                    </td>
                                                    <td style="text-align: justify">{{ $tl->keterangan }}</td>
                                                    <td>
                                                        @if ($tl->tgl_penagihan1)
                                                            {{ date('d F Y', strtotime($tl->tgl_penagihan1)) }} <br><br>
                                                            <a href="{{ asset('surat_penagihan1/' . $tl->file_penagihan1) }}"
                                                                target="_blank"><svg xmlns="http://www.w3.org/2000/svg"
                                                                    width="24" fill="red" viewBox="0 0 384 512">
                                                                    <path
                                                                        d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM64 224H88c30.9 0 56 25.1 56 56s-25.1 56-56 56H80v32c0 8.8-7.2 16-16 16s-16-7.2-16-16V320 240c0-8.8 7.2-16 16-16zm24 80c13.3 0 24-10.7 24-24s-10.7-24-24-24H80v48h8zm72-64c0-8.8 7.2-16 16-16h24c26.5 0 48 21.5 48 48v64c0 26.5-21.5 48-48 48H176c-8.8 0-16-7.2-16-16V240zm32 112h8c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16h-8v96zm96-128h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v32h32c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V304 240c0-8.8 7.2-16 16-16z" />
                                                                </svg></a>
                                                        @else
                                                            @if (Auth::user()->role_id != 2)
                                                                <button type="button"
                                                                    class="btn btn-sm rounded-pill text-light"
                                                                    style="background: teal ;"
                                                                    title="Tambah Surat Penagihan 1"
                                                                    data-original-title="Tambah Surat Penagihan 1"
                                                                    data-toggle="modal"
                                                                    data-target="#tambahPenagihanSatu{{ $tl->id }}">
                                                                    +
                                                                </button>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($tl->tgl_penagihan2)
                                                            {{ date('d F Y', strtotime($tl->tgl_penagihan2)) }} <br><br>
                                                            <a href="{{ asset('surat_penagihan2/' . $tl->file_penagihan2) }}"
                                                                target="_blank"><svg xmlns="http://www.w3.org/2000/svg"
                                                                    width="24" fill="red" viewBox="0 0 384 512">
                                                                    <path
                                                                        d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM64 224H88c30.9 0 56 25.1 56 56s-25.1 56-56 56H80v32c0 8.8-7.2 16-16 16s-16-7.2-16-16V320 240c0-8.8 7.2-16 16-16zm24 80c13.3 0 24-10.7 24-24s-10.7-24-24-24H80v48h8zm72-64c0-8.8 7.2-16 16-16h24c26.5 0 48 21.5 48 48v64c0 26.5-21.5 48-48 48H176c-8.8 0-16-7.2-16-16V240zm32 112h8c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16h-8v96zm96-128h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v32h32c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V304 240c0-8.8 7.2-16 16-16z" />
                                                                </svg></a>
                                                        @else
                                                            @if (Auth::user()->role_id != 2)
                                                                <button type="button"
                                                                    class="btn btn-sm rounded-pill text-light"
                                                                    style="background: teal ;"
                                                                    title="Tambah Surat Penagihan 2"
                                                                    data-original-title="Tambah Surat Penagihan 2"
                                                                    data-toggle="modal"
                                                                    data-target="#tambahPenagihan2{{ $tl->id }}">
                                                                    <b>+</b>
                                                                </button>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($tl->tgl_terbit_pemberitahuan)
                                                            {{ date('d F Y', strtotime($tl->tgl_terbit_pemberitahuan)) }}
                                                            <br><br>
                                                            <a href="{{ asset('surat_pemberitahuan/' . $tl->file_pemberitahuan) }}"
                                                                target="_blank"><svg xmlns="http://www.w3.org/2000/svg"
                                                                    width="24" fill="red" viewBox="0 0 384 512">
                                                                    <path
                                                                        d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM64 224H88c30.9 0 56 25.1 56 56s-25.1 56-56 56H80v32c0 8.8-7.2 16-16 16s-16-7.2-16-16V320 240c0-8.8 7.2-16 16-16zm24 80c13.3 0 24-10.7 24-24s-10.7-24-24-24H80v48h8zm72-64c0-8.8 7.2-16 16-16h24c26.5 0 48 21.5 48 48v64c0 26.5-21.5 48-48 48H176c-8.8 0-16-7.2-16-16V240zm32 112h8c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16h-8v96zm96-128h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v32h32c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V304 240c0-8.8 7.2-16 16-16z" />
                                                                </svg></a>
                                                        @else
                                                            @if (Auth::user()->role_id != 2)
                                                                <button type="button"
                                                                    class="btn btn-sm rounded-pill text-light"
                                                                    style="background: teal ;"
                                                                    title="Tambah Surat Pemberitahuan"
                                                                    data-original-title="Tambah Surat Pemberitahuan"
                                                                    data-toggle="modal"
                                                                    data-target="#tambahPemberitahuan{{ $tl->id }}">
                                                                    <b>+</b>
                                                                </button>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($tl->tgl_tindakan_tambahan)
                                                            {{ $tl->no_tindakan_tambahan }} <br>
                                                            {{ date('d F Y', strtotime($tl->tgl_tindakan_tambahan)) }}
                                                            <br><br>
                                                            <a href="{{ asset('surat_tambahan/' . $tl->file_tindakan_tambahan) }}"
                                                                target="_blank"><svg xmlns="http://www.w3.org/2000/svg"
                                                                    width="24" fill="red" viewBox="0 0 384 512">
                                                                    <path
                                                                        d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM64 224H88c30.9 0 56 25.1 56 56s-25.1 56-56 56H80v32c0 8.8-7.2 16-16 16s-16-7.2-16-16V320 240c0-8.8 7.2-16 16-16zm24 80c13.3 0 24-10.7 24-24s-10.7-24-24-24H80v48h8zm72-64c0-8.8 7.2-16 16-16h24c26.5 0 48 21.5 48 48v64c0 26.5-21.5 48-48 48H176c-8.8 0-16-7.2-16-16V240zm32 112h8c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16h-8v96zm96-128h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v32h32c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V304 240c0-8.8 7.2-16 16-16z" />
                                                                </svg></a>
                                                        @else
                                                            @if (Auth::user()->role_id != 2)
                                                                <button type="button"
                                                                    class="btn btn-sm rounded-pill text-light"
                                                                    style="background: teal ;"
                                                                    title="Tambah Surat Tindakan Tambahan"
                                                                    data-original-title="Tambah Surat Tindakan Tambahan"
                                                                    data-toggle="modal"
                                                                    data-target="#tambahSuratTambahan{{ $tl->id }}">
                                                                    <b>+</b>
                                                                </button>
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Tambah Tindak Lanjut-->
    <div class="modal fade" id="tambahTL" tabindex="-1" role="dialog" aria-labelledby="tambahTLTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="tambahTLTitle" style="padding-left: 35%" ;>
                        Tindak Lanjut</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/create-tindaklanjut" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="no_tl">No Tindak Lanjut</label>
                            <input type="text" name="no_tl" value="{{ old('no_tl') }}"
                                class="form-control @error('no_tl') is-invalid @enderror" id="no_tl"
                                aria-describedby="no_tl" required>
                            @error('no_tl')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tgl_surat">Tanggal Surat Tindak Lanjut</label>
                            <input type="date" name="tgl_surat" value="{{ old('tgl_surat') }}"
                                class="form-control @error('tgl_surat') is-invalid @enderror" id="tgl_surat"
                                aria-describedby="tgl_surat" required>
                            @error('tgl_surat')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="file_surat_tl">Upload File Tindak Lanjut</label>
                            <div class="custom-file">
                                <input type="file" name="file_surat_tl"
                                    class="custom-file-input @error('file_surat_tl') is-invalid @enderror"
                                    id="file_surat_tl">
                                <label class="custom-file-label" for="file_surat_tl">Pilih File</label>
                            </div>
                            @error('file_surat_tl')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <input type="hidden" id="kategori_tl" name="kategori_tl" value="1">
                        <input type="hidden" id="id_trlhp" name="id_trlhp" value="{{ $trlhp->id }}">
                        <input type="hidden" id="id_lhp" name="id_lhp" value="{{ $trlhp->tlhp->lhp->id }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Tambah Surat Penagihan1-->
    <div class="modal fade" id="tambahPenagihan1" tabindex="-1" role="dialog" aria-labelledby="tambahPenagihan1Title"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="tambahPenagihan1Title" style="padding-left: 33%">
                        Surat Penagihan 1</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/create-penagihan1" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="tgl_penagihan1">Tanggal Penagihan 1</label>
                            <input type="date" name="tgl_penagihan1" value="{{ old('tgl_penagihan1') }}"
                                class="form-control @error('tgl_penagihan1') is-invalid @enderror" id="tgl_penagihan1"
                                aria-describedby="tgl_penagihan1" required>
                            @error('tgl_penagihan1')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="file_penagihan1">Upload File Surat Penagihan 1</label>
                            <div class="custom-file">
                                <input type="file" name="file_penagihan1"
                                    class="custom-file-input @error('file_penagihan1') is-invalid @enderror"
                                    id="file_penagihan1" required>
                                <label class="custom-file-label" for="file_penagihan1">Pilih File</label>
                            </div>
                            @error('file_penagihan1')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <input type="hidden" id="id_trlhp" name="id_trlhp" value="{{ $trlhp->id }}">
                        <input type="hidden" id="id_trlhp" name="id_trlhp" value="{{ $trlhp->tlhp->lhp->id }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @foreach ($reko as $trlhp)
        @foreach ($trlhp->tindaklanjut as $tl)
            <!-- Modal Tambah Surat Penagihan1-->
            <div class="modal fade" id="tambahPenagihanSatu{{ $tl->id }}" tabindex="-1" role="dialog"
                aria-labelledby="tambahPenagihanSatuTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="tambahPenagihanSatuTitle"
                                style="padding-left: 33%">
                                Surat Penagihan 1</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/create-penagihanSatu/' . $tl->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="tgl_penagihan1">Tanggal Penagihan 1</label>
                                    <input type="date" name="tgl_penagihan1" value="{{ old('tgl_penagihan1') }}"
                                        class="form-control @error('tgl_penagihan1') is-invalid @enderror"
                                        id="tgl_penagihan1" aria-describedby="tgl_penagihan1" required>
                                    @error('tgl_penagihan1')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="file_penagihan1">Upload File Surat Penagihan 1</label>
                                    <div class="custom-file">
                                        <input type="file" name="file_penagihan1"
                                            class="custom-file-input @error('file_penagihan1') is-invalid @enderror"
                                            id="file_penagihan1" required>
                                        <label class="custom-file-label" for="file_penagihan1">Pilih File</label>
                                    </div>
                                    @error('file_penagihan1')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="hidden" id="id_trlhp" name="id_trlhp" value="{{ $trlhp->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Tambah Surat Penagihan2-->
            <div class="modal fade" id="tambahPenagihan2{{ $tl->id }}" tabindex="-1" role="dialog"
                aria-labelledby="tambahPenagihan2Title" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="tambahPenagihan2Title" style="padding-left: 33%">
                                Surat Penagihan 2</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/create-penagihan2/' . $tl->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="tgl_penagihan2">Tanggal Penagihan 2</label>
                                    <input type="date" name="tgl_penagihan2" value="{{ old('tgl_penagihan2') }}"
                                        class="form-control @error('tgl_penagihan2') is-invalid @enderror"
                                        id="tgl_penagihan2" aria-describedby="tgl_penagihan2" required>
                                    @error('tgl_penagihan2')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="file_penagihan2">Upload File Surat Penagihan 2</label>
                                    <div class="custom-file">
                                        <input type="file" name="file_penagihan2"
                                            class="custom-file-input @error('file_penagihan2') is-invalid @enderror"
                                            id="file_penagihan2" required>
                                        <label class="custom-file-label" for="file_penagihan2">Pilih File</label>
                                    </div>
                                    @error('file_penagihan2')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="hidden" id="id_trlhp" name="id_trlhp" value="{{ $trlhp->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Tambah Surat Pemberitahuan-->
            <div class="modal fade" id="tambahPemberitahuan{{ $tl->id }}" tabindex="-1" role="dialog"
                aria-labelledby="tambahPemberitahuanTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="tambahPemberitahuanTitle"
                                style="padding-left: 33%">
                                Surat Pemberitahuan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/create-pemberitahuan/' . $tl->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="tgl_terbit_pemberitahuan">Tanggal Surat Pemberitahuan</label>
                                    <input type="date" name="tgl_terbit_pemberitahuan"
                                        value="{{ old('tgl_terbit_pemberitahuan') }}"
                                        class="form-control @error('tgl_terbit_pemberitahuan') is-invalid @enderror"
                                        id="tgl_terbit_pemberitahuan" aria-describedby="tgl_terbit_pemberitahuan"
                                        required>
                                    @error('tgl_terbit_pemberitahuan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="file_pemberitahuan">Upload File Surat Pemberitahuan</label>
                                    <div class="custom-file">
                                        <input type="file" name="file_pemberitahuan"
                                            class="custom-file-input @error('file_pemberitahuan') is-invalid @enderror"
                                            id="file_pemberitahuan" required>
                                        <label class="custom-file-label" for="file_pemberitahuan">Pilih File</label>
                                    </div>
                                    @error('file_pemberitahuan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="hidden" id="id_trlhp" name="id_trlhp" value="{{ $trlhp->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Tambah Surat Tindakan Tambahan-->
            <div class="modal fade" id="tambahSuratTambahan{{ $tl->id }}" tabindex="-1" role="dialog"
                aria-labelledby="tambahSuratTambahanTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="tambahSuratTambahanTitle"
                                style="padding-left: 30%">
                                Surat Tindakan Tambahan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/create-tindakantambahan/' . $tl->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="no_tindakan_tambahan">No Surat Tindakan Tambahan</label>
                                    <input type="text" name="no_tindakan_tambahan"
                                        value="{{ old('no_tindakan_tambahan') }}"
                                        class="form-control @error('no_tindakan_tambahan') is-invalid @enderror"
                                        id="no_tindakan_tambahan" aria-describedby="no_tindakan_tambahan" required>
                                    @error('no_tindakan_tambahan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="tgl_tindakan_tambahan">Tanggal Surat Tindakan Tambahan</label>
                                    <input type="date" name="tgl_tindakan_tambahan"
                                        value="{{ old('tgl_tindakan_tambahan') }}"
                                        class="form-control @error('tgl_tindakan_tambahan') is-invalid @enderror"
                                        id="tgl_tindakan_tambahan" aria-describedby="tgl_tindakan_tambahan" required>
                                    @error('tgl_tindakan_tambahan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="file_tindakan_tambahan">Upload File Surat Tindakan Tambahan</label>
                                    <div class="custom-file">
                                        <input type="file" name="file_tindakan_tambahan"
                                            class="custom-file-input @error('file_tindakan_tambahan') is-invalid @enderror"
                                            id="file_tindakan_tambahan" required>
                                        <label class="custom-file-label" for="file_tindakan_tambahan">Pilih File</label>
                                    </div>
                                    @error('file_tindakan_tambahan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="hidden" id="id_trlhp" name="id_trlhp" value="{{ $trlhp->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Edit Tindak Lanjut-->
            <div class="modal fade" id="editTL{{ $tl->id }}" tabindex="-1" role="dialog"
                aria-labelledby="editTLTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="editTLTitle" style="padding-left: 35%" ;>
                                Tindak Lanjut</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/update-tindaklanjut/' . $tl->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                @if (Auth::user()->role_id == 2)
                                    <div class="form-group">
                                        <label for="no_tl">No Tindak Lanjut</label>
                                        <input type="text" name="no_tl" value="{{ old('no_tl', $tl->no_tl) }}"
                                            class="form-control @error('no_tl') is-invalid @enderror" id="no_tl"
                                            aria-describedby="no_tl" required>
                                        @error('no_tl')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="tgl_surat">Tanggal Surat Tindak Lanjut</label>
                                        <input type="date" name="tgl_surat"
                                            value="{{ old('tgl_surat', $tl->tgl_surat) }}"
                                            class="form-control @error('tgl_surat') is-invalid @enderror" id="tgl_surat"
                                            aria-describedby="tgl_surat" required>
                                        @error('tgl_surat')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <input type="hidden" name="oldFileSuratTL" value="{{ $tl->file_surat_tl }}">
                                    <div class="form-group">
                                        <label for="file_surat_tl">Upload File Tindak Lanjut</label>
                                        <div class="custom-file">
                                            <input type="file" name="file_surat_tl"
                                                class="custom-file-input @error('file_surat_tl') is-invalid @enderror"
                                                id="file_surat_tl">
                                            <label class="custom-file-label"
                                                for="file_surat_tl">{{ $tl->file_surat_tl ?? 'Pilih File' }}</label>
                                        </div>
                                        @error('file_surat_tl')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label>Kategori Tindak Lanjut</label>
                                        <select class="form-control" name="kategori_tl" id="kategori_tl">
                                            <option value="">Pilih Kategori Tindak Lanjut</option>
                                            @foreach ($kategoritl as $kategori)
                                                <option value="{{ $kategori->id }}"
                                                    {{ $tl->kategori_tl == $kategori->id ? 'selected' : '' }}>
                                                    {{ $kategori->nama_kategori }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">Keterangan</label>
                                        <textarea name="keterangan" value="{{ old('keterangan') }}"
                                            class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" rows="2" required>{{ $tl->keterangan }}</textarea>
                                        @error('keterangan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    @if ($tl->tgl_penagihan1)
                                        <hr>
                                        <label><b>Surat Penagihan 1</b></label>
                                        <div class="form-group">
                                            <label for="tgl_penagihan1">Tanggal Surat Penagihan 1</label>
                                            <input type="date" name="tgl_penagihan1"
                                                value="{{ old('tgl_penagihan1', $tl->tgl_penagihan1) }}"
                                                class="form-control @error('tgl_penagihan1') is-invalid @enderror"
                                                id="tgl_penagihan1" aria-describedby="tgl_penagihan1">
                                            @error('tgl_penagihan1')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <input type="hidden" name="oldFilePenagihan1"
                                            value="{{ $tl->file_penagihan1 }}">
                                        <div class="form-group">
                                            <label for="file_penagihan1">Upload File Surat Penagihan 1</label>
                                            <div class="custom-file">
                                                <input type="file" name="file_penagihan1"
                                                    class="custom-file-input @error('file_penagihan1') is-invalid @enderror"
                                                    id="file_penagihan1">
                                                <label class="custom-file-label"
                                                    for="file_penagihan1">{{ $tl->file_penagihan1 ?? 'Pilih File' }}</label>
                                            </div>
                                            @error('file_penagihan1')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                                    @if ($tl->tgl_penagihan2)
                                        <hr>
                                        <label><b>Surat Penagihan 2</b></label>
                                        <div class="form-group">
                                            <label for="tgl_penagihan2">Tanggal Surat Penagihan 2</label>
                                            <input type="date" name="tgl_penagihan2"
                                                value="{{ old('tgl_penagihan2', $tl->tgl_penagihan2) }}"
                                                class="form-control @error('tgl_penagihan2') is-invalid @enderror"
                                                id="tgl_penagihan2" aria-describedby="tgl_penagihan2">
                                            @error('tgl_penagihan2')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <input type="hidden" name="oldFilePenagihan2"
                                            value="{{ $tl->file_penagihan2 }}">
                                        <div class="form-group">
                                            <label for="file_penagihan2">Upload File Surat Penagihan 2</label>
                                            <div class="custom-file">
                                                <input type="file" name="file_penagihan2"
                                                    class="custom-file-input @error('file_penagihan2') is-invalid @enderror"
                                                    id="file_penagihan2">
                                                <label class="custom-file-label"
                                                    for="file_penagihan2">{{ $tl->file_penagihan2 ?? 'Pilih File' }}</label>
                                            </div>
                                            @error('file_penagihan2')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                                    @if ($tl->tgl_terbit_pemberitahuan)
                                        <hr>
                                        <label><b>Surat Pemberitahuan</b></label>
                                        <div class="form-group">
                                            <label for="tgl_terbit_pemberitahuan">Tanggal Surat Pemberitahuan</label>
                                            <input type="date" name="tgl_terbit_pemberitahuan"
                                                value="{{ old('tgl_terbit_pemberitahuan', $tl->tgl_terbit_pemberitahuan) }}"
                                                class="form-control @error('tgl_terbit_pemberitahuan') is-invalid @enderror"
                                                id="tgl_terbit_pemberitahuan" aria-describedby="tgl_terbit_pemberitahuan">
                                            @error('tgl_terbit_pemberitahuan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <input type="hidden" name="oldFilePemberitahuan"
                                            value="{{ $tl->file_pemberitahuan }}">
                                        <div class="form-group">
                                            <label for="file_pemberitahuan">Upload File Surat Pemberitahuan</label>
                                            <div class="custom-file">
                                                <input type="file" name="file_pemberitahuan"
                                                    class="custom-file-input @error('file_pemberitahuan') is-invalid @enderror"
                                                    id="file_pemberitahuan">
                                                <label class="custom-file-label"
                                                    for="file_pemberitahuan">{{ $tl->file_pemberitahuan ?? 'Pilih File' }}</label>
                                            </div>
                                            @error('file_pemberitahuan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                                    @if ($tl->no_tindakan_tambahan)
                                        <hr>
                                        <label><b>Surat Tindakan Tambahan</b></label>
                                        <div class="form-group">
                                            <label for="no_tindakan_tambahan">No Tindakan Tambahan</label>
                                            <input type="text" name="no_tindakan_tambahan"
                                                value="{{ old('no_tindakan_tambahan', $tl->no_tindakan_tambahan) }}"
                                                class="form-control @error('no_tindakan_tambahan') is-invalid @enderror"
                                                id="no_tindakan_tambahan" aria-describedby="no_tindakan_tambahan">
                                            @error('no_tindakan_tambahan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="tgl_tindakan_tambahan">Tanggal Tindakan Tambahan</label>
                                            <input type="date" name="tgl_tindakan_tambahan"
                                                value="{{ old('tgl_tindakan_tambahan', $tl->tgl_tindakan_tambahan) }}"
                                                class="form-control @error('tgl_tindakan_tambahan') is-invalid @enderror"
                                                id="tgl_tindakan_tambahan" aria-describedby="tgl_tindakan_tambahan">
                                            @error('tgl_tindakan_tambahan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <input type="hidden" name="oldFileTambahan"
                                            value="{{ $tl->file_tindakan_tambahan }}">
                                        <div class="form-group">
                                            <label for="file_tindakan_tambahan">Upload File Surat Tindakan Tambahan</label>
                                            <div class="custom-file">
                                                <input type="file" name="file_tindakan_tambahan"
                                                    class="custom-file-input @error('file_tindakan_tambahan') is-invalid @enderror"
                                                    id="file_tindakan_tambahan">
                                                <label class="custom-file-label"
                                                    for="file_tindakan_tambahan">{{ $tl->file_tindakan_tambahan ?? 'Pilih File' }}</label>
                                            </div>
                                            @error('file_tindakan_tambahan')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                                @endif
                                <input type="hidden" id="id_trlhp" name="id_trlhp" value="{{ $trlhp->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    @endforeach

    <script>
        var fileInput = document.getElementsByClassName("custom-file-input");

        for (var i = 0; i < fileInput.length; i++) {
            var input = fileInput[i];
            input.addEventListener('change', function(e) {
                var files = [];
                for (var i = 0; i < $(this)[0].files.length; i++) {
                    files.push($(this)[0].files[i].name);
                }
                $(this).next('.custom-file-label').html(files.join(', '));
            })
        }
    </script>
@endsection
