@extends('layouts.home')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @foreach ($list as $lhp)
                        <div class="iq-card">
                            <div class="iq-header-title">
                                <div style="padding-top: 15px; padding-left: 15px">
                                    <a href="{{ url('/lhp') }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#0084ff"
                                            class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                                        </svg>
                                    </a>
                                </div>
                                <h5 align="center" class="card-title-lhp">
                                    {{ $lhp->no_lhp }}<br>{{ date('d F Y', strtotime($lhp->tanggal_lhp)) }}</br>
                                </h5>
                            </div>
                            <div class="detail">
                                <table width="100%">
                                    <tr>
                                        <th Width="23%">Entitas Pengawasan</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">
                                            {{ $lhp->jadwalopd->opd->nama }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Kirim LHP</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $lhp->tanggal_kirim }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tindak Lanjut/Tanggapan</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $lhp->tanggapan }}</td>
                                    </tr>
                                    <tr>
                                        <th>Batas Waktu Penyelesaian</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $lhp->batas_waktu }}</td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Tindak Lanjut</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $lhp->jenis_tl }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="iq-card-header d-flex justify-content-between">
                                <div class="iq-header-title">
                                    <h4 class="card-title">Temuan</h4>
                                </div>
                                <div class="card-toolbarn">
                                    @if (Auth::user()->role_id != 2)
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#tambahTemuan">
                                            + Tambah
                                        </button>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-items-center mt-3">
                                <div class="col-sm-12 col-md-6">
                                    <div id="user_list_datatable_info" class="dataTables_filter">
                                        <input class="form-control me-2" type="search" placeholder="Cari.." name="search"
                                            aria-label="Search" id="searchInput">
                                    </div>
                                </div>
                            </div>
                            <div class="iq-card-body">
                                <div class="table-responsive">
                                    <table id="data-table" class="table table-striped table-bordered mt-1" role="grid"
                                        aria-describedby="user-list-page-info" width=100%>
                                        <thead>
                                            <tr>
                                                @if (Auth::user()->role_id != 2)
                                                    <th width=10%>Action</th>
                                                @endif
                                                <th width=45%>Temuan</th>
                                                <th width=45%>Rekomendasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($lhp->temuan as $temuan)
                                                <tr>
                                                    @if (Auth::user()->role_id != 2)
                                                        <td>
                                                            <div class="d-block justify-items-center ">
                                                                <div class="d-flex justify-items-center mb-2">
                                                                    <a class="btn btn-light btn-sm mx-1" data-toggle="modal"
                                                                        data-placement="top" title="Edit"
                                                                        data-original-title="Edit"
                                                                        data-target="#editTemuan{{ $temuan->pivot->id }}"><span
                                                                            class="svg-icon svg-icon-3">
                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                width="16" height="16"
                                                                                viewBox="0 0 24 24" fill="#04c8c8">
                                                                                <path opacity="0.3"
                                                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                                                                <path
                                                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                                                            </svg>
                                                                        </span>
                                                                    </a>
                                                                    <form class="btn-delete"
                                                                        action="{{ url('/delete-tlhp/' . $temuan->pivot->id) }}"
                                                                        method="post">
                                                                        @method('delete')
                                                                        @csrf
                                                                        <button type="submit"
                                                                            class="btn btn-light btn-sm mx-1"
                                                                            data-placement="top" title="Delete"
                                                                            data-original-title="Delete">
                                                                            <span class="svg-icon svg-icon-3"><svg
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    width="16" height="16"
                                                                                    viewBox="0 0 24 24" fill="#f1416c">
                                                                                    <path
                                                                                        d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" />
                                                                                    <path opacity="0.5"
                                                                                        d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" />
                                                                                    <path opacity="0.5"
                                                                                        d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" />
                                                                                </svg></span>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                                <button type="button"
                                                                    class="btn btn-sm btn-primary rounded-pill text-light"
                                                                    title="Tambah Rekomendasi"
                                                                    data-original-title="Tambah Rekomendasi"
                                                                    data-toggle="modal"
                                                                    data-target="#tambahRekomendasi{{ $temuan->pivot->id }}">
                                                                    Rekomendasi
                                                                </button>
                                                            </div>
                                                        </td>
                                                    @endif
                                                    <td style="text-align: left">
                                                        <b>{{ $temuan->nama_temuan }} ({{ $temuan->kode_temuan }})</b>
                                                        <br>
                                                        {{ $temuan->pivot->uraian_temuan }}
                                                    </td>
                                                    <td>
                                                        <table width="100%" border="1"
                                                            class="table table-striped table-bordered">
                                                            @foreach ($temuan->pivot->rekomendasi as $trlhp)
                                                                <tr>
                                                                    <td style="text-align: left">
                                                                        <b>{{ $trlhp->nama_rekomendasi }}
                                                                            ({{ $trlhp->kode_rekomendasi }})
                                                                        </b> <br>
                                                                        {{ $trlhp->pivot->uraian_rekomendasi }} <br>
                                                                    </td>
                                                                    <td>
                                                                        <div class="d-block justify-items-center ">
                                                                            @if (Auth::user()->role_id != 2)
                                                                                <div
                                                                                    class="d-flex justify-items-center mb-2">
                                                                                    <a class="btn btn-light btn-sm mx-1"
                                                                                        data-toggle="modal"
                                                                                        data-placement="top"
                                                                                        title="Edit"
                                                                                        data-original-title="Edit"
                                                                                        data-target="#editRekomendasi{{ $trlhp->pivot->id }}"><span
                                                                                            class="svg-icon svg-icon-3">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                width="16"
                                                                                                height="16"
                                                                                                viewBox="0 0 24 24"
                                                                                                fill="#04c8c8">
                                                                                                <path opacity="0.3"
                                                                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                                                                                <path
                                                                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                                                                            </svg>
                                                                                        </span>
                                                                                    </a>
                                                                                    <form class="btn-delete"
                                                                                        action="{{ url('/delete-trlhp/' . $trlhp->pivot->id) }}"
                                                                                        method="post">
                                                                                        @method('delete')
                                                                                        @csrf
                                                                                        <button type="submit"
                                                                                            class="btn btn-light btn-sm mx-1"
                                                                                            data-placement="top"
                                                                                            title="Delete"
                                                                                            data-original-title="Delete">
                                                                                            <span
                                                                                                class="svg-icon svg-icon-3"><svg
                                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                                    width="16"
                                                                                                    height="16"
                                                                                                    viewBox="0 0 24 24"
                                                                                                    fill="#f1416c">
                                                                                                    <path
                                                                                                        d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" />
                                                                                                    <path opacity="0.5"
                                                                                                        d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" />
                                                                                                    <path opacity="0.5"
                                                                                                        d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" />
                                                                                                </svg>
                                                                                            </span>
                                                                                        </button>
                                                                                    </form>
                                                                                </div>
                                                                            @endif
                                                                            <a href="{{ url('/lhp-tl/' . $trlhp->pivot->id) }}"
                                                                                type="button" style="background: teal ;"
                                                                                class="btn btn-sm rounded-pill text-light"
                                                                                title="Lihat Tindak Lanjut"
                                                                                data-original-title="Lihat Tindak Lanjut">
                                                                                TindakLanjut
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    @foreach ($list as $lhp)
        @foreach ($lhp->temuan as $tlhp)
            <!-- Modal Edit Temuan LHP-->
            <div class="modal fade" id="editTemuan{{ $tlhp->pivot->id }}" tabindex="-1" role="dialog"
                aria-labelledby="editTemuanTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="editTemuanTitle" style="padding-left: 35%" ;>
                                Edit Temuan LHP</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/update-tlhp/' . $tlhp->pivot->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="form-group">
                                    <label>Kode Temuan</label>
                                    <select class="selectpicker" name="id_temuan" data-live-search="true"
                                        title="Pilih Kode Temuan" data-width="100%">
                                        @foreach ($temuans as $temuan)
                                            <option value="{{ $temuan->id }}" title="{{ $temuan->kode_temuan }}"
                                                {{ $tlhp->pivot->id_temuan == $temuan->id ? 'selected' : '' }}>
                                                {{ $temuan->kode_temuan }}-{{ $temuan->nama_temuan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="uraian_temuan">Uraian Temuan</label>
                                    <textarea name="uraian_temuan" value="{{ old('uraian_temuan') }}"
                                        class="form-control @error('uraian_temuan') is-invalid @enderror" id="uraian_temuan" rows="2">{{ old('uraian_temuan', $tlhp->pivot->uraian_temuan) }}</textarea>
                                    @error('uraian_temuan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="hidden" name="id_lhp" value="{{ $lhp->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Tambah Rekomendasi LHP-->
            <div class="modal fade" id="tambahRekomendasi{{ $tlhp->pivot->id }}" tabindex="-1" role="dialog"
                aria-labelledby="tambahRekomendasiTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="tambahRekomendasiTitle" style="padding-left: 35%"
                                ;>
                                Rekomendasi LHP</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="/create-trlhp" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>Kode Rekomendasi</label>
                                    <select class="selectpicker" name="id_rekomendasi" data-live-search="true"
                                        title="Pilih Kode Rekomendasi" data-width="100%">
                                        @foreach ($rekomendasis as $rekomendasi)
                                            <option value="{{ $rekomendasi->id }}"
                                                title="{{ $rekomendasi->kode_rekomendasi }}">
                                                {{ $rekomendasi->kode_rekomendasi }}-{{ $rekomendasi->nama_rekomendasi }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="uraian_rekomendasi">Uraian Rekomendasi</label>
                                    <textarea name="uraian_rekomendasi" value="{{ old('uraian_rekomendasi') }}"
                                        class="form-control @error('uraian_rekomendasi') is-invalid @enderror" id="uraian_rekomendasi" rows="2"></textarea>
                                    @error('uraian_rekomendasi')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <input type="hidden" name="id_tlhp" value="{{ $tlhp->pivot->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Modal Edit Rekomendasi LHP-->
            @foreach ($tlhp->pivot->rekomendasi as $trlhp)
                <div class="modal fade" id="editRekomendasi{{ $trlhp->pivot->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="editRekomendasiTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 align="center" class="modal-title" id="editRekomendasiTitle"
                                    style="padding-left: 30%" ;>
                                    Edit Rekomendasi LHP</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ url('/update-trlhp/' . $trlhp->pivot->id) }}" method="post"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="form-group">
                                        <label>Kode Rekomendasi</label>
                                        <select class="selectpicker" name="id_rekomendasi" data-live-search="true"
                                            title="Pilih Kode Rekomendasi" data-width="100%">
                                            @foreach ($rekomendasis as $rekomendasi)
                                                <option value="{{ $rekomendasi->id }}"
                                                    title="{{ $rekomendasi->kode_rekomendasi }}"
                                                    {{ $trlhp->pivot->id_rekomendasi == $rekomendasi->id ? 'selected' : '' }}>
                                                    {{ $rekomendasi->kode_rekomendasi }}-{{ $rekomendasi->nama_rekomendasi }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="uraian_rekomendasi">Uraian Rekomendasi</label>
                                        <textarea name="uraian_rekomendasi" value="{{ old('uraian_rekomendasi') }}"
                                            class="form-control @error('uraian_rekomendasi') is-invalid @enderror" id="uraian_rekomendasi" rows="2">{{ old('uraian_rekomendasi', $trlhp->pivot->uraian_rekomendasi) }}</textarea>
                                        @error('uraian_rekomendasi')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <input type="hidden" name="id_tlhp" value="{{ $tlhp->pivot->id }}">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn btn-primary">Ubah</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        @endforeach
    @endforeach

    <!-- Modal Tambah Temuan LHP-->
    <div class="modal fade" id="tambahTemuan" tabindex="-1" role="dialog" aria-labelledby="tambahTemuanTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="tambahTemuanTitle" style="padding-left: 37%" ;>
                        Temuan LHP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/create-tlhp" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Kode Temuan</label>
                            <select class="selectpicker" name="id_temuan" data-live-search="true"
                                title="Pilih Kode Temuan" data-width="100%">
                                @foreach ($temuans as $temuan)
                                    <option value="{{ $temuan->id }}" title="{{ $temuan->kode_temuan }}">
                                        {{ $temuan->kode_temuan }}-{{ $temuan->nama_temuan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="uraian_temuan">Uraian Temuan</label>
                            <textarea name="uraian_temuan" value="{{ old('uraian_temuan') }}"
                                class="form-control @error('uraian_temuan') is-invalid @enderror" id="uraian_temuan" rows="2"></textarea>
                            @error('uraian_temuan')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <input type="hidden" name="id_lhp" value="{{ $lhp->id }}">
                        {{-- <div class="form-group">
                            <label for=""></label>
                            <a href="#" class="addTemuan btn btn-primary" style="float: right;">Tambah Temuan</a>
                        </div>
                        <div class="form-temuan"></div> --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal Edit Tindak Lanjut-->
    {{-- @foreach ($list as $lhp)
        @if ($lhp->tindaklanjut)
            <div class="modal fade" id="editTL{{ $lhp->tindaklanjut->id }}" tabindex="-1" role="dialog"
                aria-labelledby="tambahTLTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="tambahTLTitle" style="padding-left: 30%" ;>
                                Tindak Lanjut</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/update-tindaklanjut/' . $lhp->tindaklanjut->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                @if (Auth::user()->role_id == 2)
                                    <div class="form-group">
                                        <label for="no_tl">No Tindak Lanjut</label>
                                        <input type="text" name="no_tl"
                                            value="{{ old('no_tl', $lhp->tindaklanjut->no_tl) }}"
                                            class="form-control @error('no_tl') is-invalid @enderror" id="no_tl"
                                            aria-describedby="no_tl" required>
                                        @error('no_tl')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="tgl_surat">Tanggal Surat Tindak Lanjut</label>
                                        <input type="date" name="tgl_surat"
                                            value="{{ old('tgl_surat', $lhp->tindaklanjut->tgl_surat) }}"
                                            class="form-control @error('tgl_surat') is-invalid @enderror" id="tgl_surat"
                                            aria-describedby="tgl_surat" required>
                                        @error('tgl_surat')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="tgl_verifikasi">Tanggal Verifikasi Tindak Lanjut</label>
                                        <input type="date" name="tgl_verifikasi"
                                            value="{{ old('tgl_verifikasi', $lhp->tindaklanjut->tgl_verifikasi) }}"
                                            class="form-control @error('tgl_verifikasi') is-invalid @enderror"
                                            id="tgl_verifikasi" aria-describedby="tgl_verifikasi" required>
                                        @error('tgl_verifikasi')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <input type="hidden" name="oldFile"
                                        value="{{ $lhp->tindaklanjut->file_surat_tl }}">
                                    <div class="form-group">
                                        <label for="file_surat_tl">Upload File Tindak Lanjut</label>
                                        <div class="custom-file">
                                            <input type="file" name="file_surat_tl"
                                                class="custom-file-input @error('file_surat_tl') is-invalid @enderror"
                                                id="file_surat_tl">
                                            <label class="custom-file-label"
                                                for="file_surat_tl">{{ $lhp->tindaklanjut->file_surat_tl ?? 'Pilih File' }}</label>
                                        </div>
                                        @error('file_surat_tl')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label>Kategori Tindak Lanjut</label>
                                        <select class="form-control" name="kategori_tl" id="kategori_tl">
                                            <option value=""> Pilih Kategori Tindak Lanjut</option>
                                            @foreach ($kategoritl as $kategori)
                                                <option value="{{ $kategori->id }}"
                                                    {{ $lhp->tindaklanjut->kategori_tl == $kategori->id ? 'selected' : '' }}>
                                                    {{ $kategori->nama_kategori }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">Keterangan</label>
                                        <textarea name="keterangan" value="{{ old('keterangan') }}"
                                            class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" rows="2" required>{{ $lhp->tindaklanjut->keterangan }}</textarea>
                                        @error('keterangan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="tgl_penagihan1">Tanggal Terbit Surat Penagihan 1</label>
                                        <input type="date" name="tgl_penagihan1"
                                            value="{{ old('tgl_penagihan1', $lhp->tindaklanjut->tgl_penagihan1) }}"
                                            class="form-control @error('tgl_penagihan1') is-invalid @enderror"
                                            id="tgl_penagihan1" aria-describedby="tgl_penagihan1">
                                        @error('tgl_penagihan1')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="tgl_penagihan2">Tanggal Terbit Surat Penagihan 2</label>
                                        <input type="date" name="tgl_penagihan2"
                                            value="{{ old('tgl_penagihan2', $lhp->tindaklanjut->tgl_penagihan2) }}"
                                            class="form-control @error('tgl_penagihan2') is-invalid @enderror"
                                            id="tgl_penagihan2" aria-describedby="tgl_penagihan2">
                                        @error('tgl_penagihan2')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="tgl_terbit_pemberitahuan">Tanggal Terbit Surat Pemberitahuan</label>
                                        <input type="date" name="tgl_terbit_pemberitahuan"
                                            value="{{ old('tgl_terbit_pemberitahuan', $lhp->tindaklanjut->tgl_terbit_pemberitahuan) }}"
                                            class="form-control @error('tgl_terbit_pemberitahuan') is-invalid @enderror"
                                            id="tgl_terbit_pemberitahuan" aria-describedby="tgl_terbit_pemberitahuan">
                                        @error('tgl_terbit_pemberitahuan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="no_tindakan_tambahan">No Tindakan Tambahan</label>
                                        <input type="text" name="no_tindakan_tambahan"
                                            value="{{ old('no_tindakan_tambahan', $lhp->tindaklanjut->no_tindakan_tambahan) }}"
                                            class="form-control @error('no_tindakan_tambahan') is-invalid @enderror"
                                            id="no_tindakan_tambahan" aria-describedby="no_tindakan_tambahan">
                                        @error('no_tindakan_tambahan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="tgl_tindakan_tambahan">Tanggal Tindakan Tambahan</label>
                                        <input type="date" name="tgl_tindakan_tambahan"
                                            value="{{ old('tgl_tindakan_tambahan', $lhp->tindaklanjut->tgl_tindakan_tambahan) }}"
                                            class="form-control @error('tgl_tindakan_tambahan') is-invalid @enderror"
                                            id="tgl_tindakan_tambahan" aria-describedby="tgl_tindakan_tambahan">
                                        @error('tgl_tindakan_tambahan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                @endif
                                <input type="hidden" id="id_lhp" name="id_lhp" value="{{ $lhp->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    @endforeach --}}

    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script>
        $('.addTemuan').on('click', function() {
            addTemuan();
        });

        function addTemuan() {
            var temuan = '<div><div class="form-group"><label>Kode Temuan</label><select class="form-select" name="id_temuan" data-live-search="true"title="Pilih Kode Temuan" data-width="100%">@foreach ($temuans as $temuan)<option value="{{ $temuan->id }}" title="{{ $temuan->kode_temuan }}">{{ $temuan->kode_temuan }}-{{ $temuan->nama_temuan }}</option>@endforeach</select></div><div class="form-group"><label for="uraian_temuan">Uraian Temuan</label><textarea name="uraian_temuan" value="{{ old('uraian_temuan') }}"class="form-control @error('uraian_temuan') is-invalid @enderror" id="uraian_temuan" rows="2"></textarea>@error('uraian_temuan')<div class="invalid-feedback">{{ $message }}</div>@enderror</div><div><a href="#" class="remove btn btn-danger" style="float: right;">Hapus</a></div></div>';
            $('.form-temuan').append(temuan);
        };

        $('.remove').live('click', function() {
            $(this).parent().parent().remove();
        })
        $('.selectpicker2').selectpicker('refresh');
    </script> --}}

    {{-- <script>
        var fileInput = document.getElementsByClassName("custom-file-input");

        for (var i = 0; i < fileInput.length; i++) {
            var input = fileInput[i];
            input.addEventListener('change', function(e) {
                var files = [];
                for (var i = 0; i < $(this)[0].files.length; i++) {
                    files.push($(this)[0].files[i].name);
                }
                $(this).next('.custom-file-label').html(files.join(', '));
            })
        }
    </script> --}}
@endsection
