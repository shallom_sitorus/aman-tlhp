@extends('layouts.home')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Laporan Hasil Pengawasan</h4>
                            </div>
                            <div class="card-toolbarn">
                                <a type="button" href="" class="btn btn-sm btn-primary my-1" target="_blank" data-target="#printlhp" data-toggle="modal">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            fill="currentColor" class="bi bi-printer" viewBox="0 0 16 16">
                                            <path d="M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z" />
                                            <path
                                                d="M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2H5zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4V3zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2H5zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1z" />
                                        </svg>
                                    </span>Print
                                </a>
                            </div>
                            <div class="card-toolbarn">
                                <a type="button" href="/print" class="btn btn-sm btn-primary my-1" target="_blank" data-target="" data-toggle="modal">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            fill="currentColor" class="bi bi-printer" viewBox="0 0 16 16">
                                            <path d="M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z" />
                                            <path
                                                d="M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2H5zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4V3zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2H5zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1z" />
                                        </svg>
                                    </span>Print terbaru
                                </a>
                            </div>
                        </div>
                        <div class="d-flex justify-items-center mt-3">
                            <div class="col-sm-12 col-md-6">
                                <div id="user_list_datatable_info" class="dataTables_filter">
                                    <input class="form-control me-2" type="search" placeholder="Cari.." name="search"
                                        aria-label="Search" id="searchInput">
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <form action="{{ url('/lhp') }}" method="GET">
                                    @csrf
                                    <div class="form-group d-flex">
                                        <label class="px-2 mt-2">Tahun</label>
                                        <select class="form-control form-control-sm" name="tanggal_lhp">
                                            @foreach ($tahun as $thn)
                                                <option value="{{ $thn }}" @if (request()->tanggal_lhp)
                                                    {{ request()->tanggal_lhp == $thn ? 'selected' : '' }}
                                                @else
                                                    {{ date('Y') == $thn ? 'selected' : '' }}
                                                @endif>{{ $thn }}</option>
                                            @endforeach
                                        </select>
                                        <button type="submit" class="btn btn-sm btn-secondary"
                                            style="margin-left: 5px;">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered" role="grid"
                                    aria-describedby="user-list-page-info">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">Action</th>
                                            <th rowspan="2">No</th>
                                            <th colspan="1">Entitas Pengawasan</th>
                                            <th rowspan="2">Tanggal&nbsp;Kirim&nbsp;LHP</th>
                                            <th colspan="1">Tindak&nbsp;Lanjut/Tanggapan</th>
                                            <th rowspan="2">Batas Waktu Penyelesaian Tindak Lanjut</th>
                                            <th rowspan="2">Jenis Tindak&nbsp;Lanjut</th>
                                            <th rowspan="2">Status</th>
                                        </tr>
                                        <tr>
                                            <th class="px-5">Tanggal&nbsp;LHP, No&nbsp;LHP</th>
                                            <th class="px-5">(Uraian&nbsp;Ringkas)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($listlhp as $lhp)
                                            <tr>
                                                <td>
                                                    <div class="d-flex justify-items-center ">
                                                        <a class="btn btn-light btn-sm mx-1" title="Detail"
                                                            data-placement="top" title="" data-original-title="Detail"
                                                            href="{{ url('/detaillhp/' . $lhp->id . '') }}"><span
                                                                class="svg-icon svg-icon-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                                    height="16" fill="#7239ea">
                                                                    <path
                                                                        d=" M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                                                    <path
                                                                        d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
                                                                </svg>
                                                            </span>
                                                        </a>
                                                        @if (Auth::user()->role_id != 2)
                                                            <a class="btn btn-light btn-sm mx-1" data-toggle="modal"
                                                                data-placement="top" title="Edit"
                                                                data-original-title="Edit"
                                                                data-target="#editLhp{{ $lhp->id }}"><span
                                                                    class="svg-icon svg-icon-3">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                                        height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                                                        <path opacity="0.3"
                                                                            d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                                                        <path
                                                                            d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                                                    </svg>
                                                                </span>
                                                            </a>
                                                            <form class="btn-delete"
                                                                action="{{ url('/delete-lhp/' . $lhp->id) }}"
                                                                method="post">
                                                                @method('delete')
                                                                @csrf
                                                                <button type="submit" class="btn btn-light btn-sm mx-1"
                                                                    title="Delete" data-placement="top"
                                                                    data-original-title="Delete">
                                                                    <span class="svg-icon svg-icon-3"><svg
                                                                            xmlns="http://www.w3.org/2000/svg"
                                                                            width="16" height="16"
                                                                            viewBox="0 0 24 24" fill="#f1416c">
                                                                            <path
                                                                                d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" />
                                                                            <path opacity="0.5"
                                                                                d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" />
                                                                            <path opacity="0.5"
                                                                                d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" />
                                                                        </svg></span>
                                                                </button>
                                                            </form>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="text-align: center">
                                                    {{ $lhp->jadwalopd->opd->nama }}
                                                    <hr>
                                                    <div style="text-align: center">
                                                        {{ date('d F Y', strtotime($lhp->tanggal_lhp)) }}<br>
                                                        {{ $lhp->no_lhp }}
                                                    </div>
                                                </td>
                                                <td>{{ date('d-m-Y', strtotime($lhp->tanggal_kirim)) }}</td>
                                                <td style="text-align: justify">{{ Str::words($lhp->tanggapan, 10, '...') }}</td>
                                                <td>{{ date('d-m-Y', strtotime($lhp->batas_waktu)) }}</td>
                                                <td>{{ $lhp->jenis_tl }}</td>
                                                <td>
                                                    @if ($lhp->tindaklanjut)
                                                        <span class="badge badge-pill badge-success">Sudah
                                                            ditindaklanjuti</span>
                                                    @else
                                                        <span class="badge badge-pill badge-secondary">Belum
                                                            ditindaklanjuti</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal print lhp --}}
    <div class="modal fade" id="printlhp" tabindex="-1" role="dialog" aria-labelledby="printlhp"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="printlhp" style="padding-left: 30%">
                        Print Laporan Hasil Pengawasan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/printlhp" method="GET" target="_blank">
                        @csrf
                        <div class="form-group">
                            <label class="required">Tahun</label>
                            <select class="form-control" name="tanggal_lhp" id="tanggal_lhp">
                                @foreach ($tahun as $thn)
                                        <option value="{{ $thn }}"
                                            {{ request()->tanggal_lhp == $thn ? 'selected' : '' }}>{{ $thn }}
                                        </option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Print</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit-->
    @foreach ($listlhp as $lhp)
    <div class="modal fade" id="editLhp{{ $lhp->id }}" tabindex="-1" role="dialog"
        aria-labelledby="editLhpTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="editLhpTitle" style="padding-left: 30%">Laporan Hasil
                        Pengawasan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/update-lhp/' . $lhp->id) }}" method="post"
                        enctype="multipart/form-data">
                        @method('put')
                        @csrf
                        <div class="form-group">
                            <label class="required" for="tanggal_lhp">Tanggal LHP</label>
                            <input type="date" name="tanggal_lhp"
                                value="{{ old('tanggal_lhp', $lhp->tanggal_lhp) }}"
                                class="form-control @error('tanggal_lhp') is-invalid @enderror" id="tanggal_lhp"
                                aria-describedby="tanggal_lhp" required>
                            @error('tanggal_lhp')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required" for="no_lhp">No LHP</label>
                            <input type="text" name="no_lhp" value="{{ old('no_lhp', $lhp->no_lhp) }}"
                                class="form-control @error('no_lhp') is-invalid @enderror" id="no_lhp"
                                aria-describedby="no_lhp" required>
                            @error('no_lhp')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required" for="tanggal_kirim">Tanggal Kirim LHP</label>
                            <input type="date" name="tanggal_kirim"
                                value="{{ old('tanggal_kirim', $lhp->tanggal_kirim) }}"
                                class="form-control @error('tanggal_kirim') is-invalid @enderror" id="tanggal_kirim"
                                aria-describedby="tanggal_kirim" required>
                            @error('tanggal_kirim')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required" for="tanggapan">Tindak Lanjut/Tanggapan</label>
                            <textarea name="tanggapan" class="form-control @error('tanggapan') is-invalid @enderror" id="tanggapan"
                                rows="2" required>{{ old('tanggapan', $lhp->tanggapan) }}</textarea>
                            @error('tanggapan')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required" for="batas_waktu">Batas Waktu Penyelesaian Tindak Lanjut</label>
                            <input type="date" name="batas_waktu"
                                value="{{ old('batas_waktu', $lhp->batas_waktu) }}"
                                class="form-control @error('batas_waktu') is-invalid @enderror" id="batas_waktu"
                                aria-describedby="batas_waktu" required>
                            @error('batas_waktu')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required" for="jenis_tl">Jenis Tindak Lanjut</label>
                            <input type="text" name="jenis_tl" value="{{ old('jenis_tl', $lhp->jenis_tl) }}"
                                class="form-control @error('jenis_tl') is-invalid @enderror" id="jenis_tl"
                                aria-describedby="jenis_tl" required>
                            @error('jenis_tl')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <input type="hidden" id="jadwal" name="jadwal" value="{{ $lhp->jadwal }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endforeach
@endsection
