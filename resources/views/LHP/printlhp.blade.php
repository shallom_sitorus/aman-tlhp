<!doctype html>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AMAN TLHP</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{!! asset('images/Kabupaten_Sukoharjo.png') !!}" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="{!! asset('css/typography.css') !!}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{!! asset('css/style.css') !!}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{!! asset('css/responsive.css') !!}">
</head>

<body>
    <h1 align="center">{{ $title }}</h1>
    <div class="iq-card-body">
        <div class="table-responsive">
            <table id="data-table" class="table table-striped table-bordered mt-3" cellpadding="1" border="1"
                role="grid" aria-describedby="user-list-page-info">
                <thead bgcolor="#066">
                    <tr class="text-white">
                        <th width="3%" rowspan="2" align="center">No</th>
                        <th width="10%" colspan="1" align="center">Entitas</th>
                        <th width="10%" rowspan="2" align="center">Tanggal Kirim LHP</th>
                        <th width="5%" rowspan="2" align="center">Kode Temuan</th>
                        <th width="15%" colspan="1" align="center">Temuan</th>
                        <th width="4%" align="center" rowspan="2">Kode Rekomendasi</th>
                        <th width="5%" align="center" rowspan="2">Jumlah Rekomendasi</th>
                        <th width="13%" align="center" rowspan="2">Rekomendasi</th>
                        <th width="13%" align="center" colspan="1">Tindak Lanjut/Tanggapan</th>
                        <th width="10%" align="center" rowspan="2">Batas Waktu Penyelesaian Tindak Lanjut</th>
                        <th width="7%" align="center" rowspan="2">Jenis Tindak Lanjut</th>
                    </tr>
                    <tr class="text-white">
                        <th align="center">Tanggal, No LHp, No KI</th>
                        <th align="center">(Uraian Ringkas)</th>
                        <th align="center">(Uraian Ringkas)</th>
                    </tr>

                </thead>
                <tbody>
                    @foreach ($listlhp as $lhp)
                        <tr nobr="true">
                            <td width="3%" align="center">{{ $loop->iteration }}</td>
                            <td width="10%" style="text-align: left">
                                @foreach ($lhp->jadwalpengawasan->opd as $opd)
                                    - {{ $opd->nama }} <br>
                                @endforeach <hr>
                                <div style="text-align: center">
                                    {{ date('d F Y', strtotime($lhp->tanggal_lhp)) }}<br>
                                    {{ $lhp->no_lhp }}
                                </div>
                            </td>
                            <td width="10%" align="center">{{ $lhp->tanggal_kirim }}</td>
                            <td width="5%" align="center">
                                @foreach ($lhp->temuan as $temuan)
                                    {{ $temuan->kode_temuan }}
                                    <br>
                                @endforeach
                            </td>
                            <td width="15%" align="center">
                                @foreach ($lhp->temuan as $temuan)
                                    {{ $temuan->pivot->uraian_temuan }}
                                    <br>
                                @endforeach
                            </td>
                            <td width="4%" align="center">
                                @foreach ($lhp->trlhp as $trlhp)
                                    {{ $trlhp->rekomendasi->kode_rekomendasi }}
                                    <br>
                                @endforeach
                            </td>
                            <td width="5%" align="center">{{ count($lhp->trlhp) }}</td>
                            <td width="13%" align="center">
                                @foreach ($lhp->trlhp as $trlhp)
                                    {{ $trlhp->uraian_rekomendasi}}
                                    <br>
                                @endforeach
                            </td>
                            <td width="13%" align="center">{{ $lhp->tanggapan }}</td>
                            <td width="10%" align="center">{{ $lhp->batas_waktu }}</td>
                            <td width="7%" align="center">{{ $lhp->jenis_tl }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="page-break"></div>
    <div class="iq-card-body">
        <div class="table-responsive">
            <table id="user-list-table" class="table table-striped table-bordered mt-3" border="1" role="grid"
                aria-describedby="user-list-page-info">
                <thead bgcolor="#066">
                    <tr class="text-white">
                        <th width="3%" rowspan="2" align="center">No</th>
                        <th rowspan="2" width="7%" align="center">No. Tindak Lanjut</th>
                        <th colspan="2" width="20%" align="center">Tanggal Terima Tindak Lanjut</th>
                        <th rowspan="2" width="7%" align="center">File Surat Tindak Lanjut</th>
                        <th rowspan="2" width="7%" align="center" class="px-4">Kategori Tindak Lanjut</th>
                        <th rowspan="2" width="17%" align="center" class="px-5">Keterangan</th>
                        <th colspan="3" width="24%" align="center">Progres Tindak Lanjut <br><small>(Tanggal
                                Terbit)</small></th>
                        <th rowspan="2" width="15%" align="center">No dan Tgl Surat Permintaan Tindakan
                            Tambahan
                        </th>
                    </tr>
                    <tr>
                        <th width="11%" align="center">Tanggal&nbsp;Surat&nbsp;TL</th>
                        <th width="9%" align="center">Tanggal Verifikasi&nbsp;TL</th>
                        <th width="8%" align="center">Surat Penagihan&nbsp;1</th>
                        <th width="8%" align="center">Surat Penagihan&nbsp;2</th>
                        <th width="8%" align="center">Surat Pemberitahuan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listlhp as $lhp)
                        {{-- @foreach ($lhp->tindaklanjut as $tl)     --}}
                        <tr nobr="true">
                            <td width="3%" align="center">{{ $loop->iteration }}</td>
                            @if ($lhp->tindaklanjut)
                                <td width="7%" align="center">{{ $lhp->tindaklanjut->no_tl }}</td>
                                <td width="11%" align="center">{{ $lhp->tindaklanjut->tgl_surat }}</td>
                                <td width="9%" align="center">{{ $lhp->tindaklanjut->tgl_verifikasi }}</td>
                                <td width="7%" align="center">
                                    @if ($lhp->tindaklanjut->file_surat_tl == null)
                                        Surat Tindak Lanjut Tidak Terlampir
                                    @else
                                        Surat Tindak Lanjut Terlampir
                                    @endif
                                </td>
                                <td width="7%" align="center">
                                    @if ($lhp->tindaklanjut->kategoritl)
                                        {{ $lhp->tindaklanjut->kategoritl->nama_kategori }}
                                    @endif
                                </td>
                                <td width="17%" align="center">{{ $lhp->tindaklanjut->keterangan }}</td>
                                <td width="8%" align="center"> {{ $lhp->tindaklanjut->tgl_penagihan1 }}</td>
                                <td width="8%" align="center">{{ $lhp->tindaklanjut->tgl_penagihan2 }}</td>
                                <td width="8%" align="center">{{ $lhp->tindaklanjut->tgl_terbit_pemberitahuan }}
                                </td>
                                <td width="15%" align="center">{{ $lhp->tindaklanjut->no_tindakan_tambahan }} <br>
                                    {{ $lhp->tindaklanjut->tgl_tindakan_tambahan }}
                                </td>
                            @else
                                <td width="97%" align="center">Belum di tindaklanjut</td>
                            @endif
                        </tr>
                        {{-- @endforeach --}}
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <style>
        .page-break {
            page-break-after: always;
        }
    </style>

</body>

</html>
