<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Hasil Pengawasan</title>
</head>

<body>
    <div class="judul">
        <font size="11" face="tahoma">PEMERINTAHAN KABUPATEN SUKOHARJO
            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INSPEKTORAT
            DAERAH
            <hr width="250" height="1">
        </font>
    </div>
    <div align="center">
        <font size="11" face="tahoma">LAPORAN HASIL PEMERIKSAAN REGULER</font>
    </div>
    <br>
    <font size="10" face="tahoma">
        <table width="50%">
            <tr>
                <th>PADA</th>
                <th>:</th>
                <td class="card bg-light mx-1 my-1">
                </td>
            </tr>
            <tr>
                <th>NOMOR</th>
                <th>:</th>
                <td class="card bg-light mx-1 my-1"></td>
            </tr>
            <tr>
                <th>TANGGAL</th>
                <th>:</th>
                <td class="card bg-light mx-1 my-1"></td>
            </tr>
            <tr>
                <th>LAMPIRAN</th>
                <th>:</th>
                <td class="card bg-light mx-1 my-1"></td>
            </tr>
            <tr>
                <th>TAHUN</th>
                <th>:</th>
                <td class="card bg-light mx-1 my-1"></td>
            </tr>
        </table>
    </font>
    <hr width="540" height="3">
    <!-- <hr width="540" height="1"> -->
    <div align="center" size="11" face="tahoma">
        <span>BAB I</span>
        <br>
        <br>TEMUAN DAN REKOMENDASI</br>
    </div>
    <div size="11" face="tahoma">
        <span align="justify">Berdasarkan hasil pemeriksaan terdapat temuan sebagai berikut:
            <br>
            1. Pengeluaran belum dipungut pajak sebesar Rp 710.000,00 (Tujuh ratus
            sepuluh ribu rupiah). (10301).
            <br>
            <br>
            Hal tersebut tidak sesuai dengan Undang-undang Republik Indonesia Nomor 42
            tahun 2009 tentang Perubahan Ketiga atas Undang-undang Nomor 8 tahun
            1983 tentang Pajak Pertambahan Nilai Barang dan Jasa dan Pajak Penjualan
            atas Barang Mewah pasal 7 ayat (1).
            Permasalahan tersebut berakibat terdapat pajak yang terutang sebesar Rp
            710.000,00 (Tujuh ratus sepuluh ribu rupiah).
            Hal ini disebabkan Bendahara BOS belum sepenuhnya memahami ketentuan
            perpajakan yang berlaku.
            Berkaitan dengan temuan tersebut Kepala UPTD SD Negeri Purbayan 01
            Kecamatan Baki telah menyepakati dan setuju untuk menindaklanjuti.</br></span>
        <p> Rekomendasi Inspektorat</p>
        <span align="justify">Kepala Dinas Pendidikan dan Kebudayaan Kabupaten Sukoharjo sebagai
            Penanggungjawab Manajemen BOS Tingkat Kabupaten agar memerintahkan
            secara tertulis kepada Bendahara BOS UPTD SD Negeri Purbayan 01
            Kecamatan Baki melalui Kepala UPTD SD Negeri Purbayan 01 untuk
            memungut dan menyetorkan pajak sebesar Rp 710.000,00 (Tujuh ratus sepuluh
            ribu rupiah) ke kas negara. (01).
        </span>
        <p align="justify">2. Pengeluaran Dana BOS yang tidak sesuai dengan Petunjuk Teknis BOS
            sebesar Rp 4.820.000,00 (Empat juta delapan ratus dua puluh ribu rupiah).
            (10109).
        </p>
    </div>
</body>

</html>