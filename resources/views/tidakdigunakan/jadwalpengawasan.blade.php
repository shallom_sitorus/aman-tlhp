@extends('layouts.home')

@section('content')
<div id="content-page" class="content-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Jadwal Pengawasan</h4>
                        </div>
                        <div class="card-toolbarn">
                            <a type="button" href="#" class="btn btn-sm btn-primary my-1">
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                                        class="bi bi-printer" viewBox="0 0 16 16">
                                        <path d="M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z" />
                                        <path
                                            d="M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2H5zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4V3zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2H5zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1z" />
                                    </svg>
                                </span>Print
                            </a>
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#tambahModalScrollable">
                                + Tambah
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="tambahModalScrollable" tabindex="-1" role="dialog"
                                aria-labelledby="tambahModalScrollableTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 align="center" class="modal-title" id="tambahModalScrollableTitle"
                                                style="padding-left: 30%" ;>Jadwal Pengawasan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="form-group">
                                                    <label for="InputENo">No</label>
                                                    <input type="text" class="form-control" id="InputNo"
                                                        aria-describedby="no">
                                                </div>
                                                <div class="form-group">
                                                    <label for="bulan">Bulan</label>
                                                    <input type="date" class="form-control" id="InputBulan">
                                                    <small id="no" class="form-text text-muted">Diisi dengan bulan
                                                        berjalan</small>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tgllhp">Tanggal Kirim LHP</label>
                                                    <input type="date" class="form-control" id="InputTanggal">
                                                </div>
                                                <div class="form-group">
                                                    <label>Kode Temuan</label>
                                                    <select class="form-control" name="kode" id="InputKode>
                                                        <option value=" Kode1">Kode1</option>
                                                        <option value="Kode1">Kode1</option>
                                                        <option value="Kode1">Kode1</option>
                                                        <option value="Kode1">Kode1</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="temuan">Temuan</label>
                                                    <input type="text" class="form-control" id="InputTemuan">
                                                </div>
                                                <div class="form-group">
                                                    <label>Kode Rekomendasi</label>
                                                    <select class="form-control" name="koderek" id="Inputkodere>
                                                        <option value=" Kode1">Kode1</option>
                                                        <option value="Kode1">Kode1</option>
                                                        <option value="Kode1">Kode1</option>
                                                        <option value="Kode1">Kode1</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="jmlh">Jumlah Rekomendasi</label>
                                                    <input type="text" class="form-control" id="InputJmlh">
                                                </div>
                                                <div class="form-group">
                                                    <label for="rekomendasi">Rekomendasi</label>
                                                    <input type="text" class="form-control" id="InputRekomendasi">
                                                </div>
                                                <div class="form-group">
                                                    <label for="tindaklanjut">Tindak Lanjut/Tanggapan</label>
                                                    <input type="text" class="form-control" id="InputRekomendasi">
                                                </div>
                                                <div class="form-group">
                                                    <label for="batas">Batas Waktu Penyelesaian Tindak Lanjut</label>
                                                    <input type="text" class="form-control" id="InputRekomendasi">
                                                </div>
                                                <div class="form-group">
                                                    <label for="jenis">Jenis Tindak Lanjut</label>
                                                    <input type="text" class="form-control" id="InputRekomendasi">
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Tutup</button>
                                            <button type="button" class="btn btn-primary">Kirim</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-items-center mt-3">
                        <div class="col-sm-12 col-md-6">
                            <div id="user_list_datatable_info" class="dataTables_filter">
                                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                            </div>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="table-responsive">
                            <table id="user-list-table" class="table table-striped table-bordered mt-5" role="grid"
                                aria-describedby="user-list-page-info">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Action</th>
                                        <th rowspan="2">NO</th>
                                        <th rowspan="2">Bulan</th>
                                        <th colspan="1">Obrik</th>
                                        <th rowspan="2">Tanggal Kirim LHP</th>
                                        <th rowspan="2">Kode Temuan</th>
                                        <th colspan="1">Temuan</th>
                                        <th rowspan="2">Kode Rekomendasi</th>
                                        <th rowspan="2">Jumlah Rekomendasi</th>
                                        <th rowspan="2">Rekomendasi</th>
                                        <th colspan="1">Tindak Lanjut/Tanggapan</th>
                                        <th rowspan="2">Batas Waktu Penyelesaian</th>
                                        <th rowspan="2">Jenis Tindak Lanjut</th>
                                        <th rowspan="2">Nomor Tindak Lanjut</th>
                                        <th colspan="2">Tanggal Terima Tindak Lanjut</th>
                                        <th colspan="3">Kategori Tindak Lanjut</th>
                                        <th rowspan="2">Keterangan</th>
                                        <th colspan="3">Progres Tindak Lanjut</th>
                                        <th rowspan="2">No dan Tanggal Surat Permintaan Tindakan Tambahan</th>

                                    </tr>
                                    <tr>
                                        <th>Tanggal, No LHp, No KI</th>
                                        <th>(Uraian Ringkas)</th>
                                        <th>(Uraian Ringkas)</th>
                                        <th>Tanggal Surat TL</th>
                                        <th>Tanggal Verifikasi TL/th>
                                        <th>Sesuai Dengan Rekomendasi</th>
                                        <th>Dalam Proses</th>
                                        <th>Belum Ditindaklanjuti</th>
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <td>
                                        <div class="d-flex align-items-center list-user-action">
                                            <a data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title="Details" href="#"><i class="ri-eye-line"></i></a>
                                            <a data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title="Edit" href="#"><i class="ri-pencil-line"></i></a>
                                            <a data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title="Delete" href="#"><i
                                                    class="ri-delete-bin-line"></i></a>
                                        </div>
                                    </td>
                                    <td>1.</td>
                                    <td>September</td>
                                    <td>702/005/WI/2022
                                        09-09-2022</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tbody>
                            </table>
                        </div>
                        <div class="row justify-content-between mt-3">
                            <div id="user-list-page-info" class="col-md-6">
                                <span>Showing 1 to 5 of 5 entries</span>
                            </div>
                            <div class="col-md-6">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-end mb-0">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" tabindex="-1"
                                                aria-disabled="true">Previous</a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#">Next</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection