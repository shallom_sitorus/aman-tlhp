@extends('layouts.home')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Temuan</h4>
                            </div>
                            <div class="card-toolbarn">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#tambahTemuan">
                                    + Tambah
                                </button>
                            </div>
                        </div>
                        <div class="d-flex justify-items-center mt-3">
                            <div class="col-sm-12 col-md-6">
                                <div id="user_list_datatable_info" class="dataTables_filter">
                                    <input class="form-control me-2" type="search" placeholder="Cari.." name="search"
                                        aria-label="Search" id="searchInput">
                                </div>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered mt-1" role="grid"
                                    aria-describedby="user-list-page-info" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Temuan</th>
                                            <th>Nama Temuan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($temuan as $tem)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $tem->kode_temuan }}</td>
                                                <td>{{ $tem->nama_temuan }}</td>
                                                <td class="d-flex justify-content-center">
                                                    <a class="btn btn-light btn-sm mx-1" data-toggle="modal"
                                                        data-target="#editTemuan{{ $tem->id }}" data-placement="top"
                                                        title="Edit" data-original-title="Edit" href="#"><span
                                                            class="svg-icon svg-icon-3">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                                height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                                                <path opacity="0.3"
                                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                                                <path
                                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                                            </svg>
                                                        </span>
                                                    </a>
                                                    <form class="btn-delete"
                                                        action="{{ url('/delete-temuan/' . $tem->id) }}" method="post">
                                                        @method('delete')
                                                        @csrf
                                                        <button type="submit" class="btn btn-light btn-sm mx-1" data-placement="top" title="Delete"
                                                            data-original-title="Delete">
                                                            <span class="svg-icon svg-icon-3"><svg
                                                                    xmlns="http://www.w3.org/2000/svg" width="16"
                                                                    height="16" viewBox="0 0 24 24" fill="#f1416c">
                                                                    <path
                                                                        d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" />
                                                                    <path opacity="0.5"
                                                                        d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" />
                                                                    <path opacity="0.5"
                                                                        d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" />
                                                                </svg></span>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Tambah Temuan -->
    <div class="modal fade" id="tambahTemuan" tabindex="-1" role="dialog" aria-labelledby="tambahTemuanTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="tambahModalScrollableTitle" style="padding-left: 41%">
                        Temuan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/create-temuan" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="required">Kode Temuan</label>
                            <input type="text" name="kode_temuan" value="{{ old('kode_temuan') }}"
                                class="form-control @error('kode_temuan') is-invalid @enderror" id="kode_temuan"
                                aria-describedby="kode_temuan" required autofocus>
                            @error('kode_temuan')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required" for="nama_temuan">Nama Temuan</label>
                            <input type="text" name="nama_temuan" value="{{ old('nama_temuan') }}"
                                class="form-control @error('nama_temuan') is-invalid @enderror" id="nama_temuan"
                                aria-describedby="nama_temuan" required>
                            @error('nama_temuan')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Edit Temuan -->
    @foreach ($temuan as $tem)
        <div class="modal fade" id="editTemuan{{ $tem->id }}" tabindex="-1" role="dialog"
            aria-labelledby="editTemuanTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 align="center" class="modal-title" id="editModalScrollableTitle" style="padding-left: 41%">
                            Temuan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('/update-temuan/' . $tem->id) }}" method="post"
                            enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="form-group">
                                <label class="required">Kode Temuan</label>
                                <input type="text" name="kode_temuan"
                                    value="{{ old('kode_temuan', $tem->kode_temuan) }}"
                                    class="form-control @error('kode_temuan') is-invalid @enderror" id="kode_temuan"
                                    aria-describedby="kode_temuan" required>
                                @error('kode_temuan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="required" for="nama_temuan">Nama Temuan</label>
                                <input type="text" name="nama_temuan"
                                    value="{{ old('nama_temuan', $tem->nama_temuan) }}"
                                    class="form-control @error('nama_temuan') is-invalid @enderror" id="nama_temuan"
                                    aria-describedby="nama_temuan" required>
                                @error('nama_temuan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection
