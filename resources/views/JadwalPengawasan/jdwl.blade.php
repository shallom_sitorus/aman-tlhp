@extends('layouts.home')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Jadwal Pengawasan</h4>
                            </div>
                            <div class="card-toolbarn">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#tambahModalScrollable">
                                    + Tambah
                                </button>
                            </div>
                        </div>
                        <div class="d-flex justify-items-center mt-3">
                            <div class="col-sm-12 col-md-6">
                                <div id="user_list_datatable_info" class="dataTables_filter">
                                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"
                                        id="searchInput">
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <form action="{{ url('/JadwalPengawasan') }}" method="GET">
                                    @csrf
                                    <div class="form-group d-flex">
                                        <label class="px-2 mt-2">Tahun</label>
                                        <select class="form-control form-control-sm" name="rmp">
                                            @foreach ($tahun as $thn)
                                                <option value="{{ $thn }}" {{ request()->rmp == $thn ? 'selected' : '' }}>{{ $thn }}</option>
                                            @endforeach
                                        </select>
                                        <button type="submit" class="btn btn-sm btn-secondary"
                                            style="margin-left: 5px;">Cari</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered" role="grid"
                                    aria-describedby="user-list-page-info">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">Action</th>
                                            <th rowspan="2">No</th>
                                            <th rowspan="2" class="px-5">Entitas</th>
                                            <th rowspan="2">Tim Pemeriksa</th>
                                            <th rowspan="2">Jenis Pengawasan</th>
                                            <th rowspan="2">Jadwal</th>
                                            <th rowspan="2">Area Pengawasan</th>
                                            <th rowspan="2" class="px-5">Tujuan/Sasaran</th>
                                            <th colspan="6">Hari Pemeriksaan</th>
                                            <th rowspan="2">Jumlah Laporan</th>
                                            <th rowspan="2" class="px-5">Keterangan</th>
                                        </tr>
                                        <tr>
                                            <th>PJ</th>
                                            <th>WP</th>
                                            <th>PT</th>
                                            <th>KT</th>
                                            <th>AT</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($jadwal as $jdw)
                                            <tr>
                                                <td>
                                                    <div class="d-flex justify-items-center ">
                                                        <a class="btn btn-light btn-sm mx-1" 
                                                            data-placement="top" title="Detail" data-original-title="Detail"
                                                            href="{{ url('DetailJadwal/' . $jdw->id . '') }}"><span
                                                                class="svg-icon svg-icon-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                                    height="16" fill="#7239ea">
                                                                    <path
                                                                        d=" M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                                                    <path
                                                                        d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
                                                                </svg>
                                                            </span>
                                                        </a>
                                                        <form class="btn-delete"
                                                            action="{{ url('Delete-jadwal/' . $jdw->id . '') }}"
                                                            method="POST">
                                                            @method('delete')
                                                            @csrf
                                                            <button type="submit" class="btn btn-light btn-sm mx-1"
                                                                 data-placement="top" title="Delete"
                                                                data-original-title="Delete">
                                                                <span class="svg-icon svg-icon-3">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                                        height="16" viewBox="0 0 24 24" fill="#f1416c">
                                                                        <path
                                                                            d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" />
                                                                        <path opacity="0.5"
                                                                            d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" />
                                                                        <path opacity="0.5"
                                                                            d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" />
                                                                    </svg>
                                                                </span>
                                                            </button>
                                                        </form>
                                                    </div>
                                                    <p></p>
                                                    <a href="{{ url('lhp/' . $jdw->id) }}" type="button"
                                                        class="btn btn-primary">LHP</button>
                                                </td>
                                                <td>{{ $loop->iteration }}</td>
                                                {{-- <td align="left">{{ $jdw->opd->nama }}</td> --}}
                                                <td style="text-align: left">
                                                    <table width="100%" border="0" cellspacing="1" cellpadding="1"
                                                    class="table table-condensed">
                                                    @foreach ($jdw->opd as $opd)
                                                    <tr>
                                                        <td class="px-5">
                                                            {{ $opd->nama }}
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                                    {{-- @foreach ($jdw->opd as $opd)
                                                    - {{ $opd->nama }} <br>
                                                    @endforeach --}}
                                                </td>
                                                {{-- <td align="left">-</td> --}}
                                                <td align="left">
                                                    {{ $jdw->timpemeriksa1->jenis_tp1 ?? '-'}}
                                                </td>
                                                <td align="left">
                                                    <table width="100%" border="0" cellspacing="1" cellpadding="1"
                                                        class="table table-condensed">
                                                        <tr>
                                                            <td class="px-3">Jenis&nbsp;Pengawasan</td>
                                                            <td class="px-5">{{ $jdw->jenispemeriksaan1->jenis_jp1 ?? '-'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Rincian</td>
                                                            <td>{{ $jdw->jenispemeriksaan2->jenis_jp2 ?? '-'}}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left">
                                                    <table width="100%" border="0" cellspacing="1" cellpadding="1"
                                                        class="table table-condensed">
                                                        <tr>
                                                            <td>RMP</td>
                                                            <td class="px-4" style="white-space:nowrap;">{{ $jdw->rmp }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>RSP</td>
                                                            <td>{{ $jdw->rsp }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>RPL</td>
                                                            <td> {{ $jdw->rpl }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>HP</td>
                                                            <td> {{ $jdw->hp }}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>{{ $jdw->area_pengawasan }}</td>
                                                <td style="text-align: justify">{{ $jdw->tujuan_sasaran }}</td>
                                                <td>{{ $jdw->pj }}</td>
                                                <td>{{ $jdw->wp }}</td>
                                                <td>{{ $jdw->pt }}</td>
                                                <td>{{ $jdw->kt }}</td>
                                                <td>{{ $jdw->at }}</td>
                                                <?php
                                                $a = (int) $jdw->pj;
                                                $b = (int) $jdw->wp;
                                                $c = (int) $jdw->pt;
                                                $d = (int) $jdw->kt;
                                                $e = (int) $jdw->at;
                                                $total = $a + $b + $c + $d + $e;
                                                ?>
                                                <td>{{ $total }}</td>
                                                <td>{{ $jdw->lhp()->count() }} LHP</td>
                                                <td style="text-align: justify">{{ $jdw->keterangan }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Tambah Jadwal Pengawasan -->
    <div class="modal fade" id="tambahModalScrollable" role="dialog" aria-labelledby="tambahModalScrollableTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="tambahModalScrollableTitle" style="padding-left: 32%" ;>
                        Jadwal Pengawasan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('Tambah-jadwal') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="required" >Tim Pemeriksa</label>
                            <select class="form-control" data-width="100%" name="tim_pemeriksa1" id="selectTim1"
                                required>
                                <option selected>-- Silahkan Pilih --</option>
                                @foreach ($t_p1 as $id => $name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="required" >Jenis Pengawasan</label>
                            <select class="form-control" data-width="100%" name="jenis_pemeriksaan1" id="selectJenis1"
                                required>
                                <option selected>-- Silahkan Pilih --</option>
                                @foreach ($j_p1 as $id => $name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="required" >Rincian Jenis Pengawasan</label>
                            <select class="form-control" name="jenis_pemeriksaan2" id="selectJenis2" required> 
                                <option selected>-- Pilih Jenis Pengawasan Dahulu --</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="required" >Entitas</label>
                            <select class="selectpicker" name="obrik[]" multiple data-live-search="true"
                            title="Pilih Entitas" data-selected-text-format="count > 3"
                            data-width="100%" required>
                                @foreach ($obrik as $id => $name)
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jadwal</label>
                        </div>
                        <div class="mb-3 row">
                            <label class="required"  for="inputRmp" class="col-sm-2 col-form-label">RMP</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control @error('rmp') is-invalid @enderror"
                                    id="rmp" name="rmp" value="{{ old('rmp') }}" required>
                                @error('rmp')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required"  for="inputRmp" class="col-sm-2 col-form-label">RSP</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control @error('rsp') is-invalid @enderror"
                                    id="rsp" name="rsp" value="{{ old('rsp') }}" required>
                                @error('rsp')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required"  for="inputRmp" class="col-sm-2 col-form-label">RPL</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control @error('rpl') is-invalid @enderror"
                                    id="rpl" name="rpl" value="{{ old('rpl') }}" required>
                                @error('rpl')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required"  for="inputRmp" class="col-sm-2 col-form-label">HP</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('hp') is-invalid @enderror"
                                    name="hp" id="hp" value="{{ old('hp') }}" required>
                                @error('hp')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="required"  for="Inputanggaran">Anggaran</label>
                            <input type="number" placeholder="input hanya berupa angka"
                                class="form-control @error('anggaran') is-invalid @enderror" id="anggaran"
                                aria-describedby="anggaran" name="anggaran" value="{{ old('anggaran') }}" required>
                            @error('anggaran')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required"  for="">Area Pengawasan</label>
                            <input type="text" class="form-control @error('area_pengawasan') is-invalid @enderror"
                                id="area_pengawasan" aria-describedby="anggaran" name="area_pengawasan"
                                value="{{ old('area_pengawasan') }}" required>
                            @error('area_pengawasan')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required"  for="">Tujuan/Sasaran</label>
                            <input type="text" class="form-control @error('tujuan_sasaran') is-invalid @enderror"
                                id="" aria-describedby="anggaran" name="tujuan_sasaran"
                                value="{{ old('tujuan_sasaran') }}" required>
                            @error('tujua_sasaran')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required"  for="">Ruang Lingkup</label>
                            <input type="text" class="form-control @error('ruang_lingkup') is-invalid @enderror"
                                id="" aria-describedby="anggaran" name="ruang_lingkup"
                                value="{{ old('ruang_lingkup') }}" required>
                            @error('ruang_lingkup')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required"  for="">Sarana dan Prasarana</label>
                            <input type="text" class="form-control @error('sarana') is-invalid @enderror"
                                id="" aria-describedby="anggaran" name="sarana" value="{{ old('sarana') }}"
                                required>
                            @error('sarana')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required"  for="">Tingkat Risiko</label>
                            <select class="form-control" name="tingkat_risiko" id="tingkat_risiko">
                                <option selected>-- Silahkan Pilih --</option>
                                <option value="Tinggi">Tinggi</option>
                                <option value="Sedang">Sedang</option>
                                <option value="Rendah">Rendah</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label  for="">Keterangan</label>
                            <textarea class="form-control @error('keterangan') is-invalid @enderror" placeholder="" name="keterangan"
                                id="" style="height: 100px">{{ old('keterangan') }}</textarea>
                            @error('keterangan')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required"  for="">Nama Tim Audit</label>
                            <input type="text" class="form-control" id="" aria-describedby="nama"
                                name="timaudit[]" value="{{ old('tim_audit') }}" required>
                        </div>
                        <div class="form-group">
                            <label class="required" >Kedudukan</label>
                            <select class="form-control" data-width="100%" name="kedudukan[]" id="" required>
                                <option selected>-- Silahkan Pilih --</option>
                                <option value="Penanggung Jawab">Penanggung Jawab</option>
                                <option value="Wakil Penanggung Jawab">Wakil Penanggung Jawab</option>
                                <option value="Pengendali Teknis">Pengendali Teknis</option>
                                <option value="Ketua">Ketua</option>
                                <option value="Anggota">Anggota</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for=""></label>
                            <a href="#" class="addTimaudit btn btn-primary" style="float: right;">Tambah Tim
                                Audit</a>
                        </div>
                        <div class="tim-audit"></div>
                        <div class="form-group">
                            <label>Hari Pemeriksaan</label>
                        </div>
                        <div class="mb-3 row">
                            <label class="required"  for="inputRmp" class="col-sm-2 col-form-label">PJ</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('pj') is-invalid @enderror"
                                    id="pj" name="pj" value="{{ old('pj') }}" required>
                                @error('pj')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required"  for="inputRmp" class="col-sm-2 col-form-label">WP</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('wp') is-invalid @enderror"
                                    id="wp" name="wp" value="{{ old('wp') }}" required>
                                @error('wp')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required"  for="inputRmp" class="col-sm-2 col-form-label">PT</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('pt') is-invalid @enderror"
                                    id="pt" name="pt" value="{{ old('pt') }}" required>
                                @error('pt')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required"  for="inputRmp" class="col-sm-2 col-form-label">KT</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('kt') is-invalid @enderror"
                                    id="kt" name="kt" value="{{ old('kt') }}" required>
                                @error('kt')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required"  for="inputRmp" class="col-sm-2 col-form-label">AT</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('at') is-invalid @enderror"
                                    name="at" id="at" value="{{ old('at') }}" required>
                                @error('at')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script>
        $('.addTimaudit').on('click', function() {
            addTim();
        });

        function addTim() {
            var timaudit =
                '<div><div class="form-group"><label class="required">Nama Tim Audit</label><input type="text" class="form-control" id=""aria-describedby="nama" name="timaudit[]" value="{{ old('tim_audit') }}" required</div><div class="form-group"><label class="required">Kedudukan</label><select class="form-control" data-width="100%" name="kedudukan[]" id="" required><option selected>-- Silahkan Pilih --</option><option value="Penanggung Jawab">Penanggung Jawab</option><option value="Wakil Penanggung Jawab">Wakil Penanggung Jawab</option><option value="Pengendali Teknis">Pengendali Teknis</option><option value="Ketua">Ketua</option><option value="Anggota">Anggota</option></select></div><div class="form-group"><label for=""></label><a href="#" class="remove btn btn-danger" style="float: right;">Hapus</a></div></div>';
            $('.tim-audit').append(timaudit);
        };
        $('.remove').live('click', function() {
            $(this).parent().parent().parent().remove();
        })
    </script>

    
@endsection
