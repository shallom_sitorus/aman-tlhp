@extends('layouts.home')

@section('content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @foreach ($jadwal as $jdw)
                        <div class="iq-card">
                            <div class="iq-header-title">
                                <div style="padding-top: 15px; padding-left: 15px">
                                    <a href="/JadwalPengawasan">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#0084ff"
                                            class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                                            <path
                                                d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <div class="detail">
                                <table width="100%">
                                    <tr>
                                        <th>Tim Pemeriksa</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">
                                            {{ $jdw->timpemeriksa1->jenis_tp1 }}
                                        </td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#edittim{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <th>Jenis Pengawasan</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">
                                            <table width="100%" border="0" cellspacing="1" cellpadding="1"
                                                class="table table-condensed">
                                                <tr>
                                                    <td><b>Jenis Pengawasan</b></td>
                                                    <td width="64%">{{ $jdw->jenispemeriksaan1->jenis_jp1 }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>Rincian</b></td>
                                                    <td width="64%">{{ $jdw->jenispemeriksaan2->jenis_jp2 }}</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <a class="editJenisPengawasan btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#editjenis" data-id="{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Entitas</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">
                                            <table width="100%" border="0" cellspacing="1" cellpadding="1"
                                            class="table table-condensed">
                                            @foreach ($jdw->opd as $opd)
                                            <tr>
                                                <td>
                                                    {{ $opd->nama }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#editobrik{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Jadwal</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">
                                            <table width="100%" border="0" cellspacing="1" cellpadding="1"
                                                class="table table-condensed">
                                                <tr>
                                                    <td><b>RMP</b></td>
                                                    <td>{{ $jdw->rmp }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>RSP</b></td>
                                                    <td>{{ $jdw->rsp }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>RPL</b></td>
                                                    <td> {{ $jdw->rpl }}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>HP</b></td>
                                                    <td>{{ $jdw->hp }}</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#editjadwal{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Anggaran</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">@currency($jdw->anggaran)</td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#editanggaran{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Area Pengawasan</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $jdw->area_pengawasan }}</td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#editarea{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Tujuan/Sasaran</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $jdw->tujuan_sasaran }}</td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#edittujuan{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>Ruang Lingkup</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $jdw->ruang_lingkup }}</td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#editruang{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Tim Audit</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">
                                            <table width="100%" border="0" cellspacing="1" cellpadding="1"
                                                class="table table-condensed">
                                                <tr>
                                                    <td><b>Kedudukan</b></td>
                                                    <td><b>Nama Audit</b></td>
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-success rounded-pill" data-toggle="modal"
                                                        data-target="#tambahTimaudit">+ Tambah</button>
                                                    </td>
                                                </tr>
                                                @foreach ($jdw->audit as $tim_audit)
                                                    <tr>
                                                        <td>{{ $tim_audit->kedudukan }}</td>
                                                        <td>{{ $tim_audit->nama }}</td>
                                                        <td>
                                                            <div class="d-flex justify-items-center ">
                                                                <a class="btn btn-light btn-sm mx-1" data-toggle="modal"
                                                                    data-placement="top" title="Edit"
                                                                    data-original-title="Edit"
                                                                    data-target="#editTimaudit{{ $tim_audit->id }}">
                                                                    <span class="svg-icon svg-icon-3">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                            width="16" height="16"
                                                                            viewBox="0 0 24 24" fill="#04c8c8">
                                                                            <path opacity="0.3"
                                                                                d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                                                            <path
                                                                                d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                                                        </svg>
                                                                    </span>
                                                                </a>
                                                                <form class="btn-delete"
                                                                    action="{{ url('delete-timaudit/' . $tim_audit->id . '') }}"
                                                                    method="POST">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <button type="submit" class="btn btn-light btn-sm mx-1"
                                                                        data-placement="top"
                                                                        title="Delete" data-original-title="Delete">
                                                                        <span class="svg-icon svg-icon-3">
                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                width="16" height="16"
                                                                                viewBox="0 0 24 24" fill="#f1416c">
                                                                                <path
                                                                                    d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" />
                                                                                <path opacity="0.5"
                                                                                    d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" />
                                                                                <path opacity="0.5"
                                                                                    d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" />
                                                                            </svg>
                                                                        </span>
                                                                    </button>
                                                                </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Hari Pemeriksaan</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">
                                            <table width="100%" border="0" cellspacing="1" cellpadding="1"
                                                class="table table-condensed">
                                                <tr>
                                                    <td><b>PJ</b></td>
                                                    <td><b>WP</b></td>
                                                    <td><b>PT</b></td>
                                                    <td><b>KT</b></td>
                                                    <td><b>AT</b></td>
                                                    <td><b>Jumlah</b></td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $jdw->pj }}</td>
                                                    <td>{{ $jdw->wp }}</td>
                                                    <td>{{ $jdw->kt }}</td>
                                                    <td>{{ $jdw->pt }}</td>
                                                    <td>{{ $jdw->at }}</td>
                                                    <?php
                                                    $a = (int) $jdw->pj;
                                                    $b = (int) $jdw->wp;
                                                    $c = (int) $jdw->pt;
                                                    $d = (int) $jdw->kt;
                                                    $e = (int) $jdw->at;
                                                    $total = $a + $b + $c + $d + $e;
                                                    ?>
                                                    <td>{{ $total }}</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#edithari{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Jumlah Laporan</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $jdw->lhp()->count() }} LHP</td>
                                    </tr>
                                    <tr>
                                        <th>Sarana dan Prasarana</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $jdw->sarana }}</td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#editsarana{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Tingkat Risiko</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $jdw->tingkat_risiko }}</td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#edittingkat{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Keterangan</th>
                                        <th>:</th>
                                        <td class="card bg-light mx-1 my-1">{{ $jdw->keterangan }}</td>
                                        <td>
                                            <a class="btn btn-sm mx-1" data-toggle="modal"
                                            data-placement="top" title="Edit" data-original-title="Edit"
                                            data-target="#editketerangan{{ $jdw->id }}"><span class="svg-icon svg-icon-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="#04c8c8">
                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" />
                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" />
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit Tim Pemeriksa-->
    @foreach ($jadwal as $jdw)
            <div class="modal fade" id="edittim{{ $jdw->id }}" tabindex="-1" role="dialog"
                aria-labelledby="editTimTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="editTimTitle" style="padding-left: 41%" ;>
                                Tim Pemeriksa</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/update-timpemeriksa/' . $jdw->id) }}" method="post"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="required">Tim Pemeriksa</label>
                                    <select class="form-control" data-width="100%" name="tim_pemeriksa1" id=""
                                        required>
                                        @foreach ($t_p1 as $id => $name)
                                            <option value="{{ $id }}"
                                                {{ $jdw->tim_pemeriksa1 == $id ? 'selected' : '' }}>{{ $name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
    @endforeach

    <!-- Modal Edit Jenis Pengawasan-->
    @foreach ($jadwal as $jdw)
            <div class="modal fade" id="editjenis" tabindex="-1" role="dialog"
                aria-labelledby="editjenisTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="editjenisTitle" style="padding-left: 35%" ;>
                                Jenis Pengawasan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/update-jenispengawasan/' . $jdw->id) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="" id="jadwal_id">
                                <div class="form-group">
                                    <label class="required">Jenis Pengawasan</label>
                                    <select class="form-control" data-width="100%" name="jenis_pemeriksaan1"
                                        id="updateJenis1" required>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="required">Rincian Jenis Pengawasan</label>
                                    <select class="form-control" name="jenis_pemeriksaan2" id="updateJenis2"
                                        required></select>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
    @endforeach

     <!-- Modal Edit Obrik-->
     @foreach ($jadwal as $jdw)
     <div class="modal fade" id="editobrik{{ $jdw->id }}" tabindex="-1" role="dialog"
         aria-labelledby="editObrikTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-center" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 align="center" class="modal-title" id="editObrikTitle" style="padding-left: 41%" ;>
                         Entitas</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">
                     <form action="{{ url('/update-obrik/' . $jdw->id) }}" method="post"
                         enctype="multipart/form-data">
                         @csrf
                         <div class="form-group">
                             <label class="required">Entitas</label>
                             <select class="selectpicker" name="obrik[]" multiple data-live-search="true"
                                    title="Pilih Entitas" data-selected-text-format="count > 3" data-width="100%">
                                    @foreach ($obrik as $id => $name)
                                        <option value="{{ $id }}"
                                            @foreach ($jdw->opd as $opd){{ $opd->pivot->id_opd == $id ? 'selected' : '' }} @endforeach>
                                            {{ $name }}</option>
                                    @endforeach
                                </select>
                         </div>
                         <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                     <button type="submit" class="btn btn-primary">Ubah</button>
                 </div>
                 </form>
             </div>
         </div>
     </div>
@endforeach

 <!-- Modal Edit Jadwal RMP RSP RPL HP-->
 @foreach ($jadwal as $jdw)
 <div class="modal fade" id="editjadwal{{ $jdw->id }}" tabindex="-1" role="dialog"
     aria-labelledby="editJadwalTitle" aria-hidden="true">
     <div class="modal-dialog modal-dialog-scrollable" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 align="center" class="modal-title" id="editJadwalTitle" style="padding-left: 41%" ;>
                     Jadwal</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <form action="{{ url('/update-jadwal/' . $jdw->id) }}" method="post"
                     enctype="multipart/form-data">
                     @csrf
                     <div class="mb-3 row">
                        <label class="required" for="inputRmp" class="col-sm-2 col-form-label">RMP</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control @error('rmp') is-invalid @enderror"
                                name="rmp" id="rmp_edit" value="{{ $jdw->rmp }}" required>
                            @error('rmp')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="required" for="inputRmp" class="col-sm-2 col-form-label">RSP</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control @error('rsp') is-invalid @enderror"
                                name="rsp" id="rsp_edit" value="{{ $jdw->rsp }}" required>
                            @error('rsp')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="required" for="inputRmp" class="col-sm-2 col-form-label">RPL</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control @error('rpl') is-invalid @enderror"
                                name="rpl" id="rpl_edit" value="{{ $jdw->rpl }}" required>
                            @error('rpl')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label class="required" for="inputRmp" class="col-sm-2 col-form-label">HP</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('hp') is-invalid @enderror"
                                name="hp" id="hp_edit" value="{{ $jdw->hp }}" required>
                            @error('hp')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                     <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                 <button type="submit" class="btn btn-primary">Ubah</button>
             </div>
             </form>
         </div>
     </div>
 </div>
@endforeach

     <!-- Modal Edit Anggaran-->
     @foreach ($jadwal as $jdw)
     <div class="modal fade" id="editanggaran{{ $jdw->id }}" tabindex="-1" role="dialog"
         aria-labelledby="editanggaranTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-scrollable" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 align="center" class="modal-title" id="editanggaranTitle" style="padding-left: 41%" ;>
                         Anggaran</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">
                     <form action="{{ url('/update-anggaran/' . $jdw->id) }}" method="post"
                         enctype="multipart/form-data">
                         @csrf
                         <div class="form-group">
                            <label class="required" for="Inputanggaran">Anggaran</label>
                            <input type="number" placeholder="input hanya berupa angka"
                                class="form-control @error('anggaran') is-invalid @enderror" id="anggaran_edit"
                                aria-describedby="anggaran" name="anggaran" value="{{ $jdw->anggaran }}" required>
                            @error('anggaran')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                         <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                     <button type="submit" class="btn btn-primary">Ubah</button>
                 </div>
                 </form>
             </div>
         </div>
     </div>
@endforeach

      <!-- Modal Edit Area Pengawasan-->
    @foreach ($jadwal as $jdw)
    <div class="modal fade" id="editarea{{ $jdw->id }}" tabindex="-1" role="dialog"
        aria-labelledby="editareaTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="editareaTitle" style="padding-left: 35%" ;>
                        Area Pengawasan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/update-area/' . $jdw->id) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="required" for="">Area Pengawasan</label>
                            <input type="text" class="form-control @error('area_pengawasan') is-invalid @enderror"
                                name="area_pengawasan" id="area_pengawasan_edit" aria-describedby="anggaran"
                                name="area_pengawasan" value="{{ $jdw->area_pengawasan }}" required>
                            @error('area_pengawasan')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endforeach

      <!-- Modal Edit Tujuan/Sasaran-->
    @foreach ($jadwal as $jdw)
    <div class="modal fade" id="edittujuan{{ $jdw->id }}" tabindex="-1" role="dialog"
        aria-labelledby="edittujuanTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="edittujuanTitle" style="padding-left: 37%" ;>
                        Tujuan/Sasaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/update-tujuan/' . $jdw->id) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="required" for="">Tujuan/Sasaran</label>
                            <input type="text" class="form-control @error('tujuan_sasaran') is-invalid @enderror"
                                name="tujuan_sasaran" id="sasaran_edit" aria-describedby="anggaran"
                                name="tujuan_sasaran" value="{{ $jdw->tujuan_sasaran }}" required>
                            @error('tujua_sasaran')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endforeach

     <!-- Modal Edit Ruang Lingkup-->
     @foreach ($jadwal as $jdw)
     <div class="modal fade" id="editruang{{ $jdw->id }}" tabindex="-1" role="dialog"
         aria-labelledby="editruangTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-scrollable" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 align="center" class="modal-title" id="editruangTitle" style="padding-left: 37%" ;>
                         Ruang Lingkup</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="modal-body">
                     <form action="{{ url('/update-ruang/' . $jdw->id) }}" method="post"
                         enctype="multipart/form-data">
                         @csrf
                         <div class="form-group">
                            <label class="required" for="">Ruang Lingkup</label>
                            <input type="text" class="form-control @error('ruang_lingkup') is-invalid @enderror"
                                name="ruang_lingkup" id="ruang_lingkup_edit" aria-describedby="anggaran"
                                name="ruang_lingkup" value="{{ $jdw->ruang_lingkup }}" required>
                            @error('ruang_lingkup')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                         <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                     <button type="submit" class="btn btn-primary">Ubah</button>
                 </div>
                 </form>
             </div>
         </div>
     </div>
@endforeach
     
    <!-- Modal Tambah Tim Audit-->
    <div class="modal fade" id="tambahTimaudit" tabindex="-1" role="dialog" aria-labelledby="tambahTimauditScrollableTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 align="center" class="modal-title" id="tambahTimauditScrollableTitle" style="padding-left: 45%" ;>
                        Tim Audit
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('/create-timaudit') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="required" for="nama">Nama</label>
                            <input type="text" name="nama" value="{{ old('nama') }}"
                                class="form-control @error('nama') is-invalid @enderror" id="nama"
                                aria-describedby="nama" required>
                            @error('nama')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="required">Kedudukan</label>
                            <select class="form-control" name="kedudukan" id="kedudukan" required>
                                <option value="" selected>-- Pilih Kedudukan --</option>
                                <option value="Penanggung Jawab">Penanggung Jawab</option>
                                <option value="Wakil Penanggung Jawab">Wakil Penanggung Jawab</option>
                                <option value="Pengendali Teknis">Pengendali Teknis</option>
                                <option value="Ketua">Ketua</option>
                                <option value="Anggota">Anggota</option>
                            </select>
                        </div>
                        <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Edit Tim Audit-->
    @foreach ($jadwal as $jdw)
        @foreach ($jdw->audit as $tim_audit)
            <div class="modal fade" id="editTimaudit{{ $tim_audit->id }}" tabindex="-1" role="dialog"
                aria-labelledby="editTimauditTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 align="center" class="modal-title" id="editTimauditTitle" style="padding-left: 41%" ;>
                                Tim Audit</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('/update-timaudit/' . $tim_audit->id) }}" method="post"
                                enctype="multipart/form-data">
                                @method('put')
                                @csrf
                                <div class="form-group">
                                    <label class="required" for="nama">Nama</label>
                                    <input type="text" name="nama" value="{{ old('nama', $tim_audit->nama) }}"
                                        class="form-control @error('nama') is-invalid @enderror" id="nama"
                                        id="nama" aria-describedby="nama" required>
                                    @error('nama')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="required">Kedudukan</label>
                                    <select class="form-control" name="kedudukan" id="kedudukan">
                                        <option>Choose..</option>
                                        <option value="Penanggung Jawab"
                                            {{ $tim_audit->kedudukan == 'Penanggung Jawab' ? 'selected' : '' }}>
                                            Penanggung Jawab</option>
                                        <option value="Wakil Penanggung Jawab"
                                            {{ $tim_audit->kedudukan == 'Wakil Penanggung Jawab' ? 'selected' : '' }}>
                                            Wakil Penanggung Jawab</option>
                                        <option value="Pengendali Teknis"
                                            {{ $tim_audit->kedudukan == 'Pengendali Teknis' ? 'selected' : '' }}>
                                            Pengendali Teknis</option>
                                        <option value="Ketua"
                                            {{ $tim_audit->kedudukan == 'Ketua' ? 'selected' : '' }}>Ketua</option>
                                        <option value="Anggota"
                                            {{ $tim_audit->kedudukan == 'Anggota' ? 'selected' : '' }}>Anggota</option>
                                    </select>
                                </div>
                                <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    @endforeach

      <!-- Modal Edit Hari Pemeriksaan-->
      @foreach ($jadwal as $jdw)
      <div class="modal fade" id="edithari{{ $jdw->id }}" tabindex="-1" role="dialog"
          aria-labelledby="edithariTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 align="center" class="modal-title" id="edithariTitle" style="padding-left: 35%" ;>
                          Hari Pemeriksaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <form action="{{ url('/update-hari/' . $jdw->id) }}" method="post"
                          enctype="multipart/form-data">
                          @csrf
                          <div class="form-group">
                            <label>Hari Pemeriksaan</label>
                        </div>
                        <div class="mb-3 row">
                            <label class="required" for="inputRmp" class="col-sm-2 col-form-label">PJ</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('pj') is-invalid @enderror"
                                    name="pj" value="{{ $jdw->pj }}" id="pj_edit" required>
                                @error('pj')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required" for="inputRmp" class="col-sm-2 col-form-label">WP</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('wp') is-invalid @enderror"
                                    name="wp" value="{{ $jdw->wp }}" id="wp_edit" required>
                                @error('wp')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required" for="inputRmp" class="col-sm-2 col-form-label">KT</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('kt') is-invalid @enderror"
                                    name="kt" value="{{ $jdw->kt }}" id="kt_edit" required>
                                @error('kt')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required" for="inputRmp" class="col-sm-2 col-form-label">PT</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('pt') is-invalid @enderror"
                                    name="pt" value="{{ $jdw->pt }}" id="pt_edit" required>
                                @error('pt')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="required" for="inputRmp" class="col-sm-2 col-form-label">AT</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control @error('at') is-invalid @enderror"
                                    name="at" value="{{ $jdw->at }}" id="at_edit" required>
                                @error('at')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                          <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-primary">Ubah</button>
                  </div>
                  </form>
              </div>
          </div>
      </div>
@endforeach

      <!-- Modal Edit Sarana dan Prasarana-->
      @foreach ($jadwal as $jdw)
      <div class="modal fade" id="editsarana{{ $jdw->id }}" tabindex="-1" role="dialog"
          aria-labelledby="editsaranaTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-scrollable" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 align="center" class="modal-title" id="editsaranaTitle" style="padding-left: 32%" ;>
                          Sarana dan Prasarana</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      <form action="{{ url('/update-sarana/' . $jdw->id) }}" method="post"
                          enctype="multipart/form-data">
                          @csrf
                          <div class="form-group">
                            <label class="required" for="">Sarana dan Prasarana</label>
                            <input type="text" class="form-control @error('sarana') is-invalid @enderror"
                                name="sarana" id="sarana_edit" aria-describedby="anggaran" name="sarana"
                                value="{{ $jdw->sarana }}" required>
                            @error('sarana')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                          <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-primary">Ubah</button>
                  </div>
                  </form>
              </div>
          </div>
      </div>
@endforeach

<!-- Modal Edit Tingkat Risiko-->
@foreach ($jadwal as $jdw)
<div class="modal fade" id="edittingkat{{ $jdw->id }}" tabindex="-1" role="dialog"
    aria-labelledby="edittingkatTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 align="center" class="modal-title" id="edittingkatTitle" style="padding-left: 41%" ;>
                    Tingkat Risiko</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('/update-tingkat/' . $jdw->id) }}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="required" for="">Tingkat Risiko</label>
                        <select class="form-control" name="tingkat_risiko" id="tingkat_risiko">
                            <option>-- Silahkan Pilih --</option>
                            <option value="Tinggi" {{ $jdw->tingkat_risiko == 'Tinggi' ? 'selected' : '' }}>Tinggi
                            </option>
                            <option value="Sedang" {{ $jdw->tingkat_risiko == 'Sedang' ? 'selected' : '' }}>Sedang
                            </option>
                            <option value="Rendah" {{ $jdw->tingkat_risiko == 'Rendah' ? 'selected' : '' }}>Rendah</option>
                            
                        </select>
                    </div>
                    <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach

<!-- Modal Edit Keterangan-->
@foreach ($jadwal as $jdw)
<div class="modal fade" id="editketerangan{{ $jdw->id }}" tabindex="-1" role="dialog"
    aria-labelledby="editketeranganTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 align="center" class="modal-title" id="editketeranganTitle" style="padding-left: 41%" ;>
                    Keterangan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('/update-keterangan/' . $jdw->id) }}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="required" for="">Keterangan</label>
                        <textarea class="form-control @error('keterangan') is-invalid @enderror" placeholder="" name="keterangan"
                            id="keterangan_edit" style="height: 100px">{{ $jdw->keterangan }}</textarea>
                        @error('keterangan')
                            <div class="alert alert-danger mt-2">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <input type="hidden" id="jadwal" name="jadwal" value="{{ $jdw->id }}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Ubah</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    //edit jadwal
    $(".editJenisPengawasan").click(function() {
        var jadwal_id = $(this).attr('data-id');
        console.log(jadwal_id);
        $.ajax({
            url: "{{ route('EditJadwalLoad') }}",
            type: "GET",
            data: {
                id: jadwal_id
            },
            success: function(data) {
                var jadwal = data.jadwal;
                var jadwalJp1 = data.jp1;
                var jadwalJp2 = data.jp2;
                console.log(jadwal);
                console.log(jadwalJp1);
                console.log(jadwalJp2);
                var htmlJenis1 = "<option value=''>-- Silahkan Pilih --</option>";
                var htmlJenis2 = "<option value=''>-- Silahkan Pilih --</option>";
                    
                $("#jadwal_id").val(jadwal[0]['id']);
                
                for (let y = 0; y < jadwalJp1.length; y++) {
                    if (jadwal[0]['jenis_pemeriksaan1'] == jadwalJp1[y]['id']) {
                        htmlJenis1 += `<option value="` + jadwalJp1[y]['id'] + `" selected>` +
                            jadwalJp1[y]['jenis_jp1'] + `</option>`;
                    } else {
                        htmlJenis1 += `<option value="` + jadwalJp1[y]['id'] + `">` + jadwalJp1[y][
                            'jenis_jp1'
                        ] + `</option>`;
                    }
                }
                for (let x = 0; x < jadwalJp2.length; x++) {
                    if (jadwal[0]['jenis_pemeriksaan2'] == jadwalJp2[x]['id']) {
                        htmlJenis2 += `<option value="` + jadwalJp2[x]['id'] + `" selected>` +
                            jadwalJp2[x]['jenis_jp2'] + `</option>`;
                    } else {
                        htmlJenis2 += `<option value="` + jadwalJp2[x]['id'] + `">` + jadwalJp2[x][
                            'jenis_jp2'
                        ] + `</option>`;
                    }
                }
                $("#updateJenis1").html(htmlJenis1);
                $("#updateJenis2").html(htmlJenis2);
            }
        });
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
@endsection


