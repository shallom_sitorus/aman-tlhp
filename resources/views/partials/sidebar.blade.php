 <div id="loading">
     <div id="loading-center">
         <div class="loader">
             <div class="cube">
                 <div class="sides">
                     <div class="top"></div>
                     <div class="right"></div>
                     <div class="bottom"></div>
                     <div class="left"></div>
                     <div class="front"></div>
                     <div class="back"></div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!-- loader END -->
 <!-- Wrapper Start -->
 <div class="wrapper">
     <!-- Sidebar  -->
     <div class="iq-sidebar">
         <div class="iq-sidebar-logo d-flex justify-content-between">
             <a href="#">
                 <img src="{!! asset('images/Kabupaten_Sukoharjo.png') !!}" class="img-fluid" alt="">
                 <span style="font-size:100% !important" class="text-primary card-title"><b>AMAN TLHP</b></span>
             </a>
             <div class="iq-menu-bt align-self-center">
                 <div class="wrapper-menu">
                     <div class="line-menu half start"></div>
                     <div class="line-menu"></div>
                     <div class="line-menu half end"></div>
                 </div>
             </div>
         </div>
         <div id="sidebar-scrollbar">
             <nav class="iq-sidebar-menu">
                 <ul id="iq-sidebar-toggle" class="iq-menu">
                     <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                         <a href="/dashboard" class="iq-waves-effect" aria-expanded="false"><i
                                 class="ri-home-4-line"></i><span>Dashboard</span></a>
                     </li>
                     @if (Auth::user()->role_id != 2)
                         <li class="{{ Request::is('JadwalPengawasan*') || Request::is('DetailJadwal*') ? 'active' : '' }}">
                             <a href="/JadwalPengawasan" class="iq-waves-effect" aria-expanded="false"><i
                                     class="ri-calendar-2-line"></i><span>Jadwal Pengawasan</span></a>
                         </li>
                     @endif
                     <li class="{{ Request::is('lhp*') || Request::is('detaillhp*') ? 'active' : '' }}">
                         <a href="/lhp" class="iq-waves-effect" aria-expanded="false"><i
                                 class="ri-table-line"></i><span>Laporan Hasil Pengawasan</span></a>
                     </li>
                     @if (Auth::user()->role_id == 1)
                         <li class="{{ Request::is('mitra-pengawasan*') ? 'active' : '' }}">
                             <a href="/mitra-pengawasan" class="iq-waves-effect" aria-expanded="false"><i
                                     class="ri-pages-line"></i><span>Entitas</span></a>
                         </li>
                         <li
                             class="{{ Request::is('Jenispemeriksaan1*') || Request::is('Jenispemeriksaan2*') ? 'active' : '' }}">
                             <a href="/Jenispemeriksaan1" class="iq-waves-effect" aria-expanded="false"><i
                                     class="ri-pencil-ruler-line"></i><span>Jenis Pengawasan</span></a>
                         </li>
                         <li
                             class="{{ Request::is('Timpemeriksa1*') || Request::is('Timpemeriksa2*') ? 'active' : '' }}">
                             <a href="/Timpemeriksa1" class="iq-waves-effect" aria-expanded="false"><i
                                     class="ri-user-line"></i><span>Tim Pemeriksa</span></a>
                         </li>
                         <li class="{{ Request::is('temuan*') ? 'active' : '' }}">
                             <a href="/temuan" class="iq-waves-effect" aria-expanded="false"><i
                                     class="ri-message-line"></i><span>Temuan</span></a>
                         </li>
                         <li class="{{ Request::is('rekomendasi*') ? 'active' : '' }}">
                             <a href="/rekomendasi" class="iq-waves-effect" aria-expanded="false"><i
                                     class="ri-chat-check-line"></i><span>Rekomendasi</span></a>
                         </li>
                         <li class="{{ Request::is('kategoritl*') ? 'active' : '' }}">
                             <a href="/kategoritl" class="iq-waves-effect" aria-expanded="false"><i
                                     class="ri-profile-line"></i><span>Kategori Tindak Lanjut</span></a>
                         </li>
                     @endif
                     <li class="">
                        <a href="{{ asset('Manual Book Inspektorat.pdf') }}" class="iq-waves-effect" target="_blank" aria-expanded="false"><i
                                class="ri-book-line"></i><span>Manual Book</span></a>
                    </li>
                 </ul>
             </nav>
             <div class="p-3"></div>
         </div>
     </div>
     <!-- TOP Nav Bar -->
     <div class="iq-top-navbar">
         <div class="iq-navbar-custom">
             <div class="iq-sidebar-logo">
                 <div class="top-logo">
                     <a href="#" class="logo">
                         <img src="{!! asset('images/Kabupaten_Sukoharjo.png') !!}" class="img-fluid" alt="">
                         <span>Aman TLHP</span>
                     </a>
                 </div>
             </div>
             <div class="navbar-breadcrumb">
                 <h5 class="mb-0">Aplikasi Pemantauan Tindak Lanjut Hasil Pengawasan</h5>
                 <nav aria-label="breadcrumb">
                     <ul class="breadcrumb">
                         <li class="breadcrumb-item active" aria-current="page">Inspektorat Kabupaten Sukoharjo</li>
                     </ul>
             </div>
             <nav class="navbar navbar-expand-lg navbar-light p-0">
                 <button class="navbar-toggler" type="button" data-toggle="collapse"
                     data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                     aria-label="Toggle navigation">
                     <i class="ri-menu-3-line"></i>
                 </button>
                 <div class="iq-menu-bt align-self-center">
                     <div class="wrapper-menu">
                         <div class="line-menu half start"></div>
                         <div class="line-menu"></div>
                         <div class="line-menu half end"></div>
                     </div>
                 </div>
                 <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav ml-auto navbar-list">
                         <li class="nav-item iq-full-screen"><a href="#" class="iq-waves-effect"
                                 id="btnFullscreen"><i class="ri-fullscreen-line"></i></a></li>
                     </ul>
                 </div>
                 <ul class="navbar-list">
                     <li>
                         <a href="#" class="search-toggle iq-waves-effect bg-primary text-white"><img
                                 src="{!! asset('images/user/'. $users->foto) !!}" class="img-fluid rounded" alt="user"></a>
                         <div class="iq-sub-dropdown iq-user-dropdown">
                             <div class="iq-card iq-card-block iq-card-stretch iq-card-height shadow-none m-0">
                                 <div class="iq-card-body p-0 ">
                                     <div class="bg-primary p-3">
                                         <h5 class="mb-0 text-white line-height">Hello {{ Auth::user()->name }}</h5>
                                         <span class="text-white font-size-12">
                                             {{ $users->opd->nama }}
                                         </span>
                                     </div>
                                     <a href="/myprofil" class="iq-sub-card iq-bg-primary-hover">
                                         <div class="media align-items-center">
                                             <div class="rounded iq-card-icon iq-bg-primary">
                                                 <i class="ri-file-user-line"></i>
                                             </div>
                                             <div class="media-body ml-3">
                                                 <h6 class="mb-0 ">My Profile</h6>
                                                 <p class="mb-0 font-size-12">Lihat detail profil pribadi.</p>
                                             </div>
                                         </div>
                                     </a>
                                     <a href="/editprofil" class="iq-sub-card iq-bg-primary-success-hover">
                                         <div class="media align-items-center">
                                             <div class="rounded iq-card-icon iq-bg-success">
                                                 <i class="ri-profile-line"></i>
                                             </div>
                                             <div class="media-body ml-3">
                                                 <h6 class="mb-0 ">Edit Profile</h6>
                                                 <p class="mb-0 font-size-12">Ubah detail pribadi Anda.</p>
                                             </div>
                                         </div>
                                     </a>
                                     <div class="d-inline-block w-100 text-center p-3">
                                         <a class="iq-bg-danger iq-sign-btn" href="{{ route('logout') }}"
                                             onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"
                                             role="button">Log
                                             out<i class="ri-login-box-line ml-2"></i></a>
                                         <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                             class="d-none">
                                             @csrf
                                         </form>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </li>
                 </ul>
             </nav>
         </div>
     </div>
 </div>
