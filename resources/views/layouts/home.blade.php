<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AMAN TLHP</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{!! asset('images/Kabupaten_Sukoharjo.png')!!}" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css')!!}">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="{!! asset('css/typography.css')!!}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{!! asset('css/style.css')!!}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{!! asset('css/responsive.css')!!}">
    <!-- Selectpicker -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    {{-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css">
    <!-- SweetAlert -->
    <link rel="stylesheet" href="sweetalert2.min.css">
    <script src="sweetalert2.min.js"></script>
</head>

<body>
    @include('partials.sidebar')
    <div class="content-page">
        <div class="container-fluid">
            <div class="row-12">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="bg-white iq-footer">
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="col-lg-6">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
                        <li class="list-inline-item"><a href="#">Terms of Use</a></li>
                    </ul>
                </div> -->
                <div class="col-lg-6 text-right">
                    Copyright 2022 <a href="#">Inspektorat Kabupaten Sukoharjo</a> All Rights Reserved.
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer END -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
        $(document).on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const href = $(this).attr("action");

            Swal.fire({
                title: "Apakah Anda yakin?",
                text: "Data yang dihapus tidak dapat kembali",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#808080",
                confirmButtonText: "Ya, hapus!",
                cancelButtonText: "Batal",
            }).then((result) => {
                if (result.value) {
                    document.location.href = href;
                }
            });
        });
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{!! asset('js/jquery.min.js')!!}"></script>
    <script src="{!! asset('js/popper.min.js')!!}"></script>
    <script src="{!! asset('js/bootstrap.min.js')!!}"></script>
    <!-- Search Box -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $(".selectpicker").selectpicker();
    </script>
    <!-- End Search Box -->
    <!-- Appear JavaScript -->
    <script src="{!! asset('js/jquery.appear.js')!!}"></script>
    <!-- Countdown JavaScript -->
    <script src="{!! asset('js/countdown.min.js')!!}"></script>
    <!-- Counterup JavaScript -->
    <script src="{!! asset('js/waypoints.min.js')!!}"></script>
    <script src="{!! asset('js/jquery.counterup.min.js')!!}"></script>
    <!-- Wow JavaScript -->
    <script src="{!! asset('js/wow.min.js')!!}"></script>
    <!-- Apexcharts JavaScript -->
    <script src="{!! asset('js/apexcharts.js')!!}"></script>
    <!-- Slick JavaScript -->
    <script src="{!! asset('js/slick.min.js')!!}"></script>
    <!-- Select2 JavaScript -->
    <script src="{!! asset('js/select2.min.js')!!}"></script>
    <!-- Owl Carousel JavaScript -->
    <script src="{!! asset('js/owl.carousel.min.js')!!}"></script>
    <!-- Magnific Popup JavaScript -->
    <script src="{!! asset('js/jquery.magnific-popup.min.js')!!}"></script>
    <!-- Smooth Scrollbar JavaScript -->
    <script src="{!! asset('js/smooth-scrollbar.js')!!}"></script>
    <!-- lottie JavaScript -->
    <script src="{!! asset('js/lottie.jso')!!}"></script>
    <!-- Chart Custom JavaScript -->
    <script src="{!! asset('js/chart-custom.js')!!}"></script>
    <!-- Custom JavaScript -->
    <script src="{!! asset('js/custom.js')!!}"></script>
    <script src="{!! asset('js/jquery-3.6.1.js')!!}"></script>
    <!-- SweetAlert -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="sweetalert2.all.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    @include('sweetalert::alert')

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    

    <script>
    //insert jenis
    window.addEventListener("DOMContentLoaded", function(){

    const selectedJenis2 = jenis_pemeriksaan1 =>{
        axios.post('{{ route('jenis2.select') }}', {id: jenis_pemeriksaan1})
            .then(function(response){
                $('#selectJenis2').empty();
                $('#selectJenis2').append(new Option('-- Silahkan Pilih --', ''))
                $.each(response.data, function(id, name){
                    $('#selectJenis2').append(new Option(name, id))
                })
            }
        );
    }

    $('#selectJenis1').on('change', function(){
        selectedJenis2($(this).val());
    });

    })
    //end insert jenis

    //edit jenis
    window.addEventListener("DOMContentLoaded", function(){

    const selectedJenis2 = jenis_pemeriksaan1 =>{
        axios.post('{{ route('jenis2.select') }}', {id: jenis_pemeriksaan1})
            .then(function(response){
                $('#updateJenis2').empty();
                $('#updateJenis2').append(new Option('-- Silahkan Pilih --', ''))
                $.each(response.data, function(id, name){
                    $('#updateJenis2').append(new Option(name, id))
                })
            }
        );
    }

    $('#updateJenis1').on('change', function(){
        selectedJenis2($(this).val());
    });

    })
    //end edit jenis
    </script>
    
    <!-- Datatables -->
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function() {
            var oTable = $('#data-table').DataTable({
                scrollX: true,
                responsive: true,
                dom: 't<"d-flex justify-content-between mt-2"ip>',  
                // dom: '<"d-flex"f>rt<"d-flex justify-content-between mt-2"ip>',
            });
            $('#searchInput').keyup(function () {
            oTable.search($(this).val()).draw();
        });
        });
    </script>

</body>

</html>