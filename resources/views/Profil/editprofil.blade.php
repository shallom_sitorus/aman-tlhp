@extends('layouts.home')

@section('content')
<div id="content-page" class="content-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="iq-edit-list-data">
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="personal-information" role="tabpanel">
                            <div class="iq-card">
                                <div class="iq-card-header d-flex justify-content-between">
                                    <div class="iq-header-title">
                                        <h4 class="card-title">Edit Profil</h4>
                                    </div>
                                </div>
                                <div class="iq-card-body">
                                    <form action="{{ url('/update-profil/'. $users->id) }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        @method('put')
                                        <div class="form-group row align-items-center">
                                            <div class="col-md-12">
                                                <div class="profile-img-edit">
                                                    <img class="profile-pic" src="{!! asset('images/user/'. $users->foto)!!}"
                                                        alt="profile-pic">
                                                    <div class="p-image">
                                                        <i class="ri-pencil-line upload-button"></i>
                                                        <input class="file-upload" type="file" accept="image/*" name="foto" id="foto"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" row align-items-center">
                                            <div class="form-group col-sm-6">
                                                <label class="required" for="name">Nama :</label>
                                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $users->name) }}">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="email">Email :</label>
                                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $users->email) }}">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="required" for="username">Username :</label>
                                                <input type="text" class="form-control" id="username" name="username" value="{{ old('username', $users->username) }}">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="required" for="password">Password :</label>
                                                <input type="password" class="form-control" id="password" name="password" value="{{ old('password', $users->password) }}">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="required" for="opd">OPD :</label>
                                                <input type="text" class="form-control" value="{{ $users->opd->nama }}" disabled>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="required" for="role">Role :</label>
                                                <input type="text" class="form-control" value="{{ $users->role->jenis_role }}" disabled>
                                            </div>
                                            <input type="hidden" id="password_lama" name="password_lama" value="{{ $users->password }}">
                                            <input type="hidden" id="foto_lama" name="foto_lama" value="{{ $users->foto }}">
                                            <input type="hidden" id="kode_opd" name="kode_opd" value="{{ $users->kode_opd }}">
                                            <input type="hidden" id="role_id" name="role_id" value="{{ $users->role_id }}">
                                        </div>
                                        <div class="py-3 d-flex justify-content-end">
                                            <button type="reset" class="btn iq-bg-danger">Kembali</button>
                                            <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection