@extends('layouts.home')

@section('content')
<div id="content-page" class="content-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-body profile-page p-0">
                        <div class="profile-header">
                            <div class="cover-container">
                                <img src="{!! asset('images/page-img/profile-bg.jpg')!!}" alt="profile-bg"
                                    class="rounded img-fluid w-100">
                                <ul class="header-nav d-flex flex-wrap justify-end p-0 m-0">
                                    <li><a href="/editprofil" title="Edit Profile"><i class="ri-pencil-line"></i></a></li>
                                    <!-- <li><a href="javascript:void();"><i class="ri-settings-4-line"></i></a></li> -->
                                </ul>
                            </div>
                            <div class="profile-info p-4">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="user-detail pl-5">
                                            <div class="d-flex flex-wrap align-items-center">
                                                <div class="profile-img pr-4">
                                                    <img src="{!! asset('images/user/'. $users->foto)!!}" alt="profile-img"
                                                        class="avatar-130 img-fluid" />
                                                </div>
                                                <div class="profile-detail d-flex align-items-center">
                                                    <h3>{{ $users->name }}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile-profile" role="tabpanel">
                                <div class="iq-card iq-card-block iq-card-stretch"></div>
                                <div class="iq-card iq-card-block iq-card-stretch">
                                    <div class="iq-card-header d-flex justify-content-between">
                                        <div class="iq-header-title">
                                            <h4 class="card-title">Tentang Saya</h4>
                                        </div>
                                    </div>
                                    <div class="iq-card-body">
                                        {{-- <div class="user-bio">
                                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Officia quo reprehenderit necessitatibus, quae, cumque laudantium rem sed delectus facere dolores veniam, doloribus iusto excepturi eum ab libero! Dolorem, deleniti suscipit.</p>
                                        </div> --}}
                                        <div class="mt-2">
                                            <h6>Nama:</h6>
                                            <p>{{ $users->name }}</p>
                                        </div>
                                        <div class="mt-2">
                                            <h6>Username:</h6>
                                            <p>{{ $users->username }}</p>
                                        </div>
                                        <div class="mt-2">
                                            <h6>Email:</h6>
                                            <p><a href="{{ $users->email }}">{{ $users->email }}</a></p>
                                        </div>
                                        <div class="mt-2">
                                            <h6>OPD:</h6>
                                            <p>{{ $users->opd->nama }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection