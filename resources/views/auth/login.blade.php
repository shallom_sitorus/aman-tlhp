<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>AMAN TLHP</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{!! asset('images/Kabupaten_Sukoharjo.png')!!}" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css')!!}">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="{!! asset('css/typography.css')!!}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{!! asset('css/style.css')!!}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{!! asset('css/responsive.css')!!}">
</head>

<body>
    <!-- loader Start -->
    <div id="loading">
        <div id="loading-center">
            <div class="loader">
                <div class="cube">
                    <div class="sides">
                        <div class="top"></div>
                        <div class="right"></div>
                        <div class="bottom"></div>
                        <div class="left"></div>
                        <div class="front"></div>
                        <div class="back"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- loader END -->
    <!-- Sign in Start -->
    <section class="sign-in-page bg-white">
        <div class="container-fluid p-0">
            <div class="row no-gutters">
                <div class="col-sm-6 align-self-center">
                    <div class="sign-in-from">
                        <h1 class="mb-0">Login</h1>
                        <p>Masukkan Username dan kata sandi Anda</p>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group">
                                {{-- <div class="alert alert-danger alert dismissible fade show" role="alert">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                </div> --}}
                                <label for="exampleInputUsername1">Username</label>
                                <input type="text" class="form-control mb-0 @error('username') is-invalid @enderror"
                                    placeholder="Enter Username" name="username"
                                    value="{{ old('username') }}" required autocomplete="username" autofocus>
                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control mb-0 @error('password') is-invalid @enderror"
                                    id="exampleInputPassword1" placeholder="Password" name="password" required
                                    autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>
                                    {{-- @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif --}}
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-sm-6 text-center">
                    <div class="sign-in-detail text-white"
                        style="background: url(images/nyoba/Diskominfo.png) no-repeat 0 0; background-size: cover;">
                        <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false"
                            data-dots="true" data-items="1" data-items-laptop="1" data-items-tab="1"
                            data-items-mobile="1" data-items-mobile-sm="1" data-margin="0">
                            <div class="item">
                                <img src="images/nyoba/logo.png" class="img-fluid mb-4" alt="logo">
                                <h4 class="mb-1 text-white">AMAN TLHP</h4>
                                <p>Aplikasi Pemantauan Tindak Lanjut Hasil Pengawasan
                                    Inspektorat Kabupaten Sukoharjo</p>
                            </div>
                            <div class="item">
                                <img src="images/nyoba/logo.png" class="img-fluid mb-4" alt="logo">
                                <h4 class="mb-1 text-white">AMAN TLHP</h4>
                                <p>Aplikasi Pemantauan Tindak Lanjut Hasil Pengawasan
                                    Inspektorat Kabupaten Sukoharjo</p>
                            </div>
                            <div class="item">
                                <img src="images/nyoba/logo.png" class="img-fluid mb-4" alt="logo">
                                <h4 class="mb-1 text-white">AMAN TLHP</h4>
                                <p>Aplikasi Pemantauan Tindak Lanjut Hasil Pengawasan
                                    Inspektorat Kabupaten Sukoharjo</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Sign in END -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{!! asset('js/jquery.min.js')!!}"></script>
    <script src="{!! asset('js/popper.min.js')!!}"></script>
    <script src="{!! asset('js/bootstrap.min.js')!!}"></script>
    <!-- Appear JavaScript -->
    <script src="{!! asset('js/jquery.appear.js')!!}"></script>
    <!-- Countdown JavaScript -->
    <script src="{!! asset('js/countdown.min.js')!!}"></script>
    <!-- Counterup JavaScript -->
    <script src="{!! asset('js/waypoints.min.js')!!}"></script>
    <script src="{!! asset('js/jquery.counterup.min.js')!!}"></script>
    <!-- Wow JavaScript -->
    <script src="{!! asset('js/wow.min.js')!!}"></script>
    <!-- Apexcharts JavaScript -->
    <script src="{!! asset('js/apexcharts.js')!!}"></script>
    <!-- Slick JavaScript -->
    <script src="{!! asset('js/slick.min.js')!!}"></script>
    <!-- Select2 JavaScript -->
    <script src="{!! asset('js/select2.min.js')!!}"></script>
    <!-- Owl Carousel JavaScript -->
    <script src="{!! asset('js/owl.carousel.min.js')!!}"></script>
    <!-- Magnific Popup JavaScript -->
    <script src="{!! asset('js/jquery.magnific-popup.min.js')!!}"></script>
    <!-- Smooth Scrollbar JavaScript -->
    <script src="{!! asset('js/smooth-scrollbar.js')!!}"></script>
    <!-- lottie JavaScript -->
    <script src="{!! asset('js/lottie.jso')!!}"></script>
    <!-- Chart Custom JavaScript -->
    <script src="{!! asset('js/chart-custom.js')!!}"></script>
    <!-- Custom JavaScript -->
    <script src="{!! asset('js/custom.js')!!}"></script>
</body>

</html>