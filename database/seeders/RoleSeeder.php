<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->insert([[
            'jenis_role' => 'SuperAdmin',
        ], [
            'jenis_role' => 'Admin',
        ], [
            'jenis_role' => 'Tim Pengawas',
        ]]);
    }
}
