<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JenisPemeriksaan2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_pemeriksaan2')->insert([[
            'jenis_jp2' => 'Audit Keuangan',
            'id_jp1' => '1'
        ], [
            'jenis_jp2' => 'Audit Kinerja',
            'id_jp1' => '1'
        ], [
            'jenis_jp2' => 'Evaluasi Kinerja',
            'id_jp1' => '2'
        ], [
            'jenis_jp2' => 'Monitoring',
            'id_jp1' => '3'
        ]]);
    }
}
