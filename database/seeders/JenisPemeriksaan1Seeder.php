<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JenisPemeriksaan1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_pemeriksaan1')->insert([[
            'jenis_jp1' => 'Audit/Pemeriksaan',
        ], [
            'jenis_jp1' => 'Evaluasi',
        ], [
            'jenis_jp1' => 'Pemantauan',
        ]]);
    }
}
