<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class LHPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lhp')->insert([[
            'no_lhp' => '702/005/WI/2022',
            'tanggal_lhp' => '2022-08-01',
            'tanggal_kirim' => '2022-09-05',
            'jadwal' => '1',
            'jadwal_opd' => '1',
            'tanggapan' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus nulla tenetur quam. Corrupti sequi debitis ex! Optio, saepe molestias. Unde nobis delectus dolores ipsum laboriosam quas mollitia minus quaerat facilis.',
            'batas_waktu' => '2022-09-08',
            'jenis_tl' => 'Evaluasi'
        ], [
            'no_lhp' => '703/116/WK/2022',
            'tanggal_lhp' => '2022-9-01',
            'tanggal_kirim' => '2022-9-05',
            'jadwal' => '1',
            'jadwal_opd' => '2',
            'tanggapan' => 'HIHIHIHIHIHIHIHI',
            'batas_waktu' => '2022-09-12',
            'jenis_tl' => 'Proses'
        ]]);
    }
}
