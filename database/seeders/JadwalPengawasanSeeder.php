<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JadwalPengawasanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jadwal_pengawasan')->insert([[
            'tim_pemeriksa1' => '1',
            'jenis_pemeriksaan1' => '1',
            'jenis_pemeriksaan2' => '1',
            'user' => '1',
            'rmp' => '2022-08-01',
            'rsp' => '2022-09-05',
            'rpl' => '2022-09-08',
            'hp' => '5',
            'pj' => '5',
            'wp' => '5',
            'pt' => '5',
            'kt' => '5',
            'at' => '5',
            'anggaran' => '13835000',
            'area_pengawasan' => 'SD Negeri 10 Sukoharjo',
            'tujuan_sasaran' => 'Mencapai Kestabilan Keuangan di Lingkungan Sekolah',
            'ruang_lingkup' => 'Pelaksanaan BOS Reguler TA. 2021',
            'sarana' => 'ATK, Laptop',
            'tingkat_risiko' => 'Sedang',
            'keterangan' => 'Pengawasan berjalan lancar tanpa hambatan suatu apapun seperti jalan tol',

        ],
        [
            'tim_pemeriksa1' => '2',
            'jenis_pemeriksaan1' => '1',
            'jenis_pemeriksaan2' => '2',
            'user' => '1',
            'rmp' => '2022-08-01',
            'rsp' => '2022-09-05',
            'rpl' => '2022-09-08',
            'hp' => '1',
            'pj' => '1',
            'wp' => '1',
            'pt' => '1',
            'kt' => '1',
            'at' => '1',
            'anggaran' => '100000',
            'area_pengawasan' => 'SD Negeri 1 Sukoharjo',
            'tujuan_sasaran' => 'Mencapai Kestabilan Keuangan di Lingkungan Sekolah',
            'ruang_lingkup' => 'Pelaksanaan BOS Reguler TA. 2021',
            'sarana' => 'ATK, Laptop',
            'tingkat_risiko' => 'Sedang',
            'keterangan' => 'Pengawasan berjalan lancar tanpa hambatan suatu apapun seperti jalan tol',
        ]]
    );
    }
}
