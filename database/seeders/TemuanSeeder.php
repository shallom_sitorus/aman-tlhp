<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TemuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('temuan')->insert([[
            'kode_temuan' => '01',
            'nama_temuan' => 'menemukan uang'
        ], [
            'kode_temuan' => '02',
            'nama_temuan' => 'menemukan barang bukti'
        ], [
            'kode_temuan' => '03',
            'nama_temuan' => 'menemukan kecurigaan',
        ]]);

    }
}
