<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JadwalOpdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jadwal_opd')->insert([[
            'id_jadwal' => '1',
            'id_opd' => '1',
        ], [
            'id_jadwal' => '1',
            'id_opd' => '2',
        ], [
            'id_jadwal' => '2',
            'id_opd' => '1',
        ], [
            'id_jadwal' => '2',
            'id_opd' => '2',
        ], [
            'id_jadwal' => '2',
            'id_opd' => '3',
        ], [
            'id_jadwal' => '2',
            'id_opd' => '4',
        ]]);
    }
}
