<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'Shallomita Rembulan Romauli Sitorus',
            'email' => 'superadmin@gmail.com',
            'username' => 'SuperAdmin',
            'password' => Hash::make('rahasia21'),
            'kode_opd' => '1',
            'role_id' => '1',
            'foto' => 'defaultFoto.png'
        ], [
            'name' => 'Salsabila Qurrotul Aini',
            'email' => 'admin@gmail.com',
            'username' => 'Admin',
            'password' => Hash::make('rahasia22'),
            'kode_opd' => '2',
            'role_id' => '2',
            'foto' => 'defaultFoto.png'
        ], [
            'name' => 'Fathul Nisa Aini',
            'email' => 'timpengawasan@gmail.com',
            'username' => 'TimPengawasan',
            'password' => Hash::make('rahasia23'),
            'kode_opd' => '1',
            'role_id' => '3',
            'foto' => 'defaultFoto.png'
        ]]);
    }
}
