<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $this->call([
            OPDSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            JenisPemeriksaan1Seeder::class,
            JenisPemeriksaan2Seeder::class,
            TimPemeriksa1Seeder::class,
            JadwalPengawasanSeeder::class,
            JadwalOpdSeeder::class,
            LHPSeeder::class,
            TemuanSeeder::class,
            TLHPSeeder::class,
            RekomendasiSeeder::class,
            TRLHPSeeder::class,
            KategoriTLSeeder::class,
            TindakLanjutSeeder::class,
            TimAuditSeeder::class,
        ]);
    }
}
