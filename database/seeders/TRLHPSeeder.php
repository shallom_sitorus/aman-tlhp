<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TRLHPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trlhp')->insert([[
            'id_tlhp' => '1',
            'id_rekomendasi' => '1',
            'uraian_rekomendasi' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem exercitationem quidem minima libero animi, deserunt accusamus ullam commodi quia iusto illum mollitia maxime omnis sapiente qui fuga tempore, placeat repellat?'
        ], [
            'id_tlhp' => '1',
            'id_rekomendasi' => '2',
            'uraian_rekomendasi' => ''
        ], [
            'id_tlhp' => '3',
            'id_rekomendasi' => '3',
            'uraian_rekomendasi' => 'Rem exercitationem quidem minima libero animi, deserunt accusamus ullam commodi quia iusto illum mollitia maxime omnis sapiente qui fuga tempore.'
        ]]);
    }
}
