<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TindakLanjutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tindak_lanjut')->insert([[
            'no_tl' => '212/001/TL/2022',
            'tgl_surat' => '2022-09-08',
            'file_surat_tl' => '20221201013048_2380-4922-1-SM.pdf',
            'kategori_tl' => '1',
            'keterangan' => 'Tindak lanjut masih dalam proses, diperlukan beberapa tambahan tindak lanjut',
            // 'tgl_penagihan2' => '2022-09-18',
            // 'tgl_terbit_pemberitahuan' => '2022-09-23',
            // 'no_tindakan_tambahan' => '234/054/TL/2022',
            // 'tgl_tindakan_tambahan' => '2022-09-29',
            'id_trlhp' => '1',
            'id_lhp' => '1'
        ]]);
    }
}
