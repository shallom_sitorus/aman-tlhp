<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TLHPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tlhp')->insert([[
            'id_lhp' => '1',
            'id_temuan' => '1',
            'uraian_temuan' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem exercitationem quidem minima libero animi, deserunt accusamus ullam commodi quia iusto illum mollitia maxime omnis sapiente qui fuga tempore, placeat repellat?'
        ], [
            'id_lhp' => '1',
            'id_temuan' => '2',
            'uraian_temuan' => ''
        ], [
            'id_lhp' => '2',
            'id_temuan' => '3',
            'uraian_temuan' => 'deserunt accusamus ullam commodi quia iusto illum mollitia maxime omnis sapiente qui fuga tempore, placeat repellat?'
        ]]);
    }
}
