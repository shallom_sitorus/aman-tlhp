<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class OPDSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('opd')->insert([[
            'nama' => 'Inspektorat',
            'alamat' => 'Gedung Menara Wijaya Lantai. 7, Jl. Jenderal Sudirman No. 199 Sukoharjo',
            'email' => 'inspektoratsukoharjo@gmail.com',
            'telp' => '(0271) 593068'
        ], [
            'nama' => 'Dinas Komunikasi dan Informatika',
            'alamat' => 'Jl. Jenderal Sudirman Nomor 199 Sukoharjo',
            'email' => 'diskominfo@sukoharjokab.go.id',
            'telp' => '(0271) 593068 '
        ], [
            'nama' => 'Dinas Sosial',
            'alamat' => 'Jl. Veteran No.61, Kutorejo, Jetis, Kec. Sukoharjo, Kabupaten Sukoharjo, Jawa Tengah 57511',
            'email' => 'dinsos@sukoharjokab.go.id',
            'telp' => '(0271) 593024'
        ], [
            'nama' => 'Dinas Lingkungan Hidup',
            'alamat' => 'Jl. Tentara Pelajar, Jombor, Bendosari, Sukoharjo',
            'email' => 'blh@sukoharjokab.go.id',
            'telp' => '(0271) 591613'
        ], [
            'nama' => 'Badan Keuangan Daerah',
            'alamat' => 'Gedung Soewarno Honggopati Jalan Jenderal Sudirman Nomor 199 Sukoharjo',
            'email' => 'dppkad.sukoharjo@gmail.com',
            'telp' => '(0271) 591678'
        ]]);
    }
}
