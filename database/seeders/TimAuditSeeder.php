<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TimAuditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tim_audit')->insert([
        [
            'nama' => 'Drs. Djoko Poernomo',
            'jadwal' => '1',
            'kedudukan' => 'Penanggung Jawab',

        ],
        [
            'nama' => 'Sutarto, S.STP, MH',
            'jadwal' => '1',
            'jadwal' => '1',
            'kedudukan' => 'Wakil Penanggung Jawab',

        ],
        [
            'nama' => 'Sri Wahyuni, S.Sos, MM',
            'jadwal' => '1',
            'kedudukan' => 'Pengendali Teknis',

        ],
        [
            'nama' => 'Heni Rahayu, SE, MM',
            'jadwal' => '1',
            'kedudukan' => 'Ketua',

        ],
        [
            'nama' => 'Untung Priyanto, S.Pd, SH., M.Si',
            'jadwal' => '1',
            'kedudukan' => 'Anggota',

        ],
    ]);
    }
}
