<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TimPemeriksa1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tim_pemeriksa1')->insert([[
            'jenis_tp1' => 'APIP Internal',
        ],
        [
            'jenis_tp1' => 'APIP Eksternal',
        ]]);
    }
}
