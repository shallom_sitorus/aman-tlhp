<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RekomendasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rekomendasi')->insert([[
            'kode_rekomendasi' => '01',
            'nama_rekomendasi' => 'rekomendasi awal'
        ], [
            'kode_rekomendasi' => '02',
            'nama_rekomendasi' => 'rekomendasi tengah'
        ], [
            'kode_rekomendasi' => '03',
            'nama_rekomendasi' => 'rekomendasi akhir'
        ]]);
    }
}
