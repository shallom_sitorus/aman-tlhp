<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tlhp', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_lhp')->nullable()->constrained('lhp')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('id_temuan')->nullable()->constrained('temuan')->cascadeOnUpdate()->nullOnDelete();
            $table->text('uraian_temuan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tlhp');
    }
};
