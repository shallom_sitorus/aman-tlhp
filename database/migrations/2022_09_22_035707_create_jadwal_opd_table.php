<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_opd', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_jadwal')->nullable()->constrained('jadwal_pengawasan')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('id_opd')->nullable()->constrained('opd')->cascadeOnUpdate()->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_opd');
    }
};
