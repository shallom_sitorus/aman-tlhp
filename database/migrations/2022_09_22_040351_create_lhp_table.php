<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lhp', function (Blueprint $table) {
            $table->id();
            $table->string('no_lhp');
            $table->date('tanggal_lhp');
            $table->date('tanggal_kirim');
            $table->foreignId('jadwal')->nullable()->constrained('jadwal_pengawasan')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('jadwal_opd')->nullable()->constrained('jadwal_opd')->cascadeOnUpdate()->nullOnDelete();
            $table->text('tanggapan');
            $table->date('batas_waktu');
            $table->string('jenis_tl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lhp');
    }
};
