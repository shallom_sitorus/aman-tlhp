<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_pengawasan', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tim_pemeriksa1')->nullable()->constrained('tim_pemeriksa1')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('jenis_pemeriksaan1')->nullable()->constrained('jenis_pemeriksaan1')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('jenis_pemeriksaan2')->nullable()->constrained('jenis_pemeriksaan2')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('user')->nullable()->constrained('users')->cascadeOnUpdate()->nullOnDelete();
            $table->date('rmp');
            $table->date('rsp');
            $table->date('rpl');
            $table->integer('hp');
            $table->integer('pj');
            $table->integer('wp');
            $table->integer('pt');
            $table->integer('kt');
            $table->integer('at');
            $table->integer('anggaran');
            $table->string('area_pengawasan');
            $table->string('tujuan_sasaran');
            $table->string('ruang_lingkup');
            $table->string('sarana');
            $table->string('tingkat_risiko');
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_pengawasan');
    }
};
