<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tindak_lanjut', function (Blueprint $table) {
            $table->id();
            $table->string('no_tl')->nullable();
            $table->date('tgl_surat')->nullable();
            $table->string('file_surat_tl')->nullable();
            $table->foreignId('kategori_tl')->nullable()->constrained('kategori_tl')->cascadeOnUpdate()->nullOnDelete();
            $table->text('keterangan')->nullable();
            $table->date('tgl_penagihan1')->nullable();
            $table->string('file_penagihan1')->nullable();
            $table->date('tgl_penagihan2')->nullable();
            $table->string('file_penagihan2')->nullable();
            $table->date('tgl_terbit_pemberitahuan')->nullable();
            $table->string('file_pemberitahuan')->nullable();
            $table->string('no_tindakan_tambahan')->nullable();
            $table->date('tgl_tindakan_tambahan')->nullable();
            $table->string('file_tindakan_tambahan')->nullable();
            $table->foreignId('id_trlhp')->nullable()->constrained('trlhp')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('id_lhp')->nullable()->constrained('lhp')->cascadeOnUpdate()->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tindak_lanjut');
    }
};
